//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class AccountRolePermission : BaseEntity
    {
        public int RoleId { get; set; }
        public int PermissionId { get; set; }
    
        public virtual AccountPermission AccountPermission { get; set; }
        public virtual AccountRole AccountRole { get; set; }
    }
}
