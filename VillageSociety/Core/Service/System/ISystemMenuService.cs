﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface ISystemMenuService : IRowNumberedService<SystemMenu>
    {
        List<SystemMenuEx> ListOfLeftTree(Expression<Func<SystemMenu, bool>> filter, int userId);
    }
}
