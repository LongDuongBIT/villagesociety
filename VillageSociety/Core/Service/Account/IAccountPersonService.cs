﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IAccountPersonService : IBaseService<AccountPerson>
    {
        IQueryable<AccountPersonEx> GetEx(Expression<Func<AccountPerson, bool>> filter = null,
            Expression<Func<SystemVillage, bool>> filterVillage = null,
            Expression<Func<AccountPerson, bool>> filterFather = null,
            Expression<Func<AccountPerson, bool>> filterMother = null);
    }
}
