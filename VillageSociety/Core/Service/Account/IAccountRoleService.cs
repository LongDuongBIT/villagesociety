﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IAccountRoleService : IRowNumberedService<AccountRole>
    {
        IQueryable<AccountRoleEx> GetWithLanguage(Expression<Func<AccountRole, bool>> filter, int languageId);
    }
}
