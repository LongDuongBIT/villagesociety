﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IAccountPermissionMapService : IBaseService<AccountPermissionMap>
    {
        IQueryable<AccountPermissionMapWithRole> GetWithRole(int? roleId, int languageId);
        IQueryable<AccountPermissionMapWithUser> GetWithUser(int? userId, int languageId);
    }
}
