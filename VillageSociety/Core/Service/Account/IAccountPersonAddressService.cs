﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IAccountPersonAddressService : IBaseService<AccountPersonAddress>
    {
        IQueryable<AccountPersonAddressEx> GetEx(Expression<Func<AccountPersonAddress, bool>> filter = null,
            Expression<Func<AccountPerson, bool>> filterPerson = null,
            Expression<Func<Address, bool>> filterAddress = null,
            Expression<Func<AddressDistrict, bool>> filterDistrict = null,
            Expression<Func<AddressCity, bool>> filterCity = null,
            Expression<Func<AddressCountry, bool>> filterCountry = null,
            Expression<Func<AddressType, bool>> filterAddressType = null);
    }
}
