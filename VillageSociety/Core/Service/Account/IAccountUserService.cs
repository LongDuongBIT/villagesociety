﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IAccountUserService : IBaseService<AccountUser>
    {
        IQueryable<AccountUserEx> GetEx(Expression<Func<AccountUser, bool>> filter = null,
            Expression<Func<AccountPerson, bool>> filterPerson = null,
            Expression<Func<SystemVillage, bool>> filterVillage = null,
            Expression<Func<AccountRole, bool>> filterRole = null);

        void SaveProfile(AccountPerson person, AccountUser user);

        IQueryable<int?> GetPermissions(int userId);
    }
}
