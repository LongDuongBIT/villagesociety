﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IAccountRoleLanguageService : IBaseService<AccountRoleLanguage>
    {
        IQueryable<AccountRoleLanguageEx> GetEx(Expression<Func<AccountRoleLanguage, bool>> filter = null);
    }
}
