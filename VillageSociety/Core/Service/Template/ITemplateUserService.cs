﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface ITemplateUserService : IBaseService<TemplateUser>
    {
        IQueryable<TemplateUserEx> GetEx(Expression<Func<TemplateUser, bool>> filter = null,
            Expression<Func<AccountUser, bool>> filterUser = null);
        void Edit(int masterId, int[] ids, DateTime updateDate, int updatedBy);
    }
}
