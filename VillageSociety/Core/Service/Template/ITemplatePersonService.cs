﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface ITemplatePersonService : IBaseService<TemplatePerson>
    {
        IQueryable<TemplatePersonEx> GetEx(Expression<Func<TemplatePerson, bool>> filter = null,
            Expression<Func<AccountPerson, bool>> filterPerson = null);
        void Edit(int masterId, int[] ids, DateTime updateDate, int updatedBy);
    }
}
