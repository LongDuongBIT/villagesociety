﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface ITemplateDetail1Service : IBaseService<TemplateDetail1>
    {
        IQueryable<TemplateDetail1Ex> GetEx(Expression<Func<TemplateDetail1, bool>> filter = null);
    }
}
