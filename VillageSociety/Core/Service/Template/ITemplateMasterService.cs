﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface ITemplateMasterService : IBaseService<TemplateMaster>
    {
        IQueryable<TemplateMasterEx> GetEx(Expression<Func<TemplateMaster, bool>> filter = null);
        void Insert(TemplateMaster item, int[] templatePersons, int[] templateUsers);
        void Update(TemplateMaster item, int[] templatePersons, int[] templateUsers);
    }
}
