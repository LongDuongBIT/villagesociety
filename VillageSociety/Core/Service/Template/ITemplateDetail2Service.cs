﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface ITemplateDetail2Service : IRowNumberedService<TemplateDetail2>
    {
        IQueryable<TemplateDetail2Ex> GetEx(Expression<Func<TemplateDetail2, bool>> filter = null);
    }
}
