﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IAddressService : IRowNumberedService<Address>
    {
        IQueryable<AddressEx> GetEx(Expression<Func<Address, bool>> filter = null,
            Expression<Func<AddressCountry, bool>> filterCountry = null,
            Expression<Func<AddressCity, bool>> filterCity = null,
            Expression<Func<AddressDistrict, bool>> filterDistrict = null);

        AddressSetStruct InsertOrUpdate(AddressCountry country, AddressCity city, AddressDistrict district, Address address);
    }
}
