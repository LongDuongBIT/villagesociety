﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IAddressDistrictService : IRowNumberedService<AddressDistrict>
    {
        IQueryable<AddressDistrictEx> GetEx(Expression<Func<AddressDistrict, bool>> filter = null,
            Expression<Func<AddressCountry, bool>> filterCountry = null,
            Expression<Func<AddressCity, bool>> filterCity = null);
    }
}
