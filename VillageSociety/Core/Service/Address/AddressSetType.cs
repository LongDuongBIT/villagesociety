﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public struct AddressSetStruct
    {
        public AddressCountry Country { get; set; }
        public AddressCity City { get; set; }
        public AddressDistrict District { get; set; }
        public Address Address { get; set; }
    }
}
