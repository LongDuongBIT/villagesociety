﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IAddressCityService : IRowNumberedService<AddressCity>
    {
        IQueryable<AddressCityEx> GetEx(Expression<Func<AddressCity, bool>> filter = null,
            Expression<Func<AddressCountry, bool>> filterAddressCountry = null);
    }
}
