﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IRowNumberedService<TEntity> : IBaseService<TEntity> where TEntity : RowNumberedEntity
    {
        bool Up(int id1, int? id2);
        bool Down(int id1, int? id2);
    }
}
