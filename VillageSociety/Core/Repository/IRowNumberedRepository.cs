﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IRowNumberedRepository<TEntity> : IRepository<TEntity> where TEntity : RowNumberedEntity
    {
        void Up(TEntity item);
        void Down(TEntity item);
    }
}
