﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null);
        TEntity First(Expression<Func<TEntity, bool>> filter);
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter);
        TEntity Single(Expression<Func<TEntity, bool>> filter);
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> filter);
        TEntity GetById(int id);
        void Insert(TEntity item);
        void Update(TEntity item);
        void Edit(TEntity item, params string[] ignoreButSomeProperties);
        void Delete(TEntity item);
        void Restore(TEntity item, bool ignoreProperties);

        UniqueKeyException ParseUniqueKeyException(Exception exception, TEntity item);
        JsonResultModel ExceptionToJsonResult(Exception exception, TEntity item, bool isNewRecord);
    }
}
