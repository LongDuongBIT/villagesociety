﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core
{
    public class JsonPostModel
    {
        public string Command { get; set; }
        public string Parameters { get; set; }
    }
}