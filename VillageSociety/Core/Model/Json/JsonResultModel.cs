﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core
{
    public class JsonResultModel
    {
        public object Result { get; set; }
        public string Exception { get; set; }
    }
}