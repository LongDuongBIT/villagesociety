﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Core
{
    public class UniqueKeyException : BaseException
    {
        public string EntityName { get; set; }
        public string[] Columns { get; set; }
        public int Id { get; set; }
        public bool IsDeleted { get; set; }

        public UniqueKeyException(string entityName, string[] columns)
        {
            this.EntityName = entityName;
            this.Columns = columns;
        }

        public static UniqueKeyException Parse(Exception exception, string entityName)
        {
            while (exception != null)
            {
                if (exception.Message != null)
                {
                    Match match = Regex.Match(exception.Message, "IX_" + entityName + "(_[^_]+)+" + "_Unique");
                    if (match.Success)
                    {
                        List<string> columns = new List<string>();

                        //IX_AddressType_AddressTypeName_Unique
                        foreach(Capture capture in match.Groups[1].Captures)
                        {
                            columns.Add(capture.Value.Substring(1));
                        }

                        return new UniqueKeyException(entityName, columns.ToArray());
                    }
                }
                exception = exception.InnerException;
            }

            return null;
        }
    }
}
