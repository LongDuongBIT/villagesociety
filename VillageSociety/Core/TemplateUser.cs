//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class TemplateUser : BaseEntity
    {
        public int MasterId { get; set; }
        public int UserId { get; set; }
    
        public virtual AccountUser AccountUser { get; set; }
        public virtual TemplateMaster TemplateMaster { get; set; }
    }
}
