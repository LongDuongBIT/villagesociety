//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class AddressType : RowNumberedEntity
    {
        public AddressType()
        {
            this.AccountPersonAddress = new HashSet<AccountPersonAddress>();
        }
    
        public string AddressTypeName { get; set; }
        public string AddressTypeDescription { get; set; }
    
        public virtual ICollection<AccountPersonAddress> AccountPersonAddress { get; set; }
    }
}
