﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class SystemMenuEx : SystemMenu
    {
        public bool IsLeaf { get; set; }
        public bool Signed { get; set; }
        public bool FootprintOnTheLadderToParent { get; set; }
    }
}
