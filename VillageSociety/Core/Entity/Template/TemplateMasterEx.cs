﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class TemplateMasterEx : TemplateMaster
    {
        public string ComboBoxFirstName { get; set; }
        public string ComboBoxLastName { get; set; }

        public string GridLookupFirstName { get; set; }
        public string GridLookupLastName { get; set; }

        public int ComboBoxCountryId { get; set; }
        public string ComboBoxCountryName { get; set; }
        public int ComboBoxCityId { get; set; }
        public string ComboBoxCityName { get; set; }
        public int ComboBoxDistrictId { get; set; }
        public string ComboBoxDistrictName { get; set; }
        public string ComboBoxFullAddress { get; set; }
        public string ComboBoxPostalCode { get; set; }
        public double ComboBoxLatitude { get; set; }
        public double ComboBoxLongitude { get; set; }

        public int GridLookupCountryId { get; set; }
        public string GridLookupCountryName { get; set; }
        public int GridLookupCityId { get; set; }
        public string GridLookupCityName { get; set; }
        public int GridLookupDistrictId { get; set; }
        public string GridLookupDistrictName { get; set; }
        public string GridLookupFullAddress { get; set; }
        public string GridLookupPostalCode { get; set; }
        public double GridLookupLatitude { get; set; }
        public double GridLookupLongitude { get; set; }
    }
}
