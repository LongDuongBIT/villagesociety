﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class TemplateDetail2Ex : TemplateDetail2
    {
        public string StringColumn { get; set; }
        public string MultilineColumn { get; set; }
        public string HtmlColumn { get; set; }
        public string PhoneNumberColumn { get; set; }
        public string CreditCardNumberColumn { get; set; }
        public double LatitudeColumn { get; set; }
        public double LongitudeColumn { get; set; }

    }
}
