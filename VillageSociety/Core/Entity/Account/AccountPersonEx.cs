﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public partial class AccountPersonEx : AccountPerson
    {
        public string VillageName { get; set; }

        public string FatherFirstName { get; set; }
        public string FatherLastName { get; set; }
        
        public string MotherFirstName { get; set; }
        public string MotherLastName { get; set; }
    }
}
