﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public partial class AccountRoleLanguageEx : AccountRoleLanguage
    {
        public string DefaultRoleName { get; set; }
        public string DefaultRoleDescription { get; set; }

        public string LanguageCode { get; set; }
        public string LanguageName { get; set; }
        public string IconPath { get; set; }
    }
}
