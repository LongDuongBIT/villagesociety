﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public partial class AccountPersonAddressEx : AccountPersonAddress
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int CountryId { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }

        public int CityId { get; set; }
        public string CityName { get; set; }

        public int DistrictId { get; set; }
        public string DistrictName { get; set; }

        public string FullAddress { get; set; }
        public string FullAddressDescription { get; set; }
        public string PostalCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string AddressTypeName { get; set; }
    }
}
