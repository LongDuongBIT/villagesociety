﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class AccountUserEx : AccountUser
    {
        public int VillageId { get; set; }
        public string VillageName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsMale { get; set; }
        public string PicturePath { get; set; }
        public string RoleName { get; set; }
        public string ThemeName { get; set; }
        public string LanguageCode { get; set; }
        public string LanguageName { get; set; }
    }
}
