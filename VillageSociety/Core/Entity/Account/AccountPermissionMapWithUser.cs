﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class AccountPermissionMapWithUser : AccountPermissionMap
    {
        public string PermissionName { get; set; }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string UserEMail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int? UserPermissionId { get; set; }
        public bool? IsDeletedUserPermission { get; set; }
        public int? RolePermissionId { get; set; }
        public bool? IsDeletedRolePermission { get; set; }
    }
}
