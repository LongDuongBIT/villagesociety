﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class AccountPermissionMapWithRole : AccountPermissionMap
    {
        public string PermissionName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int? RolePermissionId { get; set; }
        public bool InRolePermission { get; set; }
    }
}
