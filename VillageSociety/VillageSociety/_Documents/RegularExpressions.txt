1. "ClassA" class properties to "select new ClassA""
find:
public [^ ]* ([^ ]*) \{ get\; set\; \}
replace:
$1 = item.$1,