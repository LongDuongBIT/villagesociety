﻿using Core;
using Service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VillageSociety.Models;

namespace VillageSociety.Controllers
{
    [PermissionAuthorize("AccountUserPermission", Permission.ReadAccountUserPermission)]
    public class AccountUserPermissionController : BaseTreeListController
    {
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("AccountUserPermission", Permission.ReadAccountUserPermission)]
        public ActionResult TreeListInit(AccountUserPermissionTreeListModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("AccountUserPermission", Permission.ReadAccountUserPermission)]
        public ActionResult TreeListPartial(AccountUserPermissionTreeListModel model)
        {
            if (model.JsonData != null)
            {
                var jo = JObject.Parse(model.JsonData);
                string command = jo.Value<string>("Command");

                if (command == "Reload" || command == "Save")
                {
                    ViewData["Command"] = command;
                }
            }

            model = (AccountUserPermissionTreeListModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("AccountUserPermission", Permission.ExportAccountUserPermission)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }
    }
}