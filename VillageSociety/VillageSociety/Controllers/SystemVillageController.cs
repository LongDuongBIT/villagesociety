﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class SystemVillageController : BaseGridController
    {
        protected readonly ISystemVillageService service;

        public SystemVillageController(ISystemVillageService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("SystemVillage", Permission.ReadSystemVillage)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("SystemVillage", Permission.ReadSystemVillage)]
        public ActionResult GridInit(SystemVillageGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("SystemVillage", Permission.ReadSystemVillage)]
        public ActionResult GridPartial(SystemVillageGridModel model)
        {
            model = (SystemVillageGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(SystemVillageEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("SystemVillage", Permission.InsertSystemVillage)]
        public ActionResult InsertPartial(SystemVillageEditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("SystemVillage", Permission.InsertSystemVillage)]
        public ActionResult Insert(SystemVillageEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new SystemVillage();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("SystemVillage", Permission.UpdateSystemVillage)]
        public ActionResult UpdatePartial(SystemVillageEditModel model)
        {
            var item = service.GetById(model.Id);
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("SystemVillage", Permission.UpdateSystemVillage)]
        public ActionResult Update(SystemVillageEditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new SystemVillage();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("SystemVillage", Permission.DeleteSystemVillage)]
        public ActionResult Delete(DeleteModel model)
        {
            var jsonModel = new JsonResultModel();

            foreach (int id in model.Ids)
            {
                var item = new SystemVillage { Id = id };
                service.Delete(item.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("SystemVillage", Permission.ExportSystemVillage)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("SystemVillage", Permission.InsertSystemVillage, Permission.UpdateSystemVillage)]
        public ActionResult MasterIdPartial(SystemVillageEditModel model)
        {
            return PartialView(model);
        }
    }
}