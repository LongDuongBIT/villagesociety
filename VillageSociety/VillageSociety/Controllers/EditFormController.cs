﻿using AutoMapper;
using Core;
using Internationalization;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VillageSociety.Models;

namespace VillageSociety.Controllers
{
    public class EditFormController : BaseController
    {
        public ActionResult ComboBoxMenuPartial(ComboBoxMenuModel model)
        {
            return PartialView(model);
        }

        public ActionResult GridLookupMenuPartial(GridLookupMenuModel model)
        {
            return PartialView(model);
        }

        public ActionResult MapMenuPartial(MapMenuModel model)
        {
            return PartialView(model);
        }

        public ActionResult GoogleMapsFindAddressPartial(MapModel model)
        {
            return PartialView(model);
        }

        public ActionResult GoogleMapsFindAddressContentPartial(MapModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AddressCountry", Permission.InsertAddressCountry)]
        [PermissionAuthorize("AddressCity", Permission.InsertAddressCity)]
        [PermissionAuthorize("AddressDistrict", Permission.InsertAddressDistrict)]
        [PermissionAuthorize("Address", Permission.InsertAddress)]
        public ActionResult GoogleMapsSaveToAddress(MapModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                try
                {
                    var country = new AddressCountry();
                    Mapper.DynamicMap(model, country);
                    country.CountryDescription = "Google Maps";

                    var city = new AddressCity();
                    Mapper.DynamicMap(model, city);
                    city.CityDescription = "Google Maps";

                    var district = new AddressDistrict();
                    Mapper.DynamicMap(model, district);
                    district.DistrictDescription = "Google Maps";

                    var address = new Address();
                    Mapper.DynamicMap(model, address);
                    address.FullAddressDescription = "Google Maps";

                    var service = IoC.Resolve<IAddressService>();
                    var addressSet = service.InsertOrUpdate(country.WithInsertDefaults(), city.WithInsertDefaults(), district.WithInsertDefaults(), address.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = addressSet.Address.Id, DistrictId = addressSet.District.Id, FullAddress = addressSet.Address.FullAddress, PostalCode = addressSet.Address.PostalCode, Latitude = addressSet.Address.Latitude, Longitude = addressSet.Address.Longitude, CityId = addressSet.City.Id, DistrictName = addressSet.District.DistrictName, CountryId = addressSet.Country.Id, CityName = addressSet.City.CityName, CountryCode = addressSet.Country.CountryCode, CountryName = addressSet.Country.CountryName };
                }
                catch (Exception ex)
                {
                    jsonModel.Exception = "Error";
                    jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }
    }
}