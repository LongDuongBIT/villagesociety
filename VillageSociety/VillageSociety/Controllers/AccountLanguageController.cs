﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class AccountLanguageController : BaseGridController
    {
        protected readonly IAccountLanguageService service;

        public AccountLanguageController(IAccountLanguageService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("AccountLanguage", Permission.ReadAccountLanguage)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("AccountLanguage", Permission.ReadAccountLanguage)]
        public ActionResult GridInit(AccountLanguageGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("AccountLanguage", Permission.ReadAccountLanguage)]
        public ActionResult GridPartial(AccountLanguageGridModel model)
        {
            model = (AccountLanguageGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(AccountLanguageEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountLanguage", Permission.InsertAccountLanguage)]
        public ActionResult InsertPartial(AccountLanguageEditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountLanguage", Permission.InsertAccountLanguage)]
        public ActionResult Insert(AccountLanguageEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountLanguage();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("AccountLanguage", Permission.UpdateAccountLanguage)]
        public ActionResult UpdatePartial(AccountLanguageEditModel model)
        {
            var item = service.GetById(model.Id);
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountLanguage", Permission.UpdateAccountLanguage)]
        public ActionResult Update(AccountLanguageEditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountLanguage();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AccountLanguage", Permission.DeleteAccountLanguage)]
        public ActionResult Delete(DeleteModel model)
        {
            var jsonModel = new JsonResultModel();

            foreach (int id in model.Ids)
            {
                var item = new AccountLanguage { Id = id };
                service.Delete(item.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail2", Permission.InsertTemplateDetail2, Permission.UpdateTemplateDetail2)]
        public ActionResult Restore(RestoreModel model)
        {
            var jsonModel = new JsonResultModel(); ;

            var item = new AccountLanguage { Id = model.ExistingId };
            service.Restore(item.WithRestoreDefaults(), true);

            if (model.UpdatingId.HasValue)
            {
                var itemDelete = new AccountLanguage { Id = model.UpdatingId.Value };
                service.Delete(itemDelete.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail2", Permission.UpdateTemplateDetail2)]
        public ActionResult Up(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Up(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail2", Permission.UpdateTemplateDetail2)]
        public ActionResult Down(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Down(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("AccountLanguage", Permission.ExportAccountLanguage)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

    }
}