﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class AddressDistrictController : BaseGridController
    {
        protected readonly IAddressDistrictService service;

        public AddressDistrictController(IAddressDistrictService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("AddressDistrict", Permission.ReadAddressDistrict)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("AddressDistrict", Permission.ReadAddressDistrict)]
        public ActionResult GridInit(AddressDistrictGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("AddressDistrict", Permission.ReadAddressDistrict)]
        public ActionResult GridPartial(AddressDistrictGridModel model)
        {
            model = (AddressDistrictGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(AddressDistrictEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AddressDistrict", Permission.InsertAddressDistrict)]
        public ActionResult InsertPartial(AddressDistrictEditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AddressDistrict", Permission.InsertAddressDistrict)]
        public ActionResult Insert(AddressDistrictEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AddressDistrict();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("AddressDistrict", Permission.UpdateAddressDistrict)]
        public ActionResult UpdatePartial(AddressDistrictEditModel model)
        {
            var item = service.GetEx(p => p.Id == model.Id).Single();
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AddressDistrict", Permission.UpdateAddressDistrict)]
        public ActionResult Update(AddressDistrictEditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AddressDistrict();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressDistrict", Permission.DeleteAddressDistrict)]
        public ActionResult Delete(DeleteModel model)
        {
            var jsonModel = new JsonResultModel();

            foreach (int id in model.Ids)
            {
                var item = new AddressDistrict { Id = id };
                service.Delete(item.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressDistrict", Permission.InsertAddressDistrict, Permission.UpdateAddressDistrict)]
        public ActionResult Restore(RestoreModel model)
        {
            var jsonModel = new JsonResultModel(); ;

            var item = new AddressDistrict { Id = model.ExistingId };
            service.Restore(item.WithRestoreDefaults(), true);

            if (model.UpdatingId.HasValue)
            {
                var itemDelete = new AddressDistrict { Id = model.UpdatingId.Value };
                service.Delete(itemDelete.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressDistrict", Permission.UpdateAddressDistrict)]
        public ActionResult Up(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Up(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressDistrict", Permission.UpdateAddressDistrict)]
        public ActionResult Down(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Down(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("AddressDistrict", Permission.ExportAddressDistrict)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("AddressDistrict", Permission.InsertAddressDistrict, Permission.UpdateAddressDistrict)]
        public ActionResult CountryIdPartial(AddressDistrictEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AddressDistrict", Permission.InsertAddressDistrict, Permission.UpdateAddressDistrict)]
        public ActionResult CityIdPartial(AddressDistrictEditModel model)
        {
            return PartialView(model);
        }
    }
}