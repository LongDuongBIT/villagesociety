﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;

namespace VillageSociety.Controllers
{
    public class AccountPersonAddressController : BaseGridController
    {
        protected readonly IAccountPersonAddressService service;

        public AccountPersonAddressController(IAccountPersonAddressService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("AccountPersonAddress", Permission.ReadAccountPersonAddress)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("AccountPersonAddress", Permission.ReadAccountPersonAddress)]
        public ActionResult GridInit(AccountPersonAddressGridModel model)
        {
            Session[model.Name] = model;

            return PartialView(model);
        }

        [PermissionAuthorize("AccountPersonAddress", Permission.ReadAccountPersonAddress)]
        public ActionResult GridPartial(AccountPersonAddressGridModel model)
        {
            model = (AccountPersonAddressGridModel)Session[model.Name];
            return PartialView(model);
        }

        [PermissionAuthorize("AccountPersonAddress", Permission.InsertAccountPersonAddress, Permission.UpdateAccountPersonAddress)]
        public ActionResult EditPartial(AccountPersonAddressEditModel model)
        {
            if (!model.IsNewRecord)
            {
                var item = IoC.Resolve<IAccountPersonAddressService>().GetEx(p => p.Id == model.Id).Single();
                AutoMapper.Mapper.DynamicMap(item, model);
            }

            return PartialView(model);
        }

        public ActionResult DetailPartial(AccountPersonAddressEditModel model)
        {
            return PartialView(model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountPersonAddress", Permission.InsertAccountPersonAddress)]
        public JsonResult Insert(AccountPersonAddressEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountPersonAddress();
                try
                {
                    AutoMapper.Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AccountPersonAddress", Permission.UpdateAccountPersonAddress)]
        public JsonResult Update(AccountPersonAddressEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountPersonAddress();
                try
                {
                    AutoMapper.Mapper.DynamicMap(model, item);

                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("AccountPersonAddress", Permission.ExportAccountPersonAddress)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("AccountPersonAddress", Permission.InsertAccountPersonAddress, Permission.UpdateAccountPersonAddress)]
        public ActionResult PersonIdPartial(AccountPersonAddressEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountPersonAddress", Permission.InsertAccountPersonAddress, Permission.UpdateAccountPersonAddress)]
        public ActionResult CountryIdPartial(AccountPersonAddressEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountPersonAddress", Permission.InsertAccountPersonAddress, Permission.UpdateAccountPersonAddress)]
        public ActionResult CityIdPartial(AccountPersonAddressEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountPersonAddress", Permission.InsertAccountPersonAddress, Permission.UpdateAccountPersonAddress)]
        public ActionResult DistrictIdPartial(AccountPersonAddressEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountPersonAddress", Permission.InsertAccountPersonAddress, Permission.UpdateAccountPersonAddress)]
        public ActionResult AddressIdPartial(AccountPersonAddressEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountPersonAddress", Permission.InsertAccountPersonAddress, Permission.UpdateAccountPersonAddress)]
        public ActionResult AddressTypeIdPartial(AccountPersonAddressEditModel model)
        {
            return PartialView(model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountPersonAddress", Permission.InsertAccountPersonAddress, Permission.UpdateAccountPersonAddress)]
        public JsonResult GetAddress(int id)
        {
            JsonResultModel jsonModel = new JsonResultModel();
            var itemService = IoC.Resolve<IAddressService>();
            var item = itemService.Get(p => p.Id == id).Single();
            jsonModel.Result = new { Id = item.Id, Latitude = item.Latitude, Longitude = item.Longitude };
            return Json(jsonModel);
        }
    }
}