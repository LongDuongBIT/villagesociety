﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VillageSociety.Models;

namespace VillageSociety.Controllers
{
    public class GridController : Controller
    {
        public ActionResult TitlePanelPartial(GridTitlePanelModel model)
        {
            return PartialView(model);
        }

        public ActionResult MenuPartial(GridMenuModel model)
        {
            return PartialView(model);
        }
    }
} 