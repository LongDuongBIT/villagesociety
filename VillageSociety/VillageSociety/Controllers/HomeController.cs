using Core;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VillageSociety.Models;

namespace VillageSociety.Controllers
{
    public class HomeController : BaseController
    {
        [Authorize]
        public ActionResult Index()
        {
            var source = new AddressCountry
            {
                CountryCode = "TR",
                CountryName = "Turkey",
                InsertDate = DateTime.Now,
                InsertedBy = 1
            };

            var destination = new AddressCountry
            {
                Id = 12,
                CountryCode = "TR",
                CountryName = "T�rkiye",
                InsertDate = DateTime.Now.AddYears(-10),
                InsertedBy = 23
            };

            AutoMapper.Mapper.CreateMap<AddressCountry, AddressCountry>()
                .ForMember(t => t.UpdateDate, opt => opt.MapFrom(src => src.InsertDate))
                .ForMember(t => t.UpdatedBy, opt => opt.MapFrom(src => src.InsertedBy))
                .ForMember(t => t.Id, opt => opt.Ignore())
                .ForMember(t => t.InsertDate, opt => opt.Ignore())
                .ForMember(t => t.InsertedBy, opt => opt.Ignore())
                .ForMember(t => t.DeleteDate, opt => opt.Ignore())
                .ForMember(t => t.DeletedBy, opt => opt.Ignore())
                .ForMember(t => t.IsDeleted, opt => opt.Ignore());

            AutoMapper.Mapper.Map<AddressCountry, AddressCountry>(source, destination);


            //var service = Service.IoC.Resolve<ITemplateMasterService>();
            //var list = service.GetEx().ToList();

            // DXCOMMENT: Pass a data model for GridView

            //var uow = Service.IoC.Resolve<IUnitOfWork>();

            return View();    
        }
        

    
    }
}