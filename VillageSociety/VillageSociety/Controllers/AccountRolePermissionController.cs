﻿using Core;
using Service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VillageSociety.Models;

namespace VillageSociety.Controllers
{
    [PermissionAuthorize("AccountRolePermission", Permission.ReadAccountRolePermission)]
    public class AccountRolePermissionController : BaseTreeListController
    {
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("AccountRolePermission", Permission.ReadAccountRolePermission)]
        public ActionResult TreeListInit(AccountRolePermissionTreeListModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("AccountRolePermission", Permission.ReadAccountRolePermission)]
        public ActionResult TreeListPartial(AccountRolePermissionTreeListModel model)
        {
            if (model.JsonData != null)
            {
                var jo = JObject.Parse(model.JsonData);
                string command = jo.Value<string>("Command");

                if (command == "Reload" || command == "Save")
                {
                    ViewData["Command"] = command;
                }
            }

            model = (AccountRolePermissionTreeListModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("AccountRolePermission", Permission.ExportAccountRolePermission)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }
    }
}