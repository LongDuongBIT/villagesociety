﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class AddressCityController : BaseGridController
    {
        protected readonly IAddressCityService service;

        public AddressCityController(IAddressCityService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("AddressCity", Permission.ReadAddressCity)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("AddressCity", Permission.ReadAddressCity)]
        public ActionResult GridInit(AddressCityGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("AddressCity", Permission.ReadAddressCity)]
        public ActionResult GridPartial(AddressCityGridModel model)
        {
            model = (AddressCityGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(AddressCityEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AddressCity", Permission.InsertAddressCity)]
        public ActionResult InsertPartial(AddressCityEditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCity", Permission.InsertAddressCity)]
        public ActionResult Insert(AddressCityEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AddressCity();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("AddressCity", Permission.UpdateAddressCity)]
        public ActionResult UpdatePartial(AddressCityEditModel model)
        {
            var item = service.GetById(model.Id);
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCity", Permission.UpdateAddressCity)]
        public ActionResult Update(AddressCityEditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AddressCity();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCity", Permission.DeleteAddressCity)]
        public ActionResult Delete(DeleteModel model)
        {
            var jsonModel = new JsonResultModel();

            foreach (int id in model.Ids)
            {
                var item = new AddressCity { Id = id };
                service.Delete(item.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCity", Permission.InsertAddressCity, Permission.UpdateAddressCity)]
        public ActionResult Restore(RestoreModel model)
        {
            var jsonModel = new JsonResultModel(); ;

            var item = new AddressCity { Id = model.ExistingId };
            service.Restore(item.WithRestoreDefaults(), true);

            if (model.UpdatingId.HasValue)
            {
                var itemDelete = new AddressCity { Id = model.UpdatingId.Value };
                service.Delete(itemDelete.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCity", Permission.UpdateAddressCity)]
        public ActionResult Up(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Up(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCity", Permission.UpdateAddressCity)]
        public ActionResult Down(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Down(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("AddressCity", Permission.ExportAddressCity)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("AddressCity", Permission.InsertAddressCity, Permission.UpdateAddressCity)]
        public ActionResult CountryIdPartial(AddressCityEditModel model)
        {
            return PartialView(model);
        }
    }
}