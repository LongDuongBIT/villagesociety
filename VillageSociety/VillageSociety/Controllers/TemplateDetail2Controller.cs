﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class TemplateDetail2Controller : BaseGridController
    {
        protected readonly ITemplateDetail2Service service;

        public TemplateDetail2Controller(ITemplateDetail2Service service)
        {
            this.service = service;
        }

        [PermissionAuthorize("TemplateDetail2", Permission.ReadTemplateDetail2)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("TemplateDetail2", Permission.ReadTemplateDetail2)]
        public ActionResult GridInit(TemplateDetail2GridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateDetail2", Permission.ReadTemplateDetail2)]
        public ActionResult GridPartial(TemplateDetail2GridModel model)
        {
            model = (TemplateDetail2GridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(TemplateDetail2EditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateDetail2", Permission.InsertTemplateDetail2)]
        public ActionResult InsertPartial(TemplateDetail2EditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail2", Permission.InsertTemplateDetail2)]
        public ActionResult Insert(TemplateDetail2EditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new TemplateDetail2();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("TemplateDetail2", Permission.UpdateTemplateDetail2)]
        public ActionResult UpdatePartial(TemplateDetail2EditModel model)
        {
            var item = service.GetById(model.Id);
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail2", Permission.UpdateTemplateDetail2)]
        public ActionResult Update(TemplateDetail2EditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new TemplateDetail2();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail2", Permission.DeleteTemplateDetail2)]
        public ActionResult Delete(DeleteModel model)
        {
            var jsonModel = new JsonResultModel();

            foreach (int id in model.Ids)
            {
                var item = new TemplateDetail2 { Id = id };
                service.Delete(item.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail2", Permission.InsertTemplateDetail2, Permission.UpdateTemplateDetail2)]
        public ActionResult Restore(RestoreModel model)
        {
            var jsonModel = new JsonResultModel(); ;

            var item = new TemplateDetail2 { Id = model.ExistingId };
            service.Restore(item.WithRestoreDefaults(), true);

            if (model.UpdatingId.HasValue)
            {
                var itemDelete = new TemplateDetail2 { Id = model.UpdatingId.Value };
                service.Delete(itemDelete.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail2", Permission.UpdateTemplateDetail2)]
        public ActionResult Up(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Up(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail2", Permission.UpdateTemplateDetail2)]
        public ActionResult Down(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Down(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("TemplateDetail2", Permission.ExportTemplateDetail2)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("TemplateDetail2", Permission.InsertTemplateDetail2, Permission.UpdateTemplateDetail2)]
        public ActionResult MasterIdPartial(TemplateDetail2EditModel model)
        {
            return PartialView(model);
        }
    }
}