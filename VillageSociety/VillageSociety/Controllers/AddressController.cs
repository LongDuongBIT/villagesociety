﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class AddressController : BaseGridController
    {
        protected readonly IAddressService service;

        public AddressController(IAddressService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("Address", Permission.ReadAddress)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("Address", Permission.ReadAddress)]
        public ActionResult GridInit(AddressGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("Address", Permission.ReadAddress)]
        public ActionResult GridPartial(AddressGridModel model)
        {
            model = (AddressGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(AddressEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("Address", Permission.InsertAddress)]
        public ActionResult InsertPartial(AddressEditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("Address", Permission.InsertAddress)]
        public ActionResult Insert(AddressEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new Address();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("Address", Permission.UpdateAddress)]
        public ActionResult UpdatePartial(AddressEditModel model)
        {
            var item = service.GetEx(p => p.Id == model.Id).Single();
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("Address", Permission.UpdateAddress)]
        public ActionResult Update(AddressEditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new Address();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("Address", Permission.DeleteAddress)]
        public ActionResult Delete(DeleteModel model)
        {
            var jsonModel = new JsonResultModel();

            foreach (int id in model.Ids)
            {
                var item = new Address { Id = id };
                service.Delete(item.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("Address", Permission.InsertAddress, Permission.UpdateAddress)]
        public ActionResult Restore(RestoreModel model)
        {
            var jsonModel = new JsonResultModel(); ;

            var item = new Address { Id = model.ExistingId };
            service.Restore(item.WithRestoreDefaults(), true);

            if (model.UpdatingId.HasValue)
            {
                var itemDelete = new Address { Id = model.UpdatingId.Value };
                service.Delete(itemDelete.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("Address", Permission.UpdateAddress)]
        public ActionResult Up(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Up(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("Address", Permission.UpdateAddress)]
        public ActionResult Down(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Down(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("Address", Permission.ExportAddress)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("Address", Permission.InsertAddress, Permission.UpdateAddress)]
        public ActionResult CountryIdPartial(AddressEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("Address", Permission.InsertAddress, Permission.UpdateAddress)]
        public ActionResult CityIdPartial(AddressEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("Address", Permission.InsertAddress, Permission.UpdateAddress)]
        public ActionResult DistrictIdPartial(AddressEditModel model)
        {
            return PartialView(model);
        }
    }
}