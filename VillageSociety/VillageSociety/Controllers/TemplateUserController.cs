﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class TemplateUserController : BaseGridController
    {
        protected readonly ITemplateUserService service;

        public TemplateUserController(ITemplateUserService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("TemplateUser", Permission.ReadTemplateUser)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("TemplateUser", Permission.ReadTemplateUser)]
        public ActionResult GridInit(TemplateUserGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateUser", Permission.ReadTemplateUser)]
        public ActionResult GridPartial(TemplateUserGridModel model)
        {
            model = (TemplateUserGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(TemplateUserEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateUser", Permission.EditTemplateUser)]
        public ActionResult CallbackPanelPartial(TemplateUserEditModel model)
        {
            return PartialView(model);
        }


        [PermissionAuthorize("TemplateUser", Permission.EditTemplateUser)]
        public ActionResult EditPartial(TemplateUserEditModel model)
        {
            if (!model.IsNewRecord)
            {
                var item = service.GetById(model.Id);
                Mapper.DynamicMap(item, model);

                var items = service.Get(p => !p.IsDeleted && p.MasterId == item.MasterId);
                model.Ids = items.Select(p => p.UserId).ToArray();
            }
            else if (model.MasterId.HasValue)
            {
                var items = service.Get(p => !p.IsDeleted && p.MasterId == model.MasterId.Value);
                model.Ids = items.Select(p => p.UserId).ToArray();
            }

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateUser", Permission.EditTemplateUser)]
        public ActionResult Edit(TemplateUserEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                try
                {
                    service.Edit(model.MasterId.Value, model.Ids, DateTime.Now, Security.UserId);
                    service.Save();

                    jsonModel.Result = true;
                }
                catch (Exception ex)
                {
                    jsonModel.Exception = "Error";
                    jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("TemplateUser", Permission.ExportTemplateUser)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("TemplateUser", Permission.EditTemplateUser)]
        public ActionResult MasterIdPartial(TemplateUserEditModel model)
        {
            return PartialView(model);
        }

    }
}