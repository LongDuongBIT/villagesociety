﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VillageSociety.Models;

namespace VillageSociety.Controllers
{
    public class TreeListController : Controller
    {
        public ActionResult TitlePanelPartial(TreeListTitlePanelModel model)
        {
            return PartialView(model);
        }

        public ActionResult MenuPartial(TreeListMenuModel model)
        {
            return PartialView(model);
        }
    }
} 