﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class AccountUserController : BaseGridController
    {
        protected readonly IAccountUserService service;

        public AccountUserController(IAccountUserService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("AccountUser", Permission.ReadAccountUser)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("AccountUser", Permission.ReadAccountUser)]
        public ActionResult GridInit(AccountUserGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("AccountUser", Permission.ReadAccountUser)]
        public ActionResult GridPartial(AccountUserGridModel model)
        {
            model = (AccountUserGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(AccountUserEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountUser", Permission.InsertAccountUser)]
        public ActionResult InsertPartial(AccountUserEditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountUser", Permission.InsertAccountUser)]
        public ActionResult Insert(AccountUserEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountUser();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("AccountUser", Permission.UpdateAccountUser)]
        public ActionResult UpdatePartial(AccountUserEditModel model)
        {
            var item = service.GetById(model.Id);
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountUser", Permission.UpdateAccountUser)]
        public ActionResult Update(AccountUserEditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountUser();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    if (Security.UserId == model.Id)
                    {
                        Security.Reload();
                    }

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AccountUser", Permission.DeleteAccountUser)]
        public ActionResult Delete(DeleteModel model)
        {
            var jsonModel = new JsonResultModel();

            foreach (int id in model.Ids)
            {
                var item = new AccountUser { Id = id };
                service.Delete(item.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("AccountUser", Permission.ExportAccountUser)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }


        public ActionResult ShowProfilePartial(AccountUserEx model)
        {
            if (model.Username == null)
            {
                var repository = IoC.Resolve<IAccountUserService>();
                model = repository.GetEx(p => p.Id == model.Id).Single();
            }

            return PartialView(model);
        }

        [Authorize()]
        public ActionResult EditProfilePartial(EditProfileModel model)
        {
            var item = IoC.Resolve<IAccountUserService>().GetEx(p => p.Id == Security.UserId).Single();
            AutoMapper.Mapper.DynamicMap(item, model);

            return PartialView(model);
        }

        [HttpPost]
        [Authorize()]
        public ActionResult EditProfile(EditProfileModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var user = service.GetById(Security.UserId);
                if (model.CurrentPassword != user.Password)
                {
                    jsonModel.Exception = "Warning";
                    jsonModel.Result = new { Message = Translator.CurrentPasswordIsNotValid };
                    return Json(jsonModel);
                }

                var themeOrLanguageChanged = Security.ThemeId != model.ThemeId || Security.LanguageId != model.LanguageId;
                var uow = IoC.Resolve<IUnitOfWork>();
                try
                {
                    var person = new AccountPerson();

                    AutoMapper.Mapper.DynamicMap(model, person);
                    AutoMapper.Mapper.DynamicMap(model, user);
                    user.Password = model.NewPassword;

                    service.SaveProfile(person.WithUpdateDefaults(), user.WithInsertDefaults());
                    service.Save();

                    Security.Reload();

                    jsonModel.Result = new { Id = user.Id, PicturePath = Security.PicturePath ?? (Security.IsMale ? "/Content/Image/NoPhotoForMale.png" : "/Content/Image/NoPhotoForFemale.png"), ThemeOrLanguageChanged = themeOrLanguageChanged };
                }
                catch (Exception ex)
                {
                    jsonModel.Exception = "Error";
                    jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                }
            }
            else            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("AccountUser", Permission.InsertAccountUser, Permission.UpdateAccountUser)]
        public ActionResult PersonIdPartial(AccountUserEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountUser", Permission.InsertAccountUser, Permission.UpdateAccountUser)]
        public ActionResult ThemeIdPartial(AccountUserEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountUser", Permission.InsertAccountUser, Permission.UpdateAccountUser)]
        public ActionResult LanguageIdPartial(AccountUserEditModel model)
        {
            return PartialView(model);
        }


        public ActionResult EditProfileThemeIdPartial(EditProfileModel model)
        {
            return PartialView(model);
        }

        public ActionResult EditProfileLanguageIdPartial(EditProfileModel model)
        {
            return PartialView(model);
        }
    }
}