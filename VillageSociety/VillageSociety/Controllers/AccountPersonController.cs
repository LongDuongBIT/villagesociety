﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class AccountPersonController : BaseGridController
    {
        protected readonly IAccountPersonService service;

        public AccountPersonController(IAccountPersonService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("AccountPerson", Permission.ReadAccountPerson)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("AccountPerson", Permission.ReadAccountPerson)]
        public ActionResult GridInit(AccountPersonGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("AccountPerson", Permission.ReadAccountPerson)]
        public ActionResult GridPartial(AccountPersonGridModel model)
        {
            model = (AccountPersonGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(AccountPersonEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountPerson", Permission.InsertAccountPerson)]
        public ActionResult InsertPartial(AccountPersonEditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountPerson", Permission.InsertAccountPerson)]
        public ActionResult Insert(AccountPersonEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountPerson();
                try
                {
                    Mapper.DynamicMap(model, item);

                    if (!Security.IsSystemAdmin)
                    {
                        item.VillageId = Security.VillageId;
                    }

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id, Texts = new string[] { item.FirstName, item.LastName } };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("AccountPerson", Permission.UpdateAccountPerson)]
        public ActionResult UpdatePartial(AccountPersonEditModel model)
        {
            var item = service.GetById(model.Id);
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountPerson", Permission.UpdateAccountPerson)]
        public ActionResult Update(AccountPersonEditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountPerson();
                try
                {
                    Mapper.DynamicMap(model, item);

                    if (!Security.IsSystemAdmin)
                    {
                        item.VillageId = Security.VillageId;
                    }

                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id, Texts = new string[] { item.FirstName, item.LastName } };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("AccountPerson", Permission.ExportAccountPerson)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("AccountPerson", Permission.InsertAccountPerson, Permission.UpdateAccountPerson)]
        public ActionResult VillageIdPartial(AccountPersonEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountPerson", Permission.InsertAccountPerson, Permission.UpdateAccountPerson)]
        public ActionResult FatherPartial(AccountPersonEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountPerson", Permission.InsertAccountPerson, Permission.UpdateAccountPerson)]
        public ActionResult MotherPartial(AccountPersonEditModel model)
        {
            return PartialView(model);
        }

    }
}