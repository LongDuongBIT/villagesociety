﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class TemplatePersonController : BaseGridController
    {
        protected readonly ITemplatePersonService service;

        public TemplatePersonController(ITemplatePersonService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("TemplatePerson", Permission.ReadTemplatePerson)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("TemplatePerson", Permission.ReadTemplatePerson)]
        public ActionResult GridInit(TemplatePersonGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("TemplatePerson", Permission.ReadTemplatePerson)]
        public ActionResult GridPartial(TemplatePersonGridModel model)
        {
            model = (TemplatePersonGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(TemplatePersonEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplatePerson", Permission.EditTemplatePerson)]
        public ActionResult CallbackPanelPartial(TemplatePersonEditModel model)
        {
            return PartialView(model);
        }


        [PermissionAuthorize("TemplatePerson", Permission.EditTemplatePerson)]
        public ActionResult EditPartial(TemplatePersonEditModel model)
        {
            if (!model.IsNewRecord)
            {
                var item = service.GetById(model.Id);
                Mapper.DynamicMap(item, model);

                var items = service.Get(p => !p.IsDeleted && p.MasterId == item.MasterId);
                model.Ids = items.Select(p => p.PersonId).ToArray();
            }
            else if (model.MasterId.HasValue)
            {
                var items = service.Get(p => !p.IsDeleted && p.MasterId == model.MasterId.Value);
                model.Ids = items.Select(p => p.PersonId).ToArray();
            }

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("TemplatePerson", Permission.EditTemplatePerson)]
        public ActionResult Edit(TemplatePersonEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                try
                {
                    service.Edit(model.MasterId.Value, model.Ids, DateTime.Now, Security.UserId);
                    service.Save();

                    jsonModel.Result = true;
                }
                catch (Exception ex)
                {
                    jsonModel.Exception = "Error";
                    jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("TemplatePerson", Permission.ExportTemplatePerson)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("TemplatePerson", Permission.EditTemplatePerson)]
        public ActionResult MasterIdPartial(TemplatePersonEditModel model)
        {
            return PartialView(model);
        }

    }
}