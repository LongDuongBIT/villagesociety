using Core;
using Service;
using Internationalization;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VillageSociety.Models;
using WebMatrix.WebData;

namespace VillageSociety.Controllers
{
    public class AccountController : BaseController
    {
        public ActionResult Login(string returnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
            {
                return SignOut();
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {

            if (ModelState.IsValid)// && CaptchaExtension.GetIsValid("cptLogin"))
            {
                if (Membership.ValidateUser(model.Username, model.Password))
                {
                    FormsAuthentication.RedirectFromLoginPage(model.Username, model.RememberMe.HasValue && model.RememberMe.Value);
                }

                ModelState.AddModelError("TheUsernameOrPasswordProvidedIsIncorrect", Translator.TheUsernameOrPasswordProvidedIsIncorrect);
            }

            return View(model);
        }

        public ActionResult LoginPartial()
        {
            return PartialView();
        }

        public ActionResult CaptchaPartial(LoginModel model)
        {
            return PartialView(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SignOut()
        {
            var cacheKey = string.Format("UserData_{0}", Security.Username);
            HttpRuntime.Cache.Remove(cacheKey);
            cacheKey = string.Format("UserRoles_{0}", Security.Username);
            HttpRuntime.Cache.Remove(cacheKey);

            //HttpRuntime.Close();
            Session.Abandon();
            FormsAuthentication.SignOut();

            return RedirectToAction("Login", "Account");
        }

        public ActionResult HeaderPartial()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult HeaderPartial(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "ChangeTheme")
            {
                int id = jo.Value<int>("Id");
                var themeName = IoC.Resolve<IAccountThemeService>().GetById(id).ThemeName;

                string activePageController = jo.Value<string>("ActivePageController");
                string activePageAction = jo.Value<string>("ActivePageAction");

                var service = IoC.Resolve<IAccountUserService>();
                var user = service.GetById(Security.UserId);
                user.ThemeId = id;
                service.Update(user.WithUpdateDefaults());
                service.Save();

                Security.ThemeId = id;
                Security.ThemeName = themeName;

                return RedirectToAction(activePageAction, activePageController);
            }
            else if (submit == "ChangeLanguage")
            {
                int id = jo.Value<int>("Id");
                var languageCode = IoC.Resolve<IAccountLanguageService>().GetById(id).LanguageCode;

                string activePageController = jo.Value<string>("ActivePageController");
                string activePageAction = jo.Value<string>("ActivePageAction");

                var service = IoC.Resolve<IAccountUserService>();
                var user = service.GetById(Security.UserId);
                user.LanguageId = id;
                service.Update(user.WithUpdateDefaults());
                service.Save();

                Security.LanguageId = id;
                Security.LanguageCode = languageCode;

                Session["trvLeftData"] = null;

                return RedirectToAction(activePageAction, activePageController);
            }
            else if (submit == "RedirectToAction")
            {
                string activePageController = jo.Value<string>("ActivePageController");
                string activePageAction = jo.Value<string>("ActivePageAction");

                return RedirectToAction(activePageAction, activePageController);
            }
            else if (submit == "SignOut")
            {
                return SignOut();
            }

            return null;
        }

        public ActionResult HeaderMenuPartial()
        {
            return PartialView();
        }

        public ActionResult LeftPartial()
        {
            return PartialView();
        }

        public ActionResult LeftTreePartial()
        {
            return PartialView();
        }

        public static void CreateChildren(TreeViewVirtualModeCreateChildrenEventArgs e)
        {
            int? parentId = string.IsNullOrEmpty(e.NodeName) ? (int?)null : Convert.ToInt32(e.NodeName);

            var service = IoC.Resolve<ISystemMenuService>();
            if (System.Web.HttpContext.Current.Session["trvLeftData"] == null)
            {
                System.Web.HttpContext.Current.Session["trvLeftData"] = service.ListOfLeftTree(p => !p.IsDeleted, Security.UserId);
            }
            var listMenu = (List<SystemMenuEx>)System.Web.HttpContext.Current.Session["trvLeftData"];
            var list = listMenu.Where(p => p.ParentId == parentId);

            List<TreeViewVirtualNode> children = new List<TreeViewVirtualNode>();
            foreach (var item in list)
            {
                string navigateUrl = item.NavigateUrl ?? (item.ControllerName != null ? "~/" + item.ControllerName + "/" + item.ActionName : null);
                TreeViewVirtualNode childNode = new TreeViewVirtualNode(item.Id.ToString(), item.MenuName, item.IconPath, navigateUrl);
                childNode.IsLeaf = item.IsLeaf;
                children.Add(childNode);
            }

            e.Children = children;
        }

    }
}