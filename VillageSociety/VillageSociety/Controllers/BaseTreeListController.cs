﻿using DevExpress.Web.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VillageSociety.Models;

namespace VillageSociety.Controllers
{
    public class BaseTreeListController : BaseController
    {
        public BaseTreeListController()
        {
        }

        [NonAction]
        public virtual ActionResult Export(string json)
        {
            var jo = JObject.Parse(json);
            string export = jo.Value<string>("Export");
            string modelName = jo.Value<string>("ModelName");
            TreeListModel treeListModel = (TreeListModel)Session[modelName];

            if (export == "Xls")
            {
                return TreeListExtension.ExportToXls(treeListModel.Settings, treeListModel.Data);
            }
            else if (export == "Xlsx")
            {
                return TreeListExtension.ExportToXlsx(treeListModel.Settings, treeListModel.Data);
            }
            else if (export == "Pdf")
            {
                return TreeListExtension.ExportToPdf(treeListModel.Settings, treeListModel.Data);
            }
            else if (export == "Rtf")
            {
                return TreeListExtension.ExportToRtf(treeListModel.Settings, treeListModel.Data);
            }

            return null;
        }
    }
}