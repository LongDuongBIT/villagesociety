﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class TemplateMasterController : BaseGridController
    {
        protected readonly ITemplateMasterService service;

        public TemplateMasterController(ITemplateMasterService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("TemplateMaster", Permission.ReadTemplateMaster)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("TemplateMaster", Permission.ReadTemplateMaster)]
        public ActionResult GridInit(TemplateMasterGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.ReadTemplateMaster)]
        public ActionResult GridPartial(TemplateMasterGridModel model)
        {
            model = (TemplateMasterGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster)]
        public ActionResult InsertPartial(TemplateMasterEditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster)]
        public ActionResult Insert(TemplateMasterEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new TemplateMaster();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults(), model.TemplatePersons, model.TemplateUsers);
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("TemplateMaster", Permission.UpdateTemplateMaster)]
        public ActionResult UpdatePartial(TemplateMasterEditModel model)
        {
            var item = service.GetEx(p => p.Id == model.Id).Single();
            Mapper.DynamicMap(item, model);

            var templatePersonService = IoC.Resolve<ITemplatePersonService>();
            model.TemplatePersons = templatePersonService.Get(p => !p.IsDeleted && p.MasterId == item.Id).Select(p => p.PersonId).ToArray();

            var templateUserService = IoC.Resolve<ITemplateUserService>();
            model.TemplateUsers = templateUserService.Get(p => !p.IsDeleted && p.MasterId == item.Id).Select(p => p.UserId).ToArray();

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateMaster", Permission.UpdateTemplateMaster)]
        public ActionResult Update(TemplateMasterEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new TemplateMaster();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults(), model.TemplatePersons, model.TemplateUsers);
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("TemplateMaster", Permission.ExportTemplateMaster)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult ComboBoxPersonIdPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult GridLookupPersonIdPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult ComboBoxCountryIdPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult ComboBoxCityIdPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult ComboBoxDistrictIdPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult ComboBoxAddressIdPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public JsonResult GetAddress(int id)
        {
            JsonResultModel jsonModel = new JsonResultModel();
            var item = IoC.Resolve<IAddressService>().Get(p => p.Id == id).Single();
            jsonModel.Result = new { Id = item.Id, Latitude = item.Latitude, Longitude = item.Longitude };
            return Json(jsonModel);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult GridLookupCountryIdPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult GridLookupCityIdPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult GridLookupDistrictIdPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult GridLookupAddressIdPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult TemplatePersonsPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult TemplateUsersPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateMaster", Permission.InsertTemplateMaster, Permission.UpdateTemplateMaster)]
        public ActionResult HtmlColumnPartial(TemplateMasterEditModel model)
        {
            return PartialView(model);
        }
    }
}