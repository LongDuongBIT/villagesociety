﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class AccountRoleLanguageController : BaseGridController
    {
        protected readonly IAccountRoleLanguageService service;

        public AccountRoleLanguageController(IAccountRoleLanguageService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("AccountRoleLanguage", Permission.ReadAccountRoleLanguage)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("AccountRoleLanguage", Permission.ReadAccountRoleLanguage)]
        public ActionResult GridInit(AccountRoleLanguageGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("AccountRoleLanguage", Permission.ReadAccountRoleLanguage)]
        public ActionResult GridPartial(AccountRoleLanguageGridModel model)
        {
            model = (AccountRoleLanguageGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(AccountRoleLanguageEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountRoleLanguage", Permission.InsertAccountRoleLanguage)]
        public ActionResult InsertPartial(AccountRoleLanguageEditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountRoleLanguage", Permission.InsertAccountRoleLanguage)]
        public ActionResult Insert(AccountRoleLanguageEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountRoleLanguage();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("AccountRoleLanguage", Permission.UpdateAccountRoleLanguage)]
        public ActionResult UpdatePartial(AccountRoleLanguageEditModel model)
        {
            var item = service.GetById(model.Id);
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountRoleLanguage", Permission.UpdateAccountRoleLanguage)]
        public ActionResult Update(AccountRoleLanguageEditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountRoleLanguage();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AccountRoleLanguage", Permission.DeleteAccountRoleLanguage)]
        public ActionResult Delete(DeleteModel model)
        {
            var jsonModel = new JsonResultModel();

            foreach (int id in model.Ids)
            {
                var item = new AccountRoleLanguage { Id = id };
                service.Delete(item.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("AccountRoleLanguage", Permission.ExportAccountRoleLanguage)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("AccountRoleLanguage", Permission.InsertAccountRoleLanguage, Permission.UpdateAccountRoleLanguage)]
        public ActionResult GetRoleValues(int id)
        {
            var jsonModel = new JsonResultModel();

            var roleService = IoC.Resolve<IAccountRoleService>();
            var role = roleService.GetById(id);
            jsonModel.Result = new { RoleName = role.RoleName, RoleDescription = role.RoleDescription };

            return Json(jsonModel);
        }

        [PermissionAuthorize("AccountRoleLanguage", Permission.InsertAccountRoleLanguage, Permission.UpdateAccountRoleLanguage)]
        public ActionResult RoleIdPartial(AccountRoleLanguageEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountRoleLanguage", Permission.InsertAccountRoleLanguage, Permission.UpdateAccountRoleLanguage)]
        public ActionResult LanguageIdPartial(AccountRoleLanguageEditModel model)
        {
            return PartialView(model);
        }
    }
}