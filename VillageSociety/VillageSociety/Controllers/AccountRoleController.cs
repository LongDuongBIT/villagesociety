﻿using AutoMapper;
using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;

namespace VillageSociety.Controllers
{
    public class AccountRoleController : BaseGridController
    {
        protected readonly IAccountRoleService service;

        public AccountRoleController(IAccountRoleService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("AccountRole", Permission.ReadAccountRole)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("AccountRole", Permission.ReadAccountRole)]
        public ActionResult GridInit(AccountRoleGridModel model)
        {
            Session[model.Name] = model;

            return PartialView(model);
        }

        [PermissionAuthorize("AccountRole", Permission.ReadAccountRole)]
        public ActionResult GridPartial(AccountRoleGridModel model)
        {
            model = (AccountRoleGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(AccountRoleEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AccountRole", Permission.InsertAccountRole)]
        public ActionResult InsertPartial(AccountRoleEditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountRole", Permission.InsertAccountRole)]
        public ActionResult Insert(AccountRoleEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountRole();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("AccountRole", Permission.UpdateAccountRole)]
        public ActionResult UpdatePartial(AccountRoleEditModel model)
        {
            var item = service.GetById(model.Id);
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AccountRole", Permission.UpdateAccountRole)]
        public ActionResult Update(AccountRoleEditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AccountRole();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AccountRole", Permission.DeleteAccountRole)]
        public ActionResult Delete(DeleteModel model)
        {
            object jsonModel = null;

            var itemService = IoC.Resolve<IAccountRoleService>();

            foreach (int id in model.Ids)
            {
                var item = new AccountRole { Id = id };
                itemService.Delete(item.WithDeleteDefaults());
            }

            itemService.Save();
            jsonModel = new { Result = true };

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AccountRole", Permission.InsertAccountRole, Permission.UpdateAccountRole)]
        public ActionResult Restore(RestoreModel model)
        {
            object jsonModel = null;

            var itemService = IoC.Resolve<IAccountRoleService>();
            var item = new AccountRole { Id = model.ExistingId };
            itemService.Restore(item.WithRestoreDefaults(), true);

            if (model.UpdatingId.HasValue)
            {
                var itemDelete = new AccountRole { Id = model.UpdatingId.Value };
                itemService.Delete(itemDelete.WithDeleteDefaults());
            }

            itemService.Save();
            jsonModel = new { Result = true };

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AccountRole", Permission.UpdateAccountRole)]
        public ActionResult Up(int id1, int? id2)
        {
            JsonResultModel jsonModel = new JsonResultModel();
            var repository = IoC.Resolve<IAccountRoleService>();
            jsonModel.Result = repository.Up(id1, id2);
            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AccountRole", Permission.UpdateAccountRole)]
        public ActionResult Down(int id1, int? id2)
        {
            JsonResultModel jsonModel = new JsonResultModel();
            var repository = IoC.Resolve<IAccountRoleService>();
            jsonModel.Result = repository.Down(id1, id2);
            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("AccountRole", Permission.ExportAccountRole)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }
    }
}