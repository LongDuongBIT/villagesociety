﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class TemplateDetail1Controller : BaseGridController
    {
        protected readonly ITemplateDetail1Service service;

        public TemplateDetail1Controller(ITemplateDetail1Service service)
        {
            this.service = service;
        }

        [PermissionAuthorize("TemplateDetail1", Permission.ReadTemplateDetail1)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("TemplateDetail1", Permission.ReadTemplateDetail1)]
        public ActionResult GridInit(TemplateDetail1GridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateDetail1", Permission.ReadTemplateDetail1)]
        public ActionResult GridPartial(TemplateDetail1GridModel model)
        {
            model = (TemplateDetail1GridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(TemplateDetail1EditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("TemplateDetail1", Permission.InsertTemplateDetail1)]
        public ActionResult InsertPartial(TemplateDetail1EditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail1", Permission.InsertTemplateDetail1)]
        public ActionResult Insert(TemplateDetail1EditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new TemplateDetail1();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("TemplateDetail1", Permission.UpdateTemplateDetail1)]
        public ActionResult UpdatePartial(TemplateDetail1EditModel model)
        {
            var item = service.GetById(model.Id);
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail1", Permission.UpdateTemplateDetail1)]
        public ActionResult Update(TemplateDetail1EditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new TemplateDetail1();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("TemplateDetail1", Permission.DeleteTemplateDetail1)]
        public ActionResult Delete(DeleteModel model)
        {
            var jsonModel = new JsonResultModel();

            foreach (int id in model.Ids)
            {
                var item = new TemplateDetail1 { Id = id };
                service.Delete(item.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("TemplateDetail1", Permission.ExportTemplateDetail1)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }

        [PermissionAuthorize("TemplateDetail1", Permission.InsertTemplateDetail1, Permission.UpdateTemplateDetail1)]
        public ActionResult MasterIdPartial(TemplateDetail1EditModel model)
        {
            return PartialView(model);
        }
    }
}