﻿using DevExpress.Web.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VillageSociety.Models;

namespace VillageSociety.Controllers
{
    public class BaseGridController : BaseController
    {
        public BaseGridController()
        {
        }

        [NonAction]
        public virtual ActionResult Export(string json)
        {
            var jo = JObject.Parse(json);
            string export = jo.Value<string>("Export");
            string modelName = jo.Value<string>("ModelName");
            GridModel gridModel = (GridModel)Session[modelName];

            if (export == "Xls")
            {
                return GridViewExtension.ExportToXls(gridModel.Settings, gridModel.Data().AsParallel());
            }
            else if (export == "Xlsx")
            {
                return GridViewExtension.ExportToXlsx(gridModel.Settings, gridModel.Data().AsParallel());
            }
            else if (export == "Pdf")
            {
                return GridViewExtension.ExportToPdf(gridModel.Settings, gridModel.Data().AsParallel());
            }
            else if (export == "Csv")
            {
                return GridViewExtension.ExportToCsv(gridModel.Settings, gridModel.Data().AsParallel());
            }
            else if (export == "Rtf")
            {
                return GridViewExtension.ExportToRtf(gridModel.Settings, gridModel.Data().AsParallel());
            }

            return null;
        }
    }
}