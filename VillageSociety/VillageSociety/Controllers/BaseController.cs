﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace VillageSociety.Controllers
{
    public class BaseController : Controller
    {
        public string ControllerName
        {
            get
            {
                return (string)ControllerContext.RouteData.Values["controller"];
            }
        }
        public string ActionName
        {
            get
            {
                return (string)ControllerContext.RouteData.Values["controller"];
            }
        }
    }
}