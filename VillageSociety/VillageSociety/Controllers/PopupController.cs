﻿using DevExpress.Web.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VillageSociety.Models;

namespace VillageSociety.Controllers
{
    public class PopupController : Controller
    {
        public ActionResult HtmlPartial(HtmlPopupModel model)
        {
            return PartialView(model);
        }

        public ActionResult ActionPartial(ActionPopupModel model)
        {
            return PartialView(model);
        }

        public ActionResult ActionRoutePartial(ActionPopupModel model)
        {
            if (model.ModelData == null)
            {
                if (model.ModelFullName != null)
                {
                    model.ModelData = JsonConvert.DeserializeObject(model.ModelJson, Type.GetType(model.ModelFullName));
                }
            }

            return PartialView("ActionPartial", model);
        }

        public ActionResult SelectSingleFilePartial(SelectSingleFileModel model)
        {
            if (!Directory.Exists(model.RootFolder))
            {
                string[] splits = model.RootFolder.Split('/');

                for (int i = splits.Length - 2; i >= 0; i--)
                {
                    var field = splits[i + 1];
                    var entity = splits[i];

                    var type = Type.GetType("Entity." + entity + ", Entity");

                    if (type != null)
                    {
                        var pi = type.GetProperties().SingleOrDefault(p => p.Name == field);

                        if (pi != null)
                        {
                            Directory.CreateDirectory(Server.MapPath(model.RootFolder));
                            break;
                        }
                    }
                }
            }

            Session[model.Name] = model;
            return PartialView(model);
        }

        public ActionResult SelectSingleFileManagerPartial(SelectSingleFileModel model)
        {
            model = (SelectSingleFileModel)Session[model.Name];
            return PartialView(model);
        }

        public FileStreamResult SelectSingleFileManagerDownloadRoute(SelectSingleFileModel model)
        {
            return FileManagerExtension.DownloadFiles("fmn" + model.Name, model.RootFolder);
        }
    }
}