﻿using Core;
using Service;
using Internationalization;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VillageSociety.Models;
using AutoMapper;

namespace VillageSociety.Controllers
{
    public class AddressCountryController : BaseGridController
    {
        protected readonly IAddressCountryService service;

        public AddressCountryController(IAddressCountryService service)
        {
            this.service = service;
        }

        [PermissionAuthorize("AddressCountry", Permission.ReadAddressCountry)]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorize("AddressCountry", Permission.ReadAddressCountry)]
        public ActionResult GridInit(AddressCountryGridModel model)
        {
            Session[model.Name] = model;
            return PartialView(model);
        }

        [PermissionAuthorize("AddressCountry", Permission.ReadAddressCountry)]
        public ActionResult GridPartial(AddressCountryGridModel model)
        {
            model = (AddressCountryGridModel)Session[model.Name];
            return PartialView(model);
        }

        public ActionResult DetailPartial(AddressCountryEditModel model)
        {
            return PartialView(model);
        }

        [PermissionAuthorize("AddressCountry", Permission.InsertAddressCountry)]
        public ActionResult InsertPartial(AddressCountryEditModel model)
        {
            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCountry", Permission.InsertAddressCountry)]
        public ActionResult Insert(AddressCountryEditModel model)
        {
            JsonResultModel jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AddressCountry();
                try
                {
                    Mapper.DynamicMap(model, item);

                    service.Insert(item.WithInsertDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch (Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [PermissionAuthorize("AddressCountry", Permission.UpdateAddressCountry)]
        public ActionResult UpdatePartial(AddressCountryEditModel model)
        {
            var item = service.GetById(model.Id);
            Mapper.DynamicMap(item, model);

            return PartialView("EditPartial", model);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCountry", Permission.UpdateAddressCountry)]
        public ActionResult Update(AddressCountryEditModel model)
        {
            var jsonModel = new JsonResultModel();

            if (ModelState.IsValid)
            {
                var item = new AddressCountry();
                try
                {
                    Mapper.DynamicMap(model, item);
                    service.Update(item.WithUpdateDefaults());
                    service.Save();

                    jsonModel.Result = new { Id = item.Id };
                }
                catch(Exception ex)
                {
                    jsonModel = service.ExceptionToJsonResult(ex, item, model.IsNewRecord);
                    if (jsonModel == null)
                    {
                        jsonModel.Exception = "Error";
                        jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ExceptionError(ex) };
                    }
                }
            }
            else
            {
                jsonModel.Exception = "Error";
                jsonModel.Result = new { Message = Translator.PleaseTellTheProgrammersAboutTheError + "<br />" + HtmlStringFactory.ModelStateError(ModelState) };
            }

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCountry", Permission.DeleteAddressCountry)]
        public ActionResult Delete(DeleteModel model)
        {
            var jsonModel = new JsonResultModel();

            foreach (int id in model.Ids)
            {
                var item = new AddressCountry { Id = id };
                service.Delete(item.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCountry", Permission.InsertAddressCountry, Permission.UpdateAddressCountry)]
        public ActionResult Restore(RestoreModel model)
        {
            var jsonModel = new JsonResultModel(); ;

            var item = new AddressCountry { Id = model.ExistingId };
            service.Restore(item.WithRestoreDefaults(), true);

            if (model.UpdatingId.HasValue)
            {
                var itemDelete = new AddressCountry { Id = model.UpdatingId.Value };
                service.Delete(itemDelete.WithDeleteDefaults());
            }

            service.Save();
            jsonModel.Result = true;

            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCountry", Permission.UpdateAddressCountry)]
        public ActionResult Up(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Up(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        [HttpPost]
        [PermissionAuthorize("AddressCountry", Permission.UpdateAddressCountry)]
        public ActionResult Down(int id1, int? id2)
        {
            var jsonModel = new JsonResultModel();
            jsonModel.Result = service.Down(id1, id2);
            service.Save();
            return Json(jsonModel);
        }

        public ActionResult Submit(string json)
        {
            var jo = JObject.Parse(json);
            string submit = jo.Value<string>("Submit");

            if (submit == "Export")
            {
                return Export(json);
            }

            return null;
        }

        [PermissionAuthorize("AddressCountry", Permission.ExportAddressCountry)]
        public override ActionResult Export(string json)
        {
            return base.Export(json);
        }
    }
}