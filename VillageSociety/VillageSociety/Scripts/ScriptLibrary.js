﻿function Test123(values) {
    alert('ok');
}

function grvSelectionAffectsMenuButtons(grv, mnu) {
    var selectedCount = grv.GetSelectedRowCount();
    var mniEdit = mnu.GetItemByName('mniEdit');
    if (mniEdit != undefined) {
        mniEdit.SetEnabled(selectedCount == 1);
    }

    var mniDelete = mnu.GetItemByName('mniDelete');
    if (mniDelete != undefined) {
        mniDelete.SetEnabled(selectedCount >= 1);
    }

    var mniUp = mnu.GetItemByName('mniUp');
    if (mniUp != undefined) {
        mniUp.SetEnabled(selectedCount == 1 || selectedCount == 2);
    }

    var mniDown = mnu.GetItemByName('mniDown');
    if (mniDown != undefined) {
        mniDown.SetEnabled(selectedCount == 1 || selectedCount == 2);
    }
}

function cmbClearAddAndSetItem(cmbOrName, texts, value, enabled) {
    var cmb = (typeof cmbOrName) == 'string' ? window[cmbOrName] : cmbOrName;
    cmb.ClearItems();
    cmb.AddItem(texts, value);
    cmb.SetValue(value);
    cmb.SetEnabled(enabled);
}

function gluGetValue(glu) {
    var value = glu.GetValue();
    if (value) {
        var grv = glu.GetGridView();
        value = grv.GetRowKey(grv.GetFocusedRowIndex());
    }

    return value;
}

function dxSetSize(dx, width, height) {
    dx.SetWidth(width);
    dx.SetHeight(height);
}

function EditFormAjaxPost(params)
{
    $.ajax({
        url: params.Url,
        type: 'post',
        data: params.Data,
        datatype: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (jsonResult) {
            if (jsonResult.Exception == null && jsonResult.Result) {
                params.SuccessMethod(jsonResult);
            } else if (jsonResult.Exception == 'UniqueKeyExceptionWhileInserting') {
                ShowOkCancel({
                    Message: Translator.UniqueKeyExceptionWhileInserting,
                    OkScript: function(s1, e1) {
                        var restoreModel = {
                            ExistingId: jsonResult.Result.ExistingId,
                            UpdatingId: params.Id
                        };

                        $.ajax({
                            url: params.RestoreUrl,
                            type: 'post',
                            data: JSON.stringify(restoreModel),
                            datatype: 'json',
                            contentType: 'application/json; charset=utf-8',
                            success: function (jsonResult) {
                                pucMessage.Hide();
                                params.SuccessMethod(jsonResult);
                            },
                            error: function (xhr) {
                                alert('Unexpected Error: ' + xhr.statusText);
                            }
                        });
                    }
                });
            } else if (jsonResult.Exception == 'UniqueKeyExceptionWhileUpdating') {
                ShowOkCancel({
                    Message: Translator.UniqueKeyExceptionWhileUpdating,
                    OkScript: function(s1, e1) {
                        var restoreModel = {
                            ExistingId: jsonResult.Result.ExistingId,
                            UpdatingId: params.Id
                        };

                        $.ajax({
                            url: params.RestoreUrl,
                            type: 'post',
                            data: JSON.stringify(restoreModel),
                            datatype: 'json',
                            contentType: 'application/json; charset=utf-8',
                            success: function (jsonResult) {
                                pucMessage.Hide();
                                params.SuccessMethod(jsonResult);
                            },
                            error: function (xhr) {
                                alert('Unexpected Error: ' + xhr.statusText);
                            }
                        });
                    }
                });
            } else if (jsonResult.Exception == 'UniqueKeyExceptionExistingRecord') {
                ShowWarning({
                    Message: Translator.UniqueKeyExceptionExistingRecord
                });
            } else if (jsonResult.Exception == 'Warning') {
                ShowWarning({
                    Message: jsonResult.Result.Message,
                });
            } else if (jsonResult.Exception == 'Error') {
                ShowError({
                    Message: jsonResult.Result.Message,
                });
            } else if (jsonResult.Exception == 'Help') {
                ShowHelp({
                    Message: jsonResult.Result.Message,
                });
            } else if (jsonResult.Exception == 'OkCancel') {
                ShowOkCancel({
                    Message: jsonResult.Result.Message,
                    OkScript: eval(jsonResult.Result.OkScript)
                });
            } else {
                ShowForbidden({
                    Message: Translator.YouAreNotAuthorizedToProcess
                });
            }
        },
        error: function (xhr) {
            ShowError({
                HeaderText: Translator.UnexpectedError,
                Message: Translator.PleaseTellTheProgrammersAboutTheError + '<br />' + xhr.statusText
            });
        }
    });
}

function pgcResize(pgc, visible) {
    for (var i = 0; i < pgc.cpResize.Sources.length; i++) {
        var source = window[pgc.cpResize.Sources[i]];
        if (source) {
            if (visible) {
                var destination = $('#' + pgc.cpResize.Destinations[i]);
                source.SetHeight(destination.height());
            }
            source.SetVisible(visible);
        }
    }
}

function ShowAddFormInPopup(params) {
    var modelJson = {
        Name: params.Name,
        IsNewRecord: true,
        SuccessMethod: params.SuccessMethod.toString(),
        CancelMethod: params.CancelMethod.toString(),
        Width: params.Width,
        Height: params.Height
    };

    var data = {
        Name: params.Name,
        ControllerName: params.ControllerName,
        ActionName: params.ActionName,
        ModelFullName: params.EditModelFullName,
        ModelJson: JSON.stringify(modelJson)
    };

    params.PopupControl.SetWidth(params.Width);
    params.PopupControl.SetHeight(params.Height);

    params.PopupControl.EndCallback.ClearHandlers();
    params.PopupControl.EndCallback.AddHandler(function (s, e) {
        s.Show();

        var btnSave = window['btnSave' + modelJson.Name];
        
        var pgc = window['pgcDetail' + modelJson.Name];
        pgc.ActiveTabChanging.ClearHandlers();
        pgc.ActiveTabChanging.AddHandler(function (s1, e1) {
            var tab = e1.tab;
            var formValid = $('#frm' + modelJson.Name + 'Edit').validate().form();
            if (formValid) {
                ShowOkCancel({
                    Message: Translator.TheDataYouEnterWillBeRecorded,
                    OkScript: function (s2, e2) {
                        pucMessage.Hide();

                        var formValid = $('#frm' + modelJson.Name + 'Edit').validate().form();
                        if (formValid)
                        {
                            EditFormAjaxPost({
                                Url: '/' + params.ControllerName + '/Insert',
                                RestoreUrl: '/' + params.ControllerName + '/Restore',
                                Data: JSON.stringify(btnSave.cpData()),
                                Id: null,
                                SuccessMethod: function(jsonResult) {
                                    params.Id = jsonResult.Result.Id;
                                    params.ActiveTab = tab;
                                    ShowEditFormInPopup(params);
                                }
                            });
                        }
                    }
                });
            }

            e1.cancel = true;
        });

    });

    params.PopupControl.Shown.ClearHandlers();
    params.PopupControl.Shown.AddHandler(function (s, e) {
        var pgc = window['pgcDetail' + modelJson.Name];
        s.SetWidth(pgc.cpResize.Width);
        s.SetHeight(pgc.cpResize.Height);
        pgcResize(pgc, true);
    });

    if (params.AddShownMethod != null) {
        params.PopupControl.Shown.AddHandler(params.AddShownMethod);
    }

    params.PopupControl.BeforeResizing.ClearHandlers();
    params.PopupControl.BeforeResizing.AddHandler(function (s, e) {
        var pgc = window['pgcDetail' + modelJson.Name];
        pgcResize(pgc, false);

        pgc.SetVisible(false);
    });

    params.PopupControl.AfterResizing.ClearHandlers();
    params.PopupControl.AfterResizing.AddHandler(function (s, e) {
        var pgc = window['pgcDetail' + modelJson.Name];
        pgcResize(pgc, false);

        pgc.SetVisible(true);
    });

    params.PopupControl.Resize.ClearHandlers();
    params.PopupControl.Resize.AddHandler(function (s, e) {
        var pgc = window['pgcDetail' + modelJson.Name];
        pgcResize(pgc, true);

        pgc.SetVisible(true);
    });
    
    params.PopupControl.SetHeaderImageUrl(params.AddHeaderImageUrl);
    params.PopupControl.SetHeaderText(params.AddHeaderText);

    params.PopupControl.PerformCallback(data);
}

function ShowEditFormInPopup(params) {
    var modelJson = {
        Name: params.Name + params.Id.toString(),
        IsNewRecord: false,
        SuccessMethod: params.SuccessMethod.toString(),
        CancelMethod: params.CancelMethod.toString(),
        Id: params.Id,
        Width: params.Width,
        Height: params.Height
    };

    var data = {
        Name: params.Name,
        ControllerName: params.ControllerName,
        ActionName: params.ActionName,
        ModelFullName: params.EditModelFullName,
        ModelJson: JSON.stringify(modelJson)
    };

    params.PopupControl.SetWidth(params.Width);
    params.PopupControl.SetHeight(params.Height);

    params.PopupControl.EndCallback.ClearHandlers();
    params.PopupControl.EndCallback.AddHandler(function (s, e) {
        s.Show();

        var pgc = window['pgcDetail' + modelJson.Name];
        pgc.ActiveTabChanged.ClearHandlers();
        pgc.ActiveTabChanged.AddHandler(function (s1, e1) {
            s.SetWidth(s1.cpResize.Width);
            s.SetHeight(s1.cpResize.Height);

            pgcResize(s1, true);
        });

        if (params.ActiveTab != null) {
            pgc.SetActiveTab(params.ActiveTab);
        }
    });


    params.PopupControl.Shown.ClearHandlers();
    params.PopupControl.Shown.AddHandler(function (s, e) {
        var pgc = window['pgcDetail' + modelJson.Name];
        s.SetWidth(pgc.cpResize.Width);
        s.SetHeight(pgc.cpResize.Height);
        pgcResize(pgc, true);
    });

    if (params.EditShownMethod != null) {
        params.PopupControl.Shown.AddHandler(params.EditShownMethod);
    }

    params.PopupControl.BeforeResizing.ClearHandlers();
    params.PopupControl.BeforeResizing.AddHandler(function (s, e) {
        var pgc = window['pgcDetail' + modelJson.Name];
        pgcResize(pgc, false);

        pgc.SetVisible(false);
    });

    params.PopupControl.AfterResizing.ClearHandlers();
    params.PopupControl.AfterResizing.AddHandler(function (s, e) {
        var pgc = window['pgcDetail' + modelJson.Name];
        pgcResize(pgc, false);

        pgc.SetVisible(true);
    });

    params.PopupControl.Resize.ClearHandlers();
    params.PopupControl.Resize.AddHandler(function (s, e) {
        var pgc = window['pgcDetail' + modelJson.Name];
        pgcResize(pgc, true);

        pgc.SetVisible(true);
    });

    params.PopupControl.SetHeaderImageUrl(params.EditHeaderImageUrl);
    params.PopupControl.SetHeaderText(params.EditHeaderText);

    params.PopupControl.PerformCallback(data);
}



function ShowGridInPopup(params) {
    var modelJson = {
        Name: params.Name
    };
    
    var data = {
        Name: params.Name,
        ControllerName: params.ControllerName,
        ActionName: params.ActionName,
        ModelFullName: params.GridModelFullName,
        ModelJson: JSON.stringify(modelJson)
    };

    params.PopupControl.SetWidth(params.Width);
    params.PopupControl.SetHeight(params.Height);

    params.PopupControl.EndCallback.ClearHandlers();
    params.PopupControl.EndCallback.AddHandler(function (s, e) {
        var grv = window['grv' + modelJson.Name];

        s.BeforeResizing.ClearHandlers();
        s.BeforeResizing.AddHandler(function (s1, e1) {
            grv.SetVisible(false);
        });

        s.AfterResizing.ClearHandlers();
        s.AfterResizing.AddHandler(function (s1, e1) {
            grv.SetVisible(false);
        });

        s.Resize.ClearHandlers();
        s.Resize.AddHandler(function (s1, e1) {
            grv.SetWidth(s1.GetContentWidth());
            grv.SetHeight(s1.GetContentHeight());
            grv.SetVisible(true);

        });

        s.Shown.ClearHandlers();
        s.Shown.AddHandler(function (s1, e1) {
            grv.SetWidth(s1.GetContentWidth());
            grv.SetHeight(s1.GetContentHeight());
            grv.SetVisible(true);
        });

        if (params.ShowGridShownMethod != null) {
            s.Shown.AddHandler(params.ShowGridShownMethod);
        }

        s.Show();
    });

    params.PopupControl.SetHeaderImageUrl(params.ShowGridHeaderImageUrl);
    params.PopupControl.SetHeaderText(params.ShowGridHeaderText);
    params.PopupControl.SetWidth(params.ShowGridFormWidth);
    params.PopupControl.SetHeight(params.ShowGridFormHeight);

    params.PopupControl.PerformCallback(data);
}

function ShowGoogleMapsInPopup(params) {
    params.Width = params.Width == undefined ? 600 : params.Width;
    params.Height = params.Height == undefined ? 400 : params.Height;

    var modelJson = {
        Name: params.Name,
        PopupControl: params.PopupControl.name,
        Latitude: params.Latitude,
        Longitude: params.Longitude,
        SuccessMethod: params.SuccessMethod.toString(),
        CancelMethod: params.CancelMethod.toString()
    }

    var data = {
        Name: params.Name,
        ControllerName: 'EditForm',
        ActionName: 'GoogleMapsFindAddressPartial',
        ModelFullName: 'VillageSociety.Models.MapModel',
        ModelJson: JSON.stringify(modelJson)
    };

    params.PopupControl.SetWidth(params.Width);
    params.PopupControl.SetHeight(params.Height);

    params.PopupControl.EndCallback.ClearHandlers();
    params.PopupControl.EndCallback.AddHandler(function (s, e) {
        s.Show();
    });

    params.PopupControl.SetHeaderImageUrl('/Content/Icon/GoogleMaps/16.png');
    params.PopupControl.SetHeaderText('Google Maps');
    params.PopupControl.SetSize(params.Width, params.Height);

    params.PopupControl.PerformCallback(data);
}

function ShowUserProfileInPopup(params) {
    var modelJson = {
        Id: params.Id
    };

    var data = {
        Name: params.Name,
        ControllerName: 'AccountUser',
        ActionName: 'ShowProfilePartial',
        ModelFullName: 'Core.AccountUserEx, Core',
        ModelJson: JSON.stringify(modelJson)
    };

    params.PopupControl.SetWidth(0);
    params.PopupControl.SetHeight(0);

    params.PopupControl.EndCallback.ClearHandlers();
    params.PopupControl.EndCallback.AddHandler(function (s, e) {
        s.Show();
    });



    params.PopupControl.PerformCallback(data);
}
