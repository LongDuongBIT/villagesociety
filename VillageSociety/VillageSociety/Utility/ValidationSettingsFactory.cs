﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety
{
    public class ValidationSettingsFactory
    {
        static ValidationSettings standard;
        public static ValidationSettings Standard
        {
            get
            {
                if (standard == null)
                {
                    standard = ValidationSettings.CreateValidationSettings(null);
                    standard.ValidateOnLeave = true;
                    standard.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                    standard.Display = Display.Dynamic;
                }

                return standard;
            }
        }
    }
}