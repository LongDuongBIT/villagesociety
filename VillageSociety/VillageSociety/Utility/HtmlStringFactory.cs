﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VillageSociety
{
    public static class HtmlStringFactory
    {
        public static string ModelStateError(ModelStateDictionary modelState)
        {
            string returnValue = "";

            var errors = (from item in modelState.Values
                          from error in item.Errors
                          select error.ErrorMessage).ToList();

            foreach (string error in errors)
            {
                if (returnValue != "")
                {
                    returnValue += "<br />";
                }

                returnValue += error;
            }

            return returnValue;
        }

        public static string ExceptionError(Exception exception)
        {
            string returnValue = "";
            
            while (exception != null)
            {
                if (returnValue != "")
                    returnValue += "<br />";

                returnValue += exception.Message ?? "";
                exception = exception.InnerException;
            }

            return returnValue;
        }

        public static string Message()
        {
            string html = @"
<div class=""DivSideBySide"" style=""width: {0}"">
    <div style=""vertical-align: top"">
        <img src=""{1}"" alt=""{2}"" />
    </div>
    <div style=""vertical-align: top; width: 100%; padding-left: 20px"">
        <span style=""color: {3}; font-family: {4}; font-size: {5}"">{6}</span>
    </div>
</div>";

            return html;
        }
        public static string WarningMessage(string message, string width = "300px")
        {
            return string.Format(Message(), width, "/Content/Icon/Warning/32.png", Translator.PleaseNoteThatYourInstructions, "black", "Arial", "14px", message);
        }
        public static string CompletedMessage(string message, string width = "300px")
        {
            return string.Format(Message(), width, "/Content/Icon/Completed/48.png", Translator.Completed, "black", "Arial", "14px", message);
        }
        public static string ErrorMessage(string message, string width = "300px")
        {
            return string.Format(Message(), width, "/Content/Icon/Error/48.png", Translator.PleaseTellTheProgrammersAboutTheError, "red", "Arial", "14px", message);
        }
        public static string ForbiddenMessage(string message, string width = "300px")
        {
            return string.Format(Message(), width, "/Content/Icon/Forbidden/48.png", Translator.YouAreNotAuthorizedToProcess, "red", "Arial", "14px", message);
        }
        public static string HelpMessage(string message, string width = "300px")
        {
            return string.Format(Message(), width, "/Content/Icon/Help/32.png", Translator.Help, "black", "Arial", "14px", message);
        }

    }
}