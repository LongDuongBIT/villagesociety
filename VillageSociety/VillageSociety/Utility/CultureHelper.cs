﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace VillageSociety
{
    public static class CultureHelper
    {
        private static CultureInfo javaScriptCulture = null;
        public static CultureInfo JavaScriptCulture
        {
            get
            {
                if (javaScriptCulture == null)
                {
                    javaScriptCulture = new CultureInfo("en-US");

                }
                return javaScriptCulture;
            }
        }
    }
}