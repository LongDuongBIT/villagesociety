﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety
{
    public enum IconSize { Png16 = 16, Png24 = 24, Png32 = 32, Png48 = 48, Png64 = 64, Png72 = 72, Png96 = 96, Png128 = 128 }
}