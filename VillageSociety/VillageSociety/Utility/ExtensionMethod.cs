﻿using Core;
using Service;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety
{
    public static class ExtensionMethod
    {
        public static TEntity WithInsertDefaults<TEntity>(this TEntity entity) where TEntity : BaseEntity
        {
            entity.InsertDate = DateTime.Now;
            entity.InsertedBy = Security.UserId;
            entity.IsDeleted = false;
            return entity;
        }
        public static TEntity WithUpdateDefaults<TEntity>(this TEntity entity) where TEntity : BaseEntity
        {
            entity.UpdateDate = DateTime.Now;
            entity.UpdatedBy = Security.UserId;
            return entity;
        }
        public static TEntity WithDeleteDefaults<TEntity>(this TEntity entity) where TEntity : BaseEntity
        {
            entity.DeleteDate = DateTime.Now;
            entity.DeletedBy = Security.UserId;
            entity.IsDeleted = true;
            return entity;
        }
        public static TEntity WithRestoreDefaults<TEntity>(this TEntity entity) where TEntity : BaseEntity
        {
            entity.UpdateDate = DateTime.Now;
            entity.UpdatedBy = Security.UserId;
            entity.IsDeleted = false;
            return entity;
        }

        public static string Js(this string s)
        {
            return HttpUtility.JavaScriptStringEncode(s);
        }

        public static string Html(this string s)
        {
            return HttpUtility.HtmlEncode(s);
        }

        public static string RemoveCodeSpace(this string s)
        {
            return System.Text.RegularExpressions.Regex.Replace(s, @"(\r\n\s*)+", "");
        }

        public static List<TEntity> ToList<TEntity>(this IQueryable<TEntity> query, string sessionName) where TEntity: BaseEntity
        {
            if (HttpContext.Current.Session[sessionName] == null)
            {
                HttpContext.Current.Session[sessionName] = query.ToList();
            }

            return (List<TEntity>)HttpContext.Current.Session[sessionName];
        }

        public static void FromSession(this ASPxClientLayoutArgs e, string layoutName)
        {
            //return;
            layoutName += "Layout";

            if (e.LayoutMode == ClientLayoutMode.Saving)
            {
                HttpContext.Current.Session[layoutName] = e.LayoutData;
            }
            else if (e.LayoutMode == ClientLayoutMode.Loading)
            {
                e.LayoutData = (string)HttpContext.Current.Session[layoutName];
            }
        }

        public static void FromDatabase(this ASPxClientLayoutArgs e, string layoutName)
        {
            var service = IoC.Resolve<ISystemLayoutService>();
            var item = service.SingleOrDefault(p => p.UserId == Security.UserId && p.LayoutName == layoutName);

            if (e.LayoutMode == ClientLayoutMode.Saving)
            {
                if (item == null)
                {
                    item = new SystemLayout
                    {
                        UserId = Security.UserId,
                        LayoutName = layoutName,
                        LayoutData = e.LayoutData
                    };

                    service.Insert(item.WithInsertDefaults());
                }
                else
                {
                    if (item.IsDeleted)
                    {
                        service.Restore(item.WithRestoreDefaults(), false);
                    } 
                    
                    item.LayoutData = e.LayoutData;
                }

                service.Save();
            }
            else if (e.LayoutMode == ClientLayoutMode.Loading)
            {
                if (item != null)
                {
                    e.LayoutData = item.LayoutData;
                }
            }
        }
    }
}