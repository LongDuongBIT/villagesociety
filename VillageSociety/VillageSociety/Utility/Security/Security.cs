﻿using Core;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace VillageSociety
{
    //https://codeutil.wordpress.com/2013/05/14/forms-authentication-in-asp-net-mvc-4/
    public static class Security
    {
        public static bool IsUserInPermission(Permission permission)
        {
            return System.Web.Security.Roles.IsUserInRole(Convert.ToInt32(permission).ToString());
        }

        public static bool IsUserInPermission(string username, Permission permission)
        {
            return System.Web.Security.Roles.IsUserInRole(username, Convert.ToInt32(permission).ToString());
        }

        public static int UserId
        {
            get
            {
                return (int)Membership.GetUser().ProviderUserKey;
            }
        }
        public static int PersonId
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).PersonId;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).PersonId = value;
            }
        }
        public static int RoleId
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).RoleId;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).RoleId = value;
            }
        }
        public static string Username
        {
            get
            {
                return System.Web.Security.Membership.GetUser().UserName;
            }
        }
        public static int ThemeId
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).ThemeId;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).ThemeId = value;
            }
        }
        public static int LanguageId
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).LanguageId;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).LanguageId = value;
            }
        }

        public static bool IsSystemAdmin
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).IsSystemAdmin;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).IsSystemAdmin = value;
            }
        }

        public static int VillageId
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).VillageId;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).VillageId = value;
            }
        }
        public static string VillageName
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).VillageName;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).VillageName = value;
            }
        }
        public static string FirstName
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).FirstName;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).FirstName = value;
            }
        }
        public static string LastName
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).LastName;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).LastName = value;
            }
        }
        public static bool IsMale
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).IsMale;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).IsMale = value;
            }
        }
        public static string PicturePath
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).PicturePath;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).PicturePath = value;
            }
        }
        public static string RoleName
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).RoleName;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).RoleName = value;
            }
        }
        public static string ThemeName
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).ThemeName;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).ThemeName = value;
            }
        }
        public static string LanguageCode
        {
            get
            {
                return ((CustomMembershipUser)Membership.GetUser()).LanguageCode;
            }
            set
            {
                ((CustomMembershipUser)Membership.GetUser()).LanguageCode = value;
            }
        }

        public static void Reload()
        {
            var user = IoC.Resolve<IAccountUserService>().GetEx(p => p.Id == Security.UserId).Single();
            PersonId = user.PersonId;
            RoleId = user.RoleId;
            ThemeId = user.ThemeId;
            LanguageId = user.LanguageId;
            IsSystemAdmin = user.IsSystemAdmin;
            VillageId = user.VillageId;
            VillageName = user.VillageName;
            FirstName = user.FirstName;
            LastName = user.LastName;
            IsMale = user.IsMale;
            PicturePath = user.PicturePath;
            RoleName = user.RoleName;
            ThemeName = user.ThemeName;
            LanguageCode = user.LanguageCode;

        }
    }






}