﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VillageSociety
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class PermissionAuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute
    {
        public string UnauthorizedForResourceName { get; set; }
        public PermissionAuthorizeAttribute(string unauthorizedForResourceName, params Permission[] permissions)
        {
            UnauthorizedForResourceName = unauthorizedForResourceName;
            Roles = string.Join(",", permissions.Select(r => Convert.ToInt32(r).ToString()));
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
            else if (!this.Roles.Split(',').Any(filterContext.HttpContext.User.IsInRole))
            {
                string unauthorizedFor = Internationalization.Translator.ResourceManager.GetString(UnauthorizedForResourceName);
                filterContext.RouteData.Values["UnauthorizedFor"] = unauthorizedFor;

                if (filterContext.IsChildAction)
                {
                    filterContext.Result = new PartialViewResult
                    {
                        ViewName = "~/Views/Shared/Unauthorized.cshtml"
                    };
                }
                else
                {
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "~/Views/Shared/Unauthorized.cshtml"
                    };
                }
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}