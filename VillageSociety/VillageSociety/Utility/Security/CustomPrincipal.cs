﻿using System;
using System.Security.Principal;

namespace VillageSociety
{
    [Serializable]
    public class CustomPrincipal : IPrincipal
    {
        public bool IsInRole(string permissionId)
        {
            return Identity is CustomIdentity && System.Web.Security.Roles.IsUserInRole(permissionId);
        }

        public IIdentity Identity { get; private set; }

        public CustomIdentity CustomIdentity { get { return (CustomIdentity)Identity; } set { Identity = value; } }

        public CustomPrincipal(CustomIdentity identity)
        {
            Identity = identity;
        }
    }
}