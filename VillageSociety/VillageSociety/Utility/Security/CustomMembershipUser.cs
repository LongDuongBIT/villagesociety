﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace VillageSociety
{
    public class CustomMembershipUser : MembershipUser
    {
        public int PersonId { get; set; }
        public int RoleId { get; set; }
        public int ThemeId { get; set; }
        public int LanguageId { get; set; }
        public bool IsSystemAdmin { get; set; }
        public int VillageId { get; set; }
        public string VillageName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsMale { get; set; }
        public string PicturePath { get; set; }
        public string RoleName { get; set; }
        public string ThemeName { get; set; }
        public string LanguageCode { get; set; }
 
        public CustomMembershipUser(AccountUserEx user)
            : base("CustomMembershipProvider", user.Username, user.Id, user.UserEMail, string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now)
        {
            PersonId = user.PersonId;
            RoleId = user.RoleId;
            ThemeId = user.ThemeId;
            LanguageId = user.LanguageId;
            IsSystemAdmin = user.IsSystemAdmin;
            VillageId = user.VillageId;
            FirstName = user.FirstName;
            LastName = user.LastName;
            IsMale = user.IsMale;
            PicturePath = user.PicturePath;
            VillageName = user.VillageName;
            RoleName = user.RoleName;
            ThemeName = user.ThemeName;
            LanguageCode = user.LanguageCode;
        }
    }
}