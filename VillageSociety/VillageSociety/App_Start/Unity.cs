using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Unity.Mvc5;
using BusinessLogic;
using System;

namespace VillageSociety
{
    public static class Unity
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer().LoadConfiguration();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        public static T Get<T>()
        {
            return DependencyResolver.Current.GetService<T>();
        }
    }
}