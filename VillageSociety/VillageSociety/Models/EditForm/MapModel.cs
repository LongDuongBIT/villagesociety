﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VillageSociety.Models
{
    public class MapModel
    {
        public string Name { get; set; }
        public string PopupControl { get; set; }

        public string Search { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(3, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string CountryCode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(255, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string CountryName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(255, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string CityName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(255, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string DistrictName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(255, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string FullAddress { get; set; }

        [StringLength(15, MinimumLength = 1, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string PostalCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [Range(-90.0, 90.0, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Range")]
        public double? Latitude { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [Range(-180.0, 180.0, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Range")]
        public double? Longitude { get; set; }

        public string SuccessMethod { get; set; }
        public string CancelMethod { get; set; }
    }
}