﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class GridLookupMenuModel
    {
        public string GridLookupName { get; set; }
        public string ModelName { get; set; }
        public string ControllerName { get; set; }
        public string GridActionName { get; set; }
        public string GridModelFullName { get; set; }
        public string EditActionName { get; set; }
        public string EditModelFullName { get; set; }
        public string RepositoryFullName { get; set; }
        public string ToolTip { get; set; }

        public bool IsAddVisible { get; set; }
        public string AddToolTip { get; set; }
        public string AddHeaderImageUrl { get; set; }
        public string AddHeaderText { get; set; }
        public int AddFormWidth { get; set; }
        public string AddShownMethod { get; set; }        

        public bool IsEditVisible { get; set; }
        public string EditToolTip { get; set; }
        public string EditHeaderImageUrl { get; set; }
        public string EditHeaderText { get; set; }
        public int EditFormWidth { get; set; }
        public string EditShownMethod { get; set; }

        public bool IsDeleteVisible { get; set; }
        public string DeleteToolTip { get; set; }
        
        public bool IsShowGridVisible { get; set; }
        public string ShowGridToolTip { get; set; }
        public string ShowGridHeaderImageUrl { get; set; }
        public string ShowGridHeaderText { get; set; }
        public int ShowGridFormWidth { get; set; }
        public int ShowGridFormHeight { get; set; }
        public string ShowGridShownMethod { get; set; }

        public GridLookupMenuModel()
        {
        }
        public GridLookupMenuModel(string resourceName)
        {
            string recordCaption = Translator.ResourceManager.GetString(resourceName);

            ControllerName = resourceName;
            GridActionName = "GridInit";
            GridModelFullName = Type.GetType("VillageSociety.Models." + resourceName + "GridModel").FullName;
            EditActionName = "DetailPartial";
            EditModelFullName = Type.GetType("VillageSociety.Models." + resourceName + "EditModel").FullName;
            ToolTip = string.Format(Translator.RecordRelatedOperations, recordCaption);

            IsAddVisible = true;
            AddToolTip = string.Format(Translator.AddNewRecord, recordCaption);
            AddHeaderImageUrl = string.Format("/Content/Icon/{0}/16.png", resourceName);
            AddHeaderText = string.Format(Translator.AddNewRecord, recordCaption);
            AddFormWidth = 500;

            IsEditVisible = true;
            EditToolTip = string.Format(Translator.EditTheSelectedRecord, recordCaption);
            EditHeaderImageUrl = string.Format("/Content/Icon/{0}/16.png", resourceName);
            EditHeaderText = string.Format(Translator.EditTheRecord, recordCaption);
            EditFormWidth = 500;

            IsDeleteVisible = true;
            DeleteToolTip = string.Format(Translator.DeleteTheSelectedRecord, recordCaption);

            IsShowGridVisible = true;
            ShowGridToolTip = Translator.ShowGrid;
            ShowGridHeaderImageUrl = string.Format("/Content/Icon/{0}/16.png", resourceName);
            ShowGridHeaderText = recordCaption;
            ShowGridFormWidth = 700;
            ShowGridFormHeight = 500;
        }
    }
} 