﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class ComboBoxMenuModel
    {
        public string ComboBoxName { get; set; }
        public string ModelName { get; set; }
        public string ControllerName { get; set; }
        public string GridActionName { get; set; }
        public string GridModelFullName { get; set; }
        public string EditActionName { get; set; }
        public string EditModelFullName { get; set; }
        public string RepositoryFullName { get; set; }
        public string ToolTip { get; set; }

        public bool IsAddVisible { get; set; }
        public string AddToolTip { get; set; }
        public string AddHeaderImageUrl { get; set; }
        public string AddHeaderText { get; set; }
        public int AddFormWidth { get; set; }
        public int AddFormHeight { get; set; }
        public string AddShownMethod { get; set; }        

        public bool IsEditVisible { get; set; }
        public string EditToolTip { get; set; }
        public string EditHeaderImageUrl { get; set; }
        public string EditHeaderText { get; set; }
        public int EditFormWidth { get; set; }
        public int EditFormHeight { get; set; }
        public string EditShownMethod { get; set; }

        public bool IsDeleteVisible { get; set; }
        public string DeleteToolTip { get; set; }
        
        public bool IsShowGridVisible { get; set; }
        public string ShowGridToolTip { get; set; }
        public string ShowGridHeaderImageUrl { get; set; }
        public string ShowGridHeaderText { get; set; }
        public int ShowGridFormWidth { get; set; }
        public int ShowGridFormHeight { get; set; }
        public string ShowGridShownMethod { get; set; }

        public ComboBoxMenuModel()
        {
        }
        public ComboBoxMenuModel(string controllerName)
        {
            string caption = Translator.ResourceManager.GetString(controllerName);

            ControllerName = controllerName;
            GridActionName = "GridInit";
            GridModelFullName = Type.GetType("VillageSociety.Models." + controllerName + "GridModel").FullName;
            EditActionName = "DetailPartial";
            EditModelFullName = Type.GetType("VillageSociety.Models." + controllerName + "EditModel").FullName;
            ToolTip = string.Format(Translator.RecordRelatedOperations, caption);

            Permission permission;
            if (Enum.TryParse("Insert" + ControllerName, out permission) && Security.IsUserInPermission(permission))
            {
                IsAddVisible = true;
                AddToolTip = string.Format(Translator.AddNewRecord, caption);
                AddHeaderImageUrl = string.Format("/Content/Icon/{0}/16.png", controllerName);
                AddHeaderText = string.Format(Translator.AddNewRecord, caption);
                AddFormWidth = 500;
            }

            if (Enum.TryParse("Update" + ControllerName, out permission) && Security.IsUserInPermission(permission))
            {
                IsEditVisible = true;
                EditToolTip = string.Format(Translator.EditTheSelectedRecord, caption);
                EditHeaderImageUrl = string.Format("/Content/Icon/{0}/16.png", controllerName);
                EditHeaderText = string.Format(Translator.EditTheRecord, caption);
                EditFormWidth = 500;
            }

            if (Enum.TryParse("Delete" + ControllerName, out permission) && Security.IsUserInPermission(permission))
            {
                IsDeleteVisible = true;
                DeleteToolTip = string.Format(Translator.DeleteTheSelectedRecord, caption);
            }

            if (Enum.TryParse("Read" + ControllerName, out permission) && Security.IsUserInPermission(permission))
            {
                IsShowGridVisible = true;
                ShowGridToolTip = Translator.ShowGrid;
                ShowGridHeaderImageUrl = string.Format("/Content/Icon/{0}/16.png", controllerName);
                ShowGridHeaderText = caption;
                ShowGridFormWidth = 700;
                ShowGridFormHeight = 500;
            }
        }
    }
} 