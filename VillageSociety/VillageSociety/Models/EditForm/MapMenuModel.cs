﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class MapMenuModel
    {
        public string SpinEditLatitudeName { get; set; }
        public string SpinEditLongitudeName { get; set; }
        public string ModelName { get; set; }
        public string ToolTip { get; set; }

        public bool IsGoogleMapsVisible { get; set; }
        public string GoogleMapsToolTip { get; set; }
        public string GoogleMapsHeaderImageUrl { get; set; }
        public string GoogleMapsHeaderText { get; set; }
        public string GoogleMapsShownMethod { get; set; }

        public bool IsBingMapsVisible { get; set; }
        public string BingMapsToolTip { get; set; }
        public string BingMapsHeaderImageUrl { get; set; }
        public string BingMapsHeaderText { get; set; }
        public string BingMapsShownMethod { get; set; }

        public string SuccessMethod { get; set; }
        public string CancelMethod { get; set; }

        public MapMenuModel()
        {
        }
        public MapMenuModel(IconSize iconSize)
        {
            SpinEditLatitudeName = "speLatitude";
            SpinEditLongitudeName = "speLongitude";

            ToolTip = string.Format(Translator.FindAddressByUsing_0, Translator.Map);

            IsGoogleMapsVisible = true;
            GoogleMapsToolTip = string.Format(Translator.FindAddressByUsing_0, "GoogleMaps");
            GoogleMapsHeaderImageUrl = string.Format("/Content/Icon/GoogleMaps/{0}.png", Convert.ToInt32(iconSize));
            GoogleMapsHeaderText = "GoogleMaps";

            IsBingMapsVisible = true;
            BingMapsToolTip = string.Format(Translator.FindAddressByUsing_0, "BingMaps");
            BingMapsHeaderImageUrl = string.Format("/Content/Icon/BingMaps/{0}.png", Convert.ToInt32(iconSize));
            BingMapsHeaderText = "BingMaps";
        }
    }
}