﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class TreeListTitlePanelModel
    {
        public TreeListModel TreeListModel { get; set; }
        public string ImageUrl { get; set; }
        public string Caption { get; set; }

        public TreeListTitlePanelModel()
        {

        }
        public TreeListTitlePanelModel(TreeListModel treeListModel, string resourceName)
        {
            TreeListModel = treeListModel;

            string recordCaption = Translator.ResourceManager.GetString(resourceName);

            ImageUrl = string.Format("/Content/Icon/{0}/24.png", resourceName);
            Caption = recordCaption;
        }
    }
}