﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class TreeListMenuModel
    {
        public TreeListModel TreeListModel { get; set; }
        public string ControllerName { get; set; }

        public bool IsSaveVisible { get; set; }
        public string SaveToolTip { get; set; }

        public bool IsReloadVisible { get; set; }
        public string ReloadToolTip { get; set; }

        public bool IsColumnVisible { get; set; }
        public string ColumnToolTip { get; set; }

        public bool IsExportVisible { get; set; }
        public string ExportToolTip { get; set; }

        public TreeListMenuModel(TreeListModel treeListModel, string resourceName)
        {
            TreeListModel = treeListModel;

            string recordCaption = Translator.ResourceManager.GetString(resourceName);

            ControllerName = resourceName;

            IsSaveVisible = true;
            SaveToolTip = Translator.Save;

            IsReloadVisible = true;
            ReloadToolTip = Translator.Reload;

            IsColumnVisible = true;
            ColumnToolTip = Translator.ShowDragAndDropColumns;

            IsExportVisible = true;
            ExportToolTip = Translator.ExportWholeShownData;
        }
    }
}