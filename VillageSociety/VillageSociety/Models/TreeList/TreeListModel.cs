﻿using Core;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace VillageSociety.Models
{
    public class TreeListModel
    {
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NameChanged();
            }
        }

        public readonly string ControllerName;

        public TreeListSettings Settings { get; set; }
        public List<BaseEntity> Data { get; set; }
        public string JsonData { get; set; }
        public TreeListTitlePanelModel TitlePanelModel { get; set; }
        public TreeListMenuModel MenuModel { get; set; }

        public string MasterController { get; set; }

        public TreeListModel()
        {
            string typeName = this.GetType().Name;
            ControllerName = typeName.Remove(typeName.Length - "TreeListModel".Length);

            Settings = new DevExpress.Web.Mvc.TreeListSettings();
            TitlePanelModel = new TreeListTitlePanelModel(this, ControllerName);
            MenuModel = new TreeListMenuModel(this, ControllerName);

            Name = ControllerName;

            Settings.KeyFieldName = "Id";
            Settings.ParentFieldName = "ParentId";
            Settings.Width = Unit.Percentage(100);
            Settings.Settings.HorizontalScrollBarMode = ScrollBarMode.Auto;
            Settings.Settings.VerticalScrollBarMode = ScrollBarMode.Auto;

            Settings.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Control;

            Settings.SettingsCustomizationWindow.Enabled = true;
            Settings.SettingsCustomizationWindow.PopupWidth = Unit.Pixel(250);
            Settings.SettingsCustomizationWindow.PopupHeight = Unit.Pixel(500);

            Settings.SettingsCookies.Enabled = true;
            Settings.SettingsCookies.StoreExpandedNodes = true;
            Settings.SettingsCookies.StoreSelection = false;

            Settings.SettingsSelection.Enabled = true;
            Settings.SettingsSelection.Recursive = true;
            Settings.SettingsSelection.AllowSelectAll = true;
        }
        protected virtual void NameChanged()
        {
            Settings.Name = "trl" + Name;
            Settings.CallbackRouteValues = new { Controller = ControllerName, Action = "TreeListPartial", Name = Name };
        }
    }
}