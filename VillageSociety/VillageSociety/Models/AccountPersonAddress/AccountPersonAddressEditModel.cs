﻿using DevExpress.Web;
using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VillageSociety.Models
{
    public class AccountPersonAddressEditModel : EditModel
    {
        [Display(Name = "AccountPerson", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? PersonId { get; set; }

        [Display(Name = "AddressCountry", ResourceType = typeof(Translator))]
        public int? CountryId { get; set; }

        [Display(Name = "AddressCity", ResourceType = typeof(Translator))]
        public int? CityId { get; set; }

        [Display(Name = "AddressDistrict", ResourceType = typeof(Translator))]
        public int? DistrictId { get; set; }

        [Display(Name = "Address", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? AddressId { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        [Display(Name = "AddressType", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? AddressTypeId { get; set; }
    }
}