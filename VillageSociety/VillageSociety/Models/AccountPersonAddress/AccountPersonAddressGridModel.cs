﻿using Core;
using Internationalization;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace VillageSociety.Models
{
    public class AccountPersonAddressGridModel : GridModel
    {
        public AccountPersonAddressGridModel()
        {
            Settings.Columns.Add(column =>
            {
                column.FieldName = "Id";
                column.Width = Unit.Pixel(50);
            });

            Settings.Columns.AddBand(band =>
            {
                band.Name = "bndPerson";
                band.Caption = Translator.AccountPerson;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "FirstName";                
                    column.Caption = Translator.FirstName;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "LastName";
                    column.Caption = Translator.LastName;
                });
            });

            Settings.Columns.AddBand(band =>
            {
                band.Caption = Translator.Address;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "CountryCode";
                    column.Caption = Translator.CountryCode;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "CountryName";
                    column.Caption = Translator.CountryName;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "CityName";
                    column.Caption = Translator.AddressCity;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "DistrictName";
                    column.Caption = Translator.AddressDistrict;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "FullAddress";
                    column.Caption = Translator.FullAddress;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "FullAddressDescription";
                    column.Caption = Translator.Description;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "PostalCode";
                    column.Caption = Translator.PostalCode;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "Latitude";
                    column.Caption = Translator.Latitude;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "Longitude";
                    column.Caption = Translator.Longitude;
                });
            });

            Data = () => IoC.Resolve<IAccountPersonAddressService>().GetEx(p => !p.IsDeleted);
        }

        protected override void NameChanged()
        {
            base.NameChanged();
            MenuModel.UseStandardToolbar(false);
        }
    }
}