﻿using DevExpress.Web;
using DevExpress.Web.Mvc;
using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VillageSociety.Models
{
    public class TemplateMasterEditModel : EditModel
    {
        [Display(Name = "TemplateComboBoxPerson", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? ComboBoxPersonId { get; set; }

        [Display(Name = "TemplateGridLookupPerson", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? GridLookupPersonId { get; set; }

        [Display(Name = "TemplateComboBoxCountry", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? ComboBoxCountryId { get; set; }

        [Display(Name = "TemplateComboBoxCity", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? ComboBoxCityId { get; set; }

        [Display(Name = "TemplateComboBoxDistrict", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? ComboBoxDistrictId { get; set; }

        [Display(Name = "TemplateComboBoxAddress", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? ComboBoxAddressId { get; set; }

        [Display(Name = "Latitude", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [Range(-90.0, 90.0, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Range")]
        public double? ComboBoxLatitude { get; set; }

        [Display(Name = "Longitude", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [Range(-180.0, 180.0, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Range")]
        public double? ComboBoxLongitude { get; set; }

        [Display(Name = "TemplateGridLookupCountry", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? GridLookupCountryId { get; set; }

        [Display(Name = "TemplateGridLookupCity", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? GridLookupCityId { get; set; }

        [Display(Name = "TemplateGridLookupDistrict", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? GridLookupDistrictId { get; set; }

        [Display(Name = "TemplateGridLookupAddress", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? GridLookupAddressId { get; set; }

        [Display(Name = "Latitude", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [Range(-90.0, 90.0, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Range")]
        public double? GridLookupLatitude { get; set; }

        [Display(Name = "Longitude", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [Range(-180.0, 180.0, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Range")]
        public double? GridLookupLongitude { get; set; }

        [Display(Name = "TemplateMultiSelect1", ResourceType = typeof(Translator))]
        public int[] TemplatePersons { get; set; }

        [Display(Name = "TemplateMultiSelect2", ResourceType = typeof(Translator))]
        public int[] TemplateUsers { get; set; }

        [Display(Name = "TemplateBitColumn1", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public bool? BitColumn1 { get; set; }

        [Display(Name = "TemplateBitColumn2", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public bool? BitColumn2 { get; set; }

        [Display(Name = "TemplateIntColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [Range(18.0, 35.0, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Range")]
        public int? IntColumn { get; set; }

        [Display(Name = "TemplateFloatColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [Range(3.141592, 6.25, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Range")]
        public double? FloatColumn { get; set; }

        [Display(Name = "TemplateDecimalColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [Range(30.141592, 60.25, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Range")]
        public Decimal? DecimalColumn { get; set; }

        [Display(Name = "TemplateMoneyColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [Range(950.25, 1250.75, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Range")]
        public Decimal? MoneyColumn { get; set; }

        [Display(Name = "TemplatePercentColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [Range(6.28, 100.0, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Range")]
        public Decimal? PercentColumn { get; set; }

        [Display(Name = "TemplateDateColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public DateTime? DateColumn { get; set; }

        [Display(Name = "TemplateDateTimeColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public DateTime? DateTimeColumn { get; set; }

        [Display(Name = "TemplateStringColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string StringColumn { get; set; }

        [Display(Name = "TemplateMultilineColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public string MultilineColumn { get; set; }

        [Display(Name = "TemplateHtmlColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public string HtmlColumn { get; set; }

        [Display(Name = "TemplatePhoneNumberColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        [Mask("90 (000) 000 0000")]
        public string PhoneNumberColumn { get; set; }

        [Display(Name = "TemplateCreditCardNumberColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        [Mask("0000 0000 0000 0000")]
        //[CreditCard()] //use in publish
        public string CreditCardNumberColumn { get; set; }

        [Display(Name = "TemplateLatitudeColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public double? LatitudeColumn { get; set; }

        [Display(Name = "TemplateLongitudeColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public double? LongitudeColumn { get; set; }

        [Display(Name = "TemplateFilePathColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public string FilePathColumn { get; set; }

        [Display(Name = "TemplateImagePathColumn", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public string ImagePathColumn { get; set; }
    }
}