﻿using Core;
using Service;
using Internationalization;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web;
using DevExpress.Web.Mvc;
using DevExpress.Web.Mvc.UI;

namespace VillageSociety.Models
{
    public class TemplateMasterGridModel : GridModel
    {
        public TemplateMasterGridModel()
        {
            Settings.Columns.Add(column =>
            {
                column.FieldName = "Id";
                column.Width = Unit.Pixel(50);
            });

            Settings.Columns.AddBand(band =>
            {
                band.Caption = Translator.TemplateComboBoxPerson;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "ComboBoxFirstName";
                    column.Caption = Translator.FirstName;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "ComboBoxLastName";
                    column.Caption = Translator.LastName;
                });
            });

            Settings.Columns.AddBand(band =>
            {
                band.Caption = Translator.TemplateGridLookupPerson;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "GridLookupFirstName";
                    column.Caption = Translator.FirstName;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "GridLookupLastName";
                    column.Caption = Translator.LastName;
                });
            });

            Settings.Columns.AddBand(band =>
            {
                band.Caption = Translator.TemplateComboBoxAddress;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "ComboBoxCountryName";
                    column.Caption = Translator.CountryName;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "ComboBoxCityName";
                    column.Caption = Translator.AddressCity;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "ComboBoxDistrictName";
                    column.Caption = Translator.AddressDistrict;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "ComboBoxFullAddress";
                    column.Caption = Translator.FullAddress;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "ComboBoxPostalCode";
                    column.Caption = Translator.PostalCode;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "ComboBoxLatitude";
                    column.Caption = Translator.Latitude;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "ComboBoxLongitude";
                    column.Caption = Translator.Longitude;
                });
            });

            Settings.Columns.AddBand(band =>
            {
                band.Caption = Translator.TemplateGridLookupAddress;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "GridLookupCountryName";
                    column.Caption = Translator.CountryName;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "GridLookupCityName";
                    column.Caption = Translator.AddressCity;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "GridLookupDistrictName";
                    column.Caption = Translator.AddressDistrict;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "GridLookupFullAddress";
                    column.Caption = Translator.FullAddress;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "GridLookupPostalCode";
                    column.Caption = Translator.PostalCode;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "GridLookupLatitude";
                    column.Caption = Translator.Latitude;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "GridLookupLongitude";
                    column.Caption = Translator.Longitude;
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "BitColumn1";
                column.Caption = Translator.TemplateBitColumn1;
                column.ColumnType = MVCxGridViewColumnType.CheckBox;

                column.SetDataItemTemplateContent(content =>
                {
                    bool value = (bool)DataBinder.Eval(content.DataItem, column.FieldName);

                    var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];
                    if (value)
                    {
                        viewContext.Writer.Write(@"<div><img src=""/Content/Icon/Checked/16.png"" alt="""" /><span style=""vertical-align: top; padding-left: 3px"">" + Translator.Yes + "</span></div>");
                    }
                    else
                    {
                        viewContext.Writer.Write(@"<div><img src=""/Content/Icon/Unchecked/16.png"" alt="""" /><span style=""vertical-align: top; padding-left: 3px"">" + Translator.No + "</span></div>");
                    }
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "BitColumn2";
                column.Caption = Translator.TemplateBitColumn2;
                column.ColumnType = MVCxGridViewColumnType.CheckBox;

                column.SetDataItemTemplateContent(content =>
                {
                    bool value = (bool)DataBinder.Eval(content.DataItem, column.FieldName);
                    var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];
                    viewContext.Writer.Write(@"<img src=""/Content/Icon/" + (value ? "Checked" : "Unchecked") + @"/16.png"" alt="""" />");
                });

            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "IntColumn";
                column.Caption = Translator.TemplateIntColumn;
                column.ColumnType = MVCxGridViewColumnType.SpinEdit;
                ((SpinEditProperties)column.PropertiesEdit).NumberFormat = SpinEditNumberFormat.Number;
                ((SpinEditProperties)column.PropertiesEdit).NumberType = SpinEditNumberType.Integer;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "FloatColumn";
                column.Caption = Translator.TemplateFloatColumn;
                column.ColumnType = MVCxGridViewColumnType.SpinEdit;
                ((SpinEditProperties)column.PropertiesEdit).NumberFormat = SpinEditNumberFormat.Number;
                ((SpinEditProperties)column.PropertiesEdit).NumberType = SpinEditNumberType.Float;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "DecimalColumn";
                column.Caption = Translator.TemplateDecimalColumn;
                column.ColumnType = MVCxGridViewColumnType.SpinEdit;
                ((SpinEditProperties)column.PropertiesEdit).NumberFormat = SpinEditNumberFormat.Number;
                ((SpinEditProperties)column.PropertiesEdit).NumberType = SpinEditNumberType.Float;
                ((SpinEditProperties)column.PropertiesEdit).DecimalPlaces = 2;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "MoneyColumn";
                column.Caption = Translator.TemplateMoneyColumn;
                column.ColumnType = MVCxGridViewColumnType.SpinEdit;
                ((SpinEditProperties)column.PropertiesEdit).NumberFormat = SpinEditNumberFormat.Currency;
                ((SpinEditProperties)column.PropertiesEdit).NumberType = SpinEditNumberType.Float;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "PercentColumn";
                column.Caption = Translator.TemplatePercentColumn;
                column.ColumnType = MVCxGridViewColumnType.SpinEdit;
                ((SpinEditProperties)column.PropertiesEdit).NumberFormat = SpinEditNumberFormat.Percent;
                ((SpinEditProperties)column.PropertiesEdit).NumberType = SpinEditNumberType.Float;
                ((SpinEditProperties)column.PropertiesEdit).DecimalPlaces = 2;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "DateColumn";
                column.Caption = Translator.TemplateDateColumn;
                column.ColumnType = MVCxGridViewColumnType.DateEdit;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "DateTimeColumn";
                column.Caption = Translator.TemplateDateTimeColumn;
                column.ColumnType = MVCxGridViewColumnType.DateEdit;
                ((DateEditProperties)column.PropertiesEdit).TimeSectionProperties.Visible = true;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "StringColumn";
                column.Caption = Translator.TemplateStringColumn;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "MultilineColumn";
                column.Caption = Translator.TemplateMultilineColumn;
                column.ColumnType = MVCxGridViewColumnType.Memo;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "HtmlColumn";
                column.Caption = Translator.TemplateHtmlColumn;
                column.PropertiesEdit.EncodeHtml = false;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "PhoneNumberColumn";
                column.Caption = Translator.TemplatePhoneNumberColumn;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "CreditCardNumberColumn";
                column.Caption = Translator.TemplateCreditCardNumberColumn;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "LatitudeColumn";
                column.Caption = Translator.TemplateLatitudeColumn;
                column.ColumnType = MVCxGridViewColumnType.SpinEdit;
                ((SpinEditProperties)column.PropertiesEdit).NumberFormat = SpinEditNumberFormat.Number;
                ((SpinEditProperties)column.PropertiesEdit).NumberType = SpinEditNumberType.Float;
                ((SpinEditProperties)column.PropertiesEdit).MinValue = -90;
                ((SpinEditProperties)column.PropertiesEdit).MaxValue = 90;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "LongitudeColumn";
                column.Caption = Translator.TemplateLongitudeColumn;
                column.ColumnType = MVCxGridViewColumnType.SpinEdit;
                ((SpinEditProperties)column.PropertiesEdit).NumberFormat = SpinEditNumberFormat.Number;
                ((SpinEditProperties)column.PropertiesEdit).NumberType = SpinEditNumberType.Float;
                ((SpinEditProperties)column.PropertiesEdit).MinValue = -180;
                ((SpinEditProperties)column.PropertiesEdit).MaxValue = 180;
            });

            Settings.Columns.Add(column =>
            {
                column.Caption = Translator.TemplateFilePathColumn;
                column.SetDataItemTemplateContent(content =>
                {
                    var value = (string)DataBinder.Eval(content.DataItem, "FilePathColumn");
                    if (value != null)
                    {
                        var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];
                        viewContext.Writer.Write(@"<a href=""" + value + @""" download>" + Path.GetFileName(value) + @"</a>");
                    }
                });
            });

            Settings.Columns.AddBand(band =>
            {
                band.Caption = Translator.TemplateImagePathColumn;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.Caption = Translator.ImageName;
                    column.SetDataItemTemplateContent(content =>
                    {
                        var value = (string)DataBinder.Eval(content.DataItem, "ImagePathColumn");
                        if (value != null)
                        {
                            var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];
                            viewContext.Writer.Write(@"<a href=""" + value + @""" download>" + Path.GetFileName(value) + @"</a>");
                        }
                    });
                });

                band.Columns.Add(column =>
                {
                    column.Caption = Translator.VisualImage;
                    column.SetDataItemTemplateContent(content =>
                    {
                        var value = (string)DataBinder.Eval(content.DataItem, "ImagePathColumn");
                        if (value != null)
                        {
                            var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];
                            viewContext.Writer.Write(@"<img src=""" + value + @""" alt="""" style=""width: 100%"" />");
                        }
                    });
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertDate";
                column.Caption = Translator.InsertDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertedBy";
                column.Caption = Translator.InsertedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var insertedBy = (int)DataBinder.Eval(content.DataItem, column.FieldName);

                    var html = (HtmlHelper<TemplateMasterGridModel>)HttpContext.Current.Session["Html" + Name];;
                    html.DevExpress().HyperLink(hpls =>
                    {
                        hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                        hpls.Properties.Text = Translator.InsertedBy;
                        hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                        hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + insertedBy.ToString() + @"
    });
}";
                    }).Render();
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdateDate";
                column.Caption = Translator.UpdateDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdatedBy";
                column.Caption = Translator.UpdatedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var updatedBy = (int?)DataBinder.Eval(content.DataItem, column.FieldName);

                    if (updatedBy.HasValue)
                    {
                        var html = (HtmlHelper<TemplateMasterGridModel>)HttpContext.Current.Session["Html" + Name];;
                        html.DevExpress().HyperLink(hpls =>
                        {
                            hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                            hpls.Properties.Text = Translator.UpdatedBy;
                            hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                            hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + updatedBy.ToString() + @"
    });
}";
                        }).Render();
                    }
                });
            });

            Data = () => IoC.Resolve<ITemplateMasterService>().GetEx(p => !p.IsDeleted);

        }

        protected override void NameChanged()
        {
            base.NameChanged();

            MenuModel.AddFormWidth = 1000;
            MenuModel.AddFormHeight = 800;
            MenuModel.EditFormWidth = 1000;
            MenuModel.EditFormHeight = 800;

            MenuModel.UseStandardToolbar(false);
        }

    }
}