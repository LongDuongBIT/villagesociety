﻿using Core;
using Internationalization;
using Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VillageSociety.Models
{
    public class AccountPersonGridModel : GridModel
    {
        public AccountPersonGridModel()
        {
            Settings.Columns.Add(column =>
            {
                column.FieldName = "Id";
                column.Width = Unit.Pixel(50);
            });
            Settings.Columns.Add(column =>
            {
                column.FieldName = "VillageName";
                column.Caption = Translator.SystemVillage;
            });
            Settings.Columns.Add(column =>
            {
                column.FieldName = "FirstName";                
                column.Caption = Translator.FirstName;
            });
            Settings.Columns.Add(column =>
            {
                column.FieldName = "LastName";
                column.Caption = Translator.LastName;
            });
            Settings.Columns.AddBand(band =>
            {
                band.Caption = Translator.Profile;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.Caption = Translator.PictureName;
                    column.SetDataItemTemplateContent(content =>
                    {
                        var picturePath = (string)DataBinder.Eval(content.DataItem, "PicturePath");
                        if (picturePath != null)
                        {
                            var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];
                            viewContext.Writer.Write(@"<a href=""" + picturePath + @""" download>" + Path.GetFileName(picturePath) + @"</a>");
                        }
                    });
                });
                band.Columns.Add(column =>
                {
                    column.Caption = Translator.Picture;
                    column.SetDataItemTemplateContent(content =>
                    {
                        var picturePath = (string)DataBinder.Eval(content.DataItem, "PicturePath");
                        if (picturePath != null)
                        {
                            var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];
                            viewContext.Writer.Write(@"<img src=""" + picturePath + @""" alt="""" style=""width: 100%"" />");
                        }
                    });
                });
            });
            Settings.Columns.AddBand(band =>
            {
                band.Caption = Translator.Father;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "FatherFirstName";
                    column.Caption = Translator.FirstName;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "FatherLastName";
                    column.Caption = Translator.LastName;
                });
            });
            Settings.Columns.AddBand(band =>
            {
                band.Caption = Translator.Mother;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "MotherFirstName";
                    column.Caption = Translator.FirstName;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "MotherLastName";
                    column.Caption = Translator.LastName;
                });
            });

            Data = () => IoC.Resolve<IAccountPersonService>().GetEx(p => !p.IsDeleted);
        }

        protected override void NameChanged()
        {
            base.NameChanged();

            MenuModel.AddShownMethod = @"function(s, e) {
    cmbClearAddAndSetItem(cmbVillageId" + Name + "Grid, '" + Security.VillageName + "', " + Security.VillageId.ToString() + ", " + (Security.IsSystemAdmin ? "true" : "false") + @");
}";

            MenuModel.UseStandardToolbar(false);
        }
    }
}