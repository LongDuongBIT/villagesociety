﻿using Core;
using Service;
using Internationalization;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web;
using DevExpress.Web.Mvc;
using DevExpress.Web.Mvc.UI;

namespace VillageSociety.Models
{
    public class AccountRoleLanguageGridModel : GridModel
    {
        public AccountRoleLanguageGridModel()
        {
            Settings.Columns.Add(column =>
            {
                column.FieldName = "Id";
                column.Width = Unit.Pixel(50);
            });

            Settings.Columns.AddBand(band =>
            {
                band.Name = "bndRole";
                band.Caption = Translator.AccountRole;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "DefaultRoleName";
                    column.Caption = Translator.RoleName;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "DefaultRoleDescription";
                    column.Caption = Translator.Description;
                    column.ColumnType = MVCxGridViewColumnType.Memo;
                });
            });

            Settings.Columns.AddBand(band =>
            {
                band.Name = "bndLanguage";
                band.Caption = Translator.AccountLanguage;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "LanguageCode";
                    column.Caption = Translator.LanguageCode;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "LanguageName";
                    column.Caption = Translator.Name;
                });

                band.Columns.AddBand(band2 =>
                {
                    band2.Caption = Translator.Icon;
                    band2.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                    band2.Columns.Add(column =>
                    {
                        column.Caption = Translator.IconName;
                        column.SetDataItemTemplateContent(content =>
                        {
                            var value = (string)DataBinder.Eval(content.DataItem, "IconPath");
                            if (value != null)
                            {
                                var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];
                                viewContext.Writer.Write(@"<a href=""" + value + @""" download>" + Path.GetFileName(value) + @"</a>");
                            }
                        });
                    });

                    band2.Columns.Add(column =>
                    {
                        column.Caption = Translator.VisualIcon;
                        column.Width = 30;
                        column.SetDataItemTemplateContent(content =>
                        {
                            var value = (string)DataBinder.Eval(content.DataItem, "IconPath");
                            if (value != null)
                            {
                                var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];
                                viewContext.Writer.Write(@"<img src=""" + value + @""" alt="""" style=""width: 100%"" />");
                            }
                        });
                    });
                });

            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "RoleName";
                column.Caption = Translator.RoleName;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "RoleDescription";
                column.Caption = Translator.Description;
                column.ColumnType = MVCxGridViewColumnType.Memo;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertDate";
                column.Caption = Translator.InsertDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertedBy";
                column.Caption = Translator.InsertedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var insertedBy = (int)DataBinder.Eval(content.DataItem, column.FieldName);

                    var html = (HtmlHelper<AccountRoleLanguageGridModel>)HttpContext.Current.Session["Html" + Name];;
                    html.DevExpress().HyperLink(hpls =>
                    {
                        hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                        hpls.Properties.Text = Translator.InsertedBy;
                        hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                        hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + insertedBy.ToString() + @"
    });
}";
                    }).Render();
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdateDate";
                column.Caption = Translator.UpdateDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdatedBy";
                column.Caption = Translator.UpdatedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var updatedBy = (int?)DataBinder.Eval(content.DataItem, column.FieldName);

                    if (updatedBy.HasValue)
                    {
                        var html = (HtmlHelper<AccountRoleLanguageGridModel>)HttpContext.Current.Session["Html" + Name];;
                        html.DevExpress().HyperLink(hpls =>
                        {
                            hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                            hpls.Properties.Text = Translator.UpdatedBy;
                            hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                            hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + updatedBy.ToString() + @"
    });
}";
                        }).Render();
                    }
                });
            });


            Data = () => IoC.Resolve<IAccountRoleLanguageService>().GetEx(p => !p.IsDeleted);
        }

        protected override void NameChanged()
        {
            base.NameChanged();

            MenuModel.AddFormWidth = 900;
            MenuModel.EditFormWidth = 600;

            MenuModel.UseStandardToolbar(false);
        }

    }
}