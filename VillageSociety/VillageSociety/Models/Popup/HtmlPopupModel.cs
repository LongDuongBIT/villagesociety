﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace VillageSociety.Models
{
    public class HtmlPopupModel
    {
        public string Name { get; set; }
        public string HeaderImageUrl { get; set; }
        public string HeaderText { get; set; }
        public Unit Width { get; set; }
        public Unit Height { get; set; }
        public bool Modal { get; set; }

        public bool ShowOnPageLoad { get; set; }
        public string Html { get; set; }
        public bool IsOkVisible { get; set; }
        public string OkMethod { get; set; }
        public bool IsCancelVisible { get; set; }
        public string CancelMethod { get; set; }
        public bool IsCloseVisible { get; set; }

        public HtmlPopupModel()
        {
            IsOkVisible = false;
            IsCancelVisible = false;
            IsCloseVisible = true;
            Modal = true;
        }
    }
}