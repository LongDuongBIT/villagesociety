﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace VillageSociety.Models
{
    public class ActionPopupModel
    {
        public string Name { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string ModelFullName { get; set; }
        public string ModelJson { get; set; }
        public object ModelData { get; set; }
        public string HeaderImageUrl { get; set; }
        public string HeaderText { get; set; }
        public Unit Width { get; set; }
        public Unit Height { get; set; }
        public bool Modal { get; set; }
        public string ShownMethod { get; set; }

        public ActionPopupModel()
        {
            Modal = true;
        }
    }
}