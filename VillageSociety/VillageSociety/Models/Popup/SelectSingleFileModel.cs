﻿using DevExpress.Web;
using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace VillageSociety.Models
{
    public class SelectSingleFileModel
    {
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NameChanged();
            }
        }
        public Unit Width { get; set; }
        public Unit Height { get; set; }

        public string[] AllowedFileExtensions { get; set; }

        public string RootFolder { get; set; }
        public string InitialFolder { get; set; }

        public int? MinUploadSize { get; set; }
        public int? MaxUploadSize { get; set; }

        public int? MinImageWidth { get; set; }
        public int? MaxImageWidth { get; set; }
        public int? MinImageHeight { get; set; }
        public int? MaxImageHeight { get; set; }

        public FileManagerSettingsEditing SettingsEditing { get; set; }
        public FileManagerSettingsToolbar SettingsToolbar { get; set; }
        public FileManagerSettingsFolders SettingsFolders { get; set; }
        public FileManagerSettingsUpload SettingsUpload { get; set; }

        public string OkScript { get; set; }
        public string CancelScript { get; set; }

        public SelectSingleFileModel()
        {
            InitialFolder = "/";

            SettingsEditing = new FileManagerSettingsEditing(null)
            {
                AllowCopy = true,
                AllowCreate = true,
                AllowDelete = true,
                AllowDownload = true,
                AllowMove = true,
                AllowRename = true
            };

            SettingsToolbar = new FileManagerSettingsToolbar(null)
            {
                ShowPath = true,
                ShowFilterBox = true
            };
            SettingsToolbar.Items.Add(new FileManagerToolbarRefreshButton());
            SettingsToolbar.Items.Add(new FileManagerToolbarCopyButton());
            SettingsToolbar.Items.Add(new FileManagerToolbarCreateButton());
            SettingsToolbar.Items.Add(new FileManagerToolbarDeleteButton());
            SettingsToolbar.Items.Add(new FileManagerToolbarDownloadButton());
            SettingsToolbar.Items.Add(new FileManagerToolbarMoveButton());
            SettingsToolbar.Items.Add(new FileManagerToolbarRenameButton());

            SettingsFolders = new FileManagerSettingsFolders(null)
            {
                Visible = true,
                EnableCallBacks = false,
                ShowFolderIcons = true,
                ShowLockedFolderIcons = true
            };

            SettingsUpload = new MVCxFileManagerSettingsUpload()
            {
                Enabled = true,
                UseAdvancedUploadMode = true
            };
            SettingsUpload.AdvancedModeSettings.EnableMultiSelect = true;


        }

        protected virtual void NameChanged()
        {            
            CancelScript = @"function(s, e) {
    puc" + this.Name + @".Hide();
}";
        }
    }   
}