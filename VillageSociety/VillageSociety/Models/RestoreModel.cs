﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class RestoreModel
    {
        public int ExistingId { get; set; }
        public int? UpdatingId { get; set; }
    }
}