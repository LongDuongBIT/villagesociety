﻿using Core;
using Service;
using Internationalization;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web;
using DevExpress.Web.Mvc;
using DevExpress.Web.Mvc.UI;

namespace VillageSociety.Models
{
    public class TemplateDetail2GridModel : GridModel
    {
        public TemplateDetail2GridModel()
        {
            Settings.Columns.Add(column =>
            {
                column.FieldName = "Id";
                column.Width = Unit.Pixel(50);
            });

            Settings.Columns.AddBand(band =>
            {
                band.Name = "bndMaster";
                band.Caption = Translator.TemplateMaster;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "StringColumn";
                    column.Caption = Translator.TemplateStringColumn;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "MultilineColumn";
                    column.Caption = Translator.TemplateMultilineColumn;
                    column.ColumnType = MVCxGridViewColumnType.Memo;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "HtmlColumn";
                    column.Caption = Translator.TemplateHtmlColumn;
                    column.PropertiesEdit.EncodeHtml = false;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "PhoneNumberColumn";
                    column.Caption = Translator.TemplatePhoneNumberColumn;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "CreditCardNumberColumn";
                    column.Caption = Translator.TemplateCreditCardNumberColumn;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "LatitudeColumn";
                    column.Caption = Translator.TemplateLatitudeColumn;
                    column.ColumnType = MVCxGridViewColumnType.SpinEdit;
                    ((SpinEditProperties)column.PropertiesEdit).NumberFormat = SpinEditNumberFormat.Number;
                    ((SpinEditProperties)column.PropertiesEdit).NumberType = SpinEditNumberType.Float;
                    ((SpinEditProperties)column.PropertiesEdit).MinValue = -90;
                    ((SpinEditProperties)column.PropertiesEdit).MaxValue = 90;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "LongitudeColumn";
                    column.Caption = Translator.TemplateLongitudeColumn;
                    column.ColumnType = MVCxGridViewColumnType.SpinEdit;
                    ((SpinEditProperties)column.PropertiesEdit).NumberFormat = SpinEditNumberFormat.Number;
                    ((SpinEditProperties)column.PropertiesEdit).NumberType = SpinEditNumberType.Float;
                    ((SpinEditProperties)column.PropertiesEdit).MinValue = -180;
                    ((SpinEditProperties)column.PropertiesEdit).MaxValue = 180;
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "Detail2Name";
                column.Caption = Translator.TemplateDetail2Name;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "Detail2Description";
                column.Caption = Translator.TemplateDetail2Description;
                column.ColumnType = MVCxGridViewColumnType.Memo;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertDate";
                column.Caption = Translator.InsertDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertedBy";
                column.Caption = Translator.InsertedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var insertedBy = (int)DataBinder.Eval(content.DataItem, column.FieldName);

                    var html = (HtmlHelper<TemplateDetail2GridModel>)HttpContext.Current.Session["Html" + Name];;
                    html.DevExpress().HyperLink(hpls =>
                    {
                        hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                        hpls.Properties.Text = Translator.InsertedBy;
                        hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                        hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + insertedBy.ToString() + @"
    });
}";
                    }).Render();
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdateDate";
                column.Caption = Translator.UpdateDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdatedBy";
                column.Caption = Translator.UpdatedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var updatedBy = (int?)DataBinder.Eval(content.DataItem, column.FieldName);

                    if (updatedBy.HasValue)
                    {
                        var html = (HtmlHelper<TemplateDetail2GridModel>)HttpContext.Current.Session["Html" + Name];;
                        html.DevExpress().HyperLink(hpls =>
                        {
                            hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                            hpls.Properties.Text = Translator.UpdatedBy;
                            hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                            hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + updatedBy.ToString() + @"
    });
}";
                        }).Render();
                    }
                });
            });


            Data = () => IoC.Resolve<ITemplateDetail2Service>().GetEx(p => !p.IsDeleted);
        }

        protected override void NameChanged()
        {
            base.NameChanged();

            MenuModel.AddFormWidth = 900;
            MenuModel.EditFormWidth = 600;

            MenuModel.UseStandardToolbar(true);
        }

    }
}