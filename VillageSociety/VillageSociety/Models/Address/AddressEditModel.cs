﻿using DevExpress.Web;
using DevExpress.Web.Mvc;
using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VillageSociety.Models
{
    public class AddressEditModel : EditModel
    {
        [Display(Name = "AddressCountry", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? CountryId { get; set; }

        [Display(Name = "AddressCity", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? CityId { get; set; }

        [Display(Name = "AddressDistrict", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? DistrictId { get; set; }

        [Display(Name = "FullAddress", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(512, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string FullAddress { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Translator))]
        [StringLength(512, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string FullAddressDescription { get; set; }

        [Display(Name = "PostalCode", ResourceType = typeof(Translator))]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string PostalCode { get; set; }

        [Display(Name = "Latitude", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public double? Latitude { get; set; }

        [Display(Name = "Longitude", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public double? Longitude { get; set; }

    }
}