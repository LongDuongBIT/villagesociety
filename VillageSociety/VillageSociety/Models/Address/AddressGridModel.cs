﻿using Core;
using Service;
using Internationalization;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web;
using DevExpress.Web.Mvc;
using DevExpress.Web.Mvc.UI;

namespace VillageSociety.Models
{
    public class AddressGridModel : GridModel
    {
        public AddressGridModel()
        {
            Settings.Columns.Add(column =>
            {
                column.FieldName = "Id";
                column.Width = Unit.Pixel(50);
            });

            Settings.Columns.AddBand(band =>
            {
                band.Name = "bndCountry";
                band.Caption = Translator.AddressCountry;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "CountryCode";
                    column.Caption = Translator.CountryCode;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "CountryName";
                    column.Caption = Translator.CountryName;
                });
            });

            Settings.Columns.AddBand(band =>
            {
                band.Name = "bndCity";
                band.Caption = Translator.AddressCity;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "CityName";
                    column.Caption = Translator.CityName;
                });
            });

            Settings.Columns.AddBand(band =>
            {
                band.Name = "bndDistrict";
                band.Caption = Translator.AddressDistrict;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "DistrictName";
                    column.Caption = Translator.DistrictName;
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "FullAddress";
                column.Caption = Translator.FullAddress;
                column.ColumnType = MVCxGridViewColumnType.Memo;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "FullAddressDescription";
                column.Caption = Translator.Description;
                column.ColumnType = MVCxGridViewColumnType.Memo;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "PostalCode";
                column.Caption = Translator.PostalCode;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "Latitude";
                column.Caption = Translator.Latitude;
                column.ColumnType = MVCxGridViewColumnType.SpinEdit;
                ((SpinEditProperties)column.PropertiesEdit).NumberFormat = SpinEditNumberFormat.Number;
                ((SpinEditProperties)column.PropertiesEdit).NumberType = SpinEditNumberType.Float;
                ((SpinEditProperties)column.PropertiesEdit).MinValue = -90;
                ((SpinEditProperties)column.PropertiesEdit).MaxValue = 90;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "Longitude";
                column.Caption = Translator.Longitude;
                column.ColumnType = MVCxGridViewColumnType.SpinEdit;
                ((SpinEditProperties)column.PropertiesEdit).NumberFormat = SpinEditNumberFormat.Number;
                ((SpinEditProperties)column.PropertiesEdit).NumberType = SpinEditNumberType.Float;
                ((SpinEditProperties)column.PropertiesEdit).MinValue = -180;
                ((SpinEditProperties)column.PropertiesEdit).MaxValue = 180;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertDate";
                column.Caption = Translator.InsertDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertedBy";
                column.Caption = Translator.InsertedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var insertedBy = (int)DataBinder.Eval(content.DataItem, column.FieldName);

                    var html = (HtmlHelper<AddressGridModel>)HttpContext.Current.Session["Html" + Name];;
                    html.DevExpress().HyperLink(hpls =>
                    {
                        hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                        hpls.Properties.Text = Translator.InsertedBy;
                        hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                        hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + insertedBy.ToString() + @"
    });
}";
                    }).Render();
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdateDate";
                column.Caption = Translator.UpdateDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdatedBy";
                column.Caption = Translator.UpdatedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var updatedBy = (int?)DataBinder.Eval(content.DataItem, column.FieldName);

                    if (updatedBy.HasValue)
                    {
                        var html = (HtmlHelper<AddressGridModel>)HttpContext.Current.Session["Html" + Name];;
                        html.DevExpress().HyperLink(hpls =>
                        {
                            hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                            hpls.Properties.Text = Translator.UpdatedBy;
                            hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                            hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + updatedBy.ToString() + @"
    });
}";
                        }).Render();
                    }
                });
            });


            Data = () => IoC.Resolve<IAddressService>().GetEx(p => !p.IsDeleted);
        }

        protected override void NameChanged()
        {
            base.NameChanged();

            MenuModel.UseStandardToolbar(true);
        }

    }
}