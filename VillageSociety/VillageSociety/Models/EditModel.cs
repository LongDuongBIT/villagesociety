﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class EditModel
    {
        public string Name { get; set; }
        public bool IsNewRecord { get; set; }
        public string SuccessMethod { get; set; }
        public string CancelMethod { get; set; }
        public int Id { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }

        public string MasterController { get; set; }
    }
}