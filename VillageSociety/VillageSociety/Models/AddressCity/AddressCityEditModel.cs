﻿using DevExpress.Web;
using DevExpress.Web.Mvc;
using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VillageSociety.Models
{
    public class AddressCityEditModel : EditModel
    {
        [Display(Name = "AddressCountry", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? CountryId { get; set; }

        [Display(Name = "CityName", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string CityName { get; set; }


        [Display(Name = "Description", ResourceType = typeof(Translator))]
        [StringLength(512, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string CityDescription { get; set; }

    }
}