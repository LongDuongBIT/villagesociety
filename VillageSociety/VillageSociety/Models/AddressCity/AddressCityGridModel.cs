﻿using Core;
using Service;
using Internationalization;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web;
using DevExpress.Web.Mvc;
using DevExpress.Web.Mvc.UI;

namespace VillageSociety.Models
{
    public class AddressCityGridModel : GridModel
    {
        public AddressCityGridModel()
        {
            Settings.Columns.Add(column =>
            {
                column.FieldName = "Id";
                column.Width = Unit.Pixel(50);
            });

            Settings.Columns.AddBand(band =>
            {
                band.Name = "bndCountry";
                band.Caption = Translator.AddressCountry;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "CountryCode";
                    column.Caption = Translator.CountryCode;
                });

                band.Columns.Add(column =>
                {
                    column.FieldName = "CountryName";
                    column.Caption = Translator.CountryName;
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "CityName";
                column.Caption = Translator.CityName;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "CityDescription";
                column.Caption = Translator.Description;
                column.ColumnType = MVCxGridViewColumnType.Memo;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertDate";
                column.Caption = Translator.InsertDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertedBy";
                column.Caption = Translator.InsertedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var insertedBy = (int)DataBinder.Eval(content.DataItem, column.FieldName);

                    var html = (HtmlHelper<AddressCityGridModel>)HttpContext.Current.Session["Html" + Name];;
                    html.DevExpress().HyperLink(hpls =>
                    {
                        hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                        hpls.Properties.Text = Translator.InsertedBy;
                        hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                        hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + insertedBy.ToString() + @"
    });
}";
                    }).Render();
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdateDate";
                column.Caption = Translator.UpdateDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdatedBy";
                column.Caption = Translator.UpdatedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var updatedBy = (int?)DataBinder.Eval(content.DataItem, column.FieldName);

                    if (updatedBy.HasValue)
                    {
                        var html = (HtmlHelper<AddressCityGridModel>)HttpContext.Current.Session["Html" + Name];;
                        html.DevExpress().HyperLink(hpls =>
                        {
                            hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                            hpls.Properties.Text = Translator.UpdatedBy;
                            hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                            hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + updatedBy.ToString() + @"
    });
}";
                        }).Render();
                    }
                });
            });


            Data = () => IoC.Resolve<IAddressCityService>().GetEx(p => !p.IsDeleted);
        }

        protected override void NameChanged()
        {
            base.NameChanged();

            MenuModel.AddFormWidth = 900;
            MenuModel.EditFormWidth = 600;

            MenuModel.UseStandardToolbar(true);
        }

    }
}