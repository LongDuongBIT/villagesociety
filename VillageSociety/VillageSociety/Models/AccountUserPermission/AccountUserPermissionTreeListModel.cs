﻿using Core;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.Mvc;
using Internationalization;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VillageSociety.Models
{
    public class AccountUserPermissionTreeListModel : TreeListModel
    {
        public AccountUserPermissionTreeListModel()
        {
            Settings.SettingsSelection.AllowSelectAll = false;

            Settings.Columns.Add(column =>
            {
                column.FieldName = "Id";
                column.Width = Unit.Pixel(50);
                column.Visible = false;
            });
            Settings.Columns.Add(column =>
            {
                column.FieldName = "PermissionMapName";
                column.Caption = Translator.PermissionName;
                column.Width = 500;

                column.SetDataCellTemplateContent(content =>
                {
                    var value = (string)DataBinder.Eval(content.DataItem, column.FieldName);
                    
                    var permissionId = (int?)DataBinder.Eval(content.DataItem, "PermissionId");
                    
                    var isDeletedUserPermission = (bool?)DataBinder.Eval(content.DataItem, "IsDeletedUserPermission");
                    var inUserPermission = (isDeletedUserPermission.HasValue && !isDeletedUserPermission.Value);
                    
                    var isDeletedRolePermission = (bool?)DataBinder.Eval(content.DataItem, "IsDeletedRolePermission");
                    var inRolePermission = (isDeletedRolePermission.HasValue && !isDeletedRolePermission.Value);

                    var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];

                    if (permissionId.HasValue)
                    {
                        var text = @"<span id=""spn" + column.FieldName + Name + content.NodeKey + @""" style=""font-size: 12px; font-family: Tahoma; color: #21218f;"">" + value + "</span>";

                        if (inRolePermission && !inUserPermission)
                        {
                            if (content.Selected)
                            {
                                viewContext.Writer.Write(text + @"<span id=""spn" + column.FieldName + Name + content.NodeKey + @""" style=""font-size: 12px; font-family: Courier; color: #008000; font-style: italic; padding-left: 20px"">" + Translator.ActiveInRole + "</span>");
                            }
                            else
                            {
                                viewContext.Writer.Write(text + @"<span id=""spn" + column.FieldName + Name + content.NodeKey + @""" style=""font-size: 12px; font-family: Courier; color: #b33434; font-style: italic; text-decoration: underline;  padding-left: 20px"">" + Translator.RoleWillBeOverridden + "</span>");
                            }
                        }
                        else if (inRolePermission && inUserPermission)
                        {
                            if (content.Selected)
                            {
                                viewContext.Writer.Write(text + @"<span id=""spn" + column.FieldName + Name + content.NodeKey + @""" style=""font-size: 12px; font-family: Courier; color: #008000; font-style: italic; text-decoration: underline;  padding-left: 20px"">" + Translator.RoleWillBeProtected + "</span>");
                            }
                            else
                            {
                                viewContext.Writer.Write(text + @"<span id=""spn" + column.FieldName + Name + content.NodeKey + @""" style=""font-size: 12px; font-family: Courier; color: #b33434; font-style: italic; text-decoration: line-through;  padding-left: 20px"">" + Translator.RoleOverridden + "</span>");
                            }
                        }
                        else if (!inRolePermission && inUserPermission && content.Selected)
                        {
                            viewContext.Writer.Write(text + @"<span id=""spn" + column.FieldName + Name + content.NodeKey + @""" style=""font-size: 12px; font-family: Courier; color: #008000; font-style: italic; padding-left: 20px"">" + Translator.ExtraPermission + "</span>");
                        }
                        else if ((!inUserPermission && !inRolePermission) && content.Selected)
                        {
                            viewContext.Writer.Write(text + @"<span id=""spn" + column.FieldName + Name + content.NodeKey + @""" style=""font-size: 12px; font-family: Courier; color: #008000; font-style: italic; text-decoration: underline;  padding-left: 20px"">" + Translator.ExtraPermissionWillBeAdded + "</span>");
                        }
                        else if ((inUserPermission && !inRolePermission) && !content.Selected)
                        {
                            viewContext.Writer.Write(text + @"<span id=""spn" + column.FieldName + Name + content.NodeKey + @""" style=""font-size: 12px; font-family: Courier; color: #008000; font-style: italic; text-decoration: underline;  padding-left: 20px"">" + Translator.ExtraPermissionWillBeRemoved + "</span>");
                        }
                        else
                        {
                            viewContext.Writer.Write(text);
                        }
                    }
                    else
                    {
                        var text = @"<span id=""spn" + column.FieldName + Name + content.NodeKey + @""" style=""font-size: 12px; font-family: Tahoma; color: black; font-weight: bold"">" + value + "</span>";

                        viewContext.Writer.Write(text);
                    }

                });
            });


            Settings.Columns.Add(column =>
            {
                column.FieldName = "PermissionMapDescription";
                column.Caption = Translator.Description;
                column.Visible = false;
            });
            Settings.Columns.Add(column =>
            {
                column.FieldName = "Username";
                column.Caption = Translator.Username;
                column.Visible = false;
            });
            Settings.Columns.Add(column =>
            {
                column.FieldName = "UserEMail";
                column.Caption = Translator.EMail;
                column.Visible = false;
            });
            Settings.Columns.Add(column =>
            {
                column.FieldName = "FirstName";
                column.Caption = Translator.FirstName;
                column.Visible = false;
            });
            Settings.Columns.Add(column =>
            {
                column.FieldName = "LastName";
                column.Caption = Translator.LastName;
                column.Visible = false;
            });
            
            Settings.DataBound = (s, e) =>
            {
                MVCxTreeList trl = (MVCxTreeList)s;
                var iterator = trl.CreateNodeIterator();
                var node = iterator.Current;

                while (node != null)
                {
                    if (node != trl.RootNode)
                    {
                        var permissionId = (int?)node.GetValue("PermissionId");
                        
                        var isDeletedUserPermission = (bool?)node.GetValue("IsDeletedUserPermission");
                        var inUserPermission = (isDeletedUserPermission.HasValue && !isDeletedUserPermission.Value);

                        var isDeletedRolePermission = (bool?)node.GetValue("IsDeletedRolePermission");
                        var inRolePermission = (isDeletedRolePermission.HasValue && !isDeletedRolePermission.Value);
                        
                        if (permissionId.HasValue)
                        {
                            node.Selected = inUserPermission ^ inRolePermission;
                        }
                    }
                    node = iterator.GetNext();
                }
            };

            Data = IoC.Resolve<IAccountPermissionMapService>().GetWithUser(null, Security.LanguageId).ToList<BaseEntity>();
        }
    }
}