﻿using DevExpress.Web;
using DevExpress.Web.Mvc;
using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VillageSociety.Models
{
    public class TemplateUserEditModel : EditModel
    {
        [Display(Name = "TemplateMaster", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? MasterId { get; set; }
        public int[] Ids { get; set; }

    }
}