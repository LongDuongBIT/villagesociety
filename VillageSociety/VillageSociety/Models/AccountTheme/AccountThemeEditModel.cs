﻿using DevExpress.Web;
using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VillageSociety.Models
{
    public class AccountThemeEditModel : EditModel
    {
        [Display(Name = "SystemVillage", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? VillageId { get; set; }

        [Display(Name = "FirstName", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string FirstName { get; set; }

        [Display(Name = "LastName", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string LastName { get; set; }

        [Display(Name = "Gender", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public bool? IsMale { get; set; }

        [Display(Name = "Picture", ResourceType = typeof(Translator))]
        [StringLength(1024, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string PicturePath { get; set; }

        [Display(Name = "Father", ResourceType = typeof(Translator))]
        public int? Father { get; set; }

        [Display(Name = "Mother", ResourceType = typeof(Translator))]
        public int? Mother { get; set; }
    }
}