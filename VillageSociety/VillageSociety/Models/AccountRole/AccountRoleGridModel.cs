﻿using Core;
using DevExpress.Web.Mvc;
using Internationalization;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace VillageSociety.Models
{
    public class AccountRoleGridModel : GridModel
    {
        public AccountRoleGridModel()
        {
            Settings.Columns.Add(column =>
            {
                column.FieldName = "Id";
                column.Width = Unit.Pixel(50);
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "RoleName";
                column.Caption = Translator.RoleName;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "RoleDescription";
                column.Caption = Translator.Description;
                column.ColumnType = MVCxGridViewColumnType.Memo;
            });

            var items = IoC.Resolve<IAccountRoleService>().GetWithLanguage(p => !p.IsDeleted, Security.LanguageId).ToList();

            Data = () => IoC.Resolve<IAccountRoleService>().GetWithLanguage(p => !p.IsDeleted, Security.LanguageId);

        }

        protected override void NameChanged()
        {
            base.NameChanged();

            MenuModel.UseStandardToolbar(true);
        }

    }
}