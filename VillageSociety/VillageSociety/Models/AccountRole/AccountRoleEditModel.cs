﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class AccountRoleEditModel : EditModel
    {
        [Display(Name = "RoleName", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string RoleName { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Translator))]
        [StringLength(512, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string RoleDescription { get; set; }
    }
}