﻿using Core;
using DevExpress.Web.Mvc;
using Internationalization;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VillageSociety.Models
{
    public class AccountRolePermissionTreeListModel : TreeListModel
    {
        public AccountRolePermissionTreeListModel()
        {
            Settings.SettingsSelection.AllowSelectAll = false;

            Settings.Columns.Add(column =>
            {
                column.FieldName = "Id";
                column.Width = Unit.Pixel(50);
                column.Visible = false;
            });
            Settings.Columns.Add(column =>
            {
                column.FieldName = "PermissionMapName";
                column.Caption = Translator.PermissionName;

                column.Width = 500;

                column.SetDataCellTemplateContent(content =>
                {
                    var value = (string)DataBinder.Eval(content.DataItem, column.FieldName);

                    var permissionId = (int?)DataBinder.Eval(content.DataItem, "PermissionId");
                    var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];

                    if (permissionId.HasValue)
                    {
                        var text = @"<span id=""spn" + column.FieldName + Name + content.NodeKey + @""" style=""font-size: 12px; font-family: Tahoma; color: #21218f;"">" + value + "</span>";
                        viewContext.Writer.Write(text);
                    }
                    else
                    {
                        var text = @"<span id=""spn" + column.FieldName + Name + content.NodeKey + @""" style=""font-size: 12px; font-family: Tahoma; color: black; font-weight: bold"">" + value + "</span>";
                        viewContext.Writer.Write(text);
                    }
                });
            });
            Settings.Columns.Add(column =>
            {
                column.FieldName = "PermissionMapDescription";
                column.Caption = Translator.Description;
                column.Visible = false;
            });
            
            Settings.DataBound = (s, e) =>
            {
                MVCxTreeList trl = (MVCxTreeList)s;
                DevExpress.Web.ASPxTreeList.TreeListNodeIterator iterator = trl.CreateNodeIterator();
                DevExpress.Web.ASPxTreeList.TreeListNode node = iterator.Current;
                while (node != null)
                {
                    if (node != trl.RootNode)
                    {
                        int? permissionId = (int?)node.GetValue("PermissionId");

                        if (permissionId.HasValue)
                        {
                            node.Selected = (bool)node.GetValue("InRolePermission");
                        }
                    }
                    node = iterator.GetNext();
                }
            };

            Data = IoC.Resolve<IAccountPermissionMapService>().GetWithRole(null, Security.LanguageId).ToList<BaseEntity>();
        }
    }
}