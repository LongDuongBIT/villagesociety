﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class AccountUserEditModel : EditModel
    {
        [Display(Name = "AccountPerson", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? PersonId { get; set; }

        [Display(Name = "AccountRole", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? RoleId { get; set; }

        [Display(Name = "Username", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string Username { get; set; }

        [Display(Name = "EMail", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string UserEMail { get; set; }

        [Display(Name = "Password", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "ConfirmPassword", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        [Compare("Password", ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Compare")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Theme", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? ThemeId { get; set; }

        [Display(Name = "Language", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? LanguageId { get; set; }

        [Display(Name = "IsSystemAdmin", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public bool? IsSystemAdmin { get; set; }
    }
}