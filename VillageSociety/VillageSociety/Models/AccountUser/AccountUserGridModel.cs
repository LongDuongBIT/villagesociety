﻿using Core;
using Service;
using Internationalization;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web;
using DevExpress.Web.Mvc;
using DevExpress.Web.Mvc.UI;

namespace VillageSociety.Models
{
    public class AccountUserGridModel : GridModel
    {
        public AccountUserGridModel()
        {
            Settings.Columns.Add(column =>
            {
                column.FieldName = "Id";
                column.Width = Unit.Pixel(50);
            });

            Settings.Columns.AddBand(band =>
            {
                band.Name = "bndPerson";
                band.Caption = Translator.AccountPerson;
                band.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                band.Columns.Add(column =>
                {
                    column.FieldName = "FirstName";
                    column.Caption = Translator.FirstName;
                });
                band.Columns.Add(column =>
                {
                    column.FieldName = "LastName";
                    column.Caption = Translator.LastName;
                });
            });


            Settings.Columns.Add(column =>
            {
                column.FieldName = "Username";
                column.Caption = Translator.Username;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UserEMail";
                column.Caption = Translator.EMail;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "Password";
                column.Caption = Translator.Password;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "ThemeName";
                column.Caption = Translator.EMail;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "LanguageName";
                column.Caption = Translator.EMail;
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "IsSystemAdmin";
                column.Caption = Translator.IsSystemAdmin;
                column.ColumnType = MVCxGridViewColumnType.CheckBox;

                column.SetDataItemTemplateContent(content =>
                {
                    bool value = (bool)DataBinder.Eval(content.DataItem, column.FieldName);
                    var viewContext = (ViewContext)HttpContext.Current.Session["ViewContext" + Name];
                    viewContext.Writer.Write(@"<img src=""/Content/Icon/" + (value ? "Checked" : "Unchecked") + @"/16.png"" alt="""" />");
                });

            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertDate";
                column.Caption = Translator.InsertDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "InsertedBy";
                column.Caption = Translator.InsertedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var insertedBy = (int)DataBinder.Eval(content.DataItem, column.FieldName);

                    var html = (HtmlHelper<AccountUserGridModel>)HttpContext.Current.Session["Html" + Name];;
                    html.DevExpress().HyperLink(hpls =>
                    {
                        hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                        hpls.Properties.Text = Translator.InsertedBy;
                        hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                        hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + insertedBy.ToString() + @"
    });
}";
                    }).Render();
                });
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdateDate";
                column.Caption = Translator.UpdateDate;
                column.PropertiesEdit.DisplayFormatString = "dd.MM.yyyy hh:mm";
            });

            Settings.Columns.Add(column =>
            {
                column.FieldName = "UpdatedBy";
                column.Caption = Translator.UpdatedBy;

                column.SetDataItemTemplateContent(content =>
                {
                    var id = (int)content.KeyValue;
                    var updatedBy = (int?)DataBinder.Eval(content.DataItem, column.FieldName);

                    if (updatedBy.HasValue)
                    {
                        var html = (HtmlHelper<AccountUserGridModel>)HttpContext.Current.Session["Html" + Name];;
                        html.DevExpress().HyperLink(hpls =>
                        {
                            hpls.Name = "hpl" + column.FieldName + id.ToString() + Name;
                            hpls.Properties.Text = Translator.UpdatedBy;
                            hpls.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                            hpls.Properties.ClientSideEvents.Click = @"function(s, e) {
    ShowUserProfileInPopup({
        PopupControl: puc" + Name + @"Grid,
        Name: '" + Name + @"Grid',
        Id: " + updatedBy.ToString() + @"
    });
}";
                        }).Render();
                    }
                });
            });


            Data = () => IoC.Resolve<IAccountUserService>().GetEx(p => !p.IsDeleted);
        }

        protected override void NameChanged()
        {
            base.NameChanged();

            MenuModel.AddFormWidth = 900;
            MenuModel.EditFormWidth = 600;

            MenuModel.UseStandardToolbar(false);
        }

    }
}