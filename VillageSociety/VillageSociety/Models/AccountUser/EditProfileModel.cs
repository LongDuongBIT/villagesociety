﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class EditProfileModel : EditModel
    {
        [Display(Name = "FirstName", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string FirstName { get; set; }

        [Display(Name = "LastName", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string LastName { get; set; }

        [Display(Name = "Gender", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public bool? IsMale { get; set; }

        [Display(Name = "Picture", ResourceType = typeof(Translator))]
        [StringLength(1024, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string PicturePath { get; set; }

        [Display(Name = "Username", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string Username { get; set; }

        [Display(Name = "EMail", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string UserEMail { get; set; }

        [Display(Name = "CurrentPassword", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Display(Name = "NewPassword", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "ConfirmPassword", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Compare")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Theme", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? ThemeId { get; set; }

        [Display(Name = "Language", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        public int? LanguageId { get; set; }

        public bool? IsSystemAdmin { get; set; }
    }
}