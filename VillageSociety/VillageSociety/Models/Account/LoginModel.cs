﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class LoginModel
    {
        [Display(Name = "Username", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 1, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string Username { get; set; }

        [Display(Name = "Password", ResourceType = typeof(Translator))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 1, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "RememberMe", ResourceType = typeof(Translator))]
        public bool? RememberMe { get; set; }
    }
}