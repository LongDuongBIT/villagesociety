﻿using Internationalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class GridTitlePanelModel
    {
        public GridModel GridModel { get; set; }
        public string ImageUrl { get; set; }
        public string Caption { get; set; }

        public GridTitlePanelModel()
        {

        }
        public GridTitlePanelModel(GridModel gridModel, string resourceName)
        {
            GridModel = gridModel;

            string recordCaption = Translator.ResourceManager.GetString(resourceName);

            ImageUrl = string.Format("/Content/Icon/{0}/24.png", resourceName);
            Caption = recordCaption;
        }
    }
}