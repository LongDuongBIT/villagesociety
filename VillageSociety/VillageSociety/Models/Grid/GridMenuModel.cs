﻿using DevExpress.Web.Mvc;
using Internationalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VillageSociety.Models
{
    public class GridMenuModel
    {
        public readonly string Caption;

        public MenuSettings Settings { get; set; }

        public GridModel GridModel { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string EditModelFullName { get; set; }

        public string AddHeaderImageUrl { get; set; }
        public string AddHeaderText { get; set; }
        public int AddFormWidth { get; set; }
        public int AddFormHeight { get; set; }
        public string AddShownMethod { get; set; }        

        public string EditHeaderImageUrl { get; set; }
        public string EditHeaderText { get; set; }
        public int EditFormWidth { get; set; }
        public int EditFormHeight { get; set; }
        public string EditShownMethod { get; set; }

        public void AddCustomItem(MVCxMenuItemCollection parentItem, string name, string text, string imageUrl, string imageUrlDisabled, string toolTip, bool clientEnabled, string itemClickScript)
        {
            parentItem.Add(item =>
            {
                item.Name = name;
                item.Text = text;
                item.Image.Url = imageUrl;
                item.Image.UrlDisabled = imageUrlDisabled;
                item.ToolTip = toolTip;
                item.ClientEnabled = clientEnabled;
            });

            if (itemClickScript != null)
            {
                bool ifExisted = Settings.ClientSideEvents.ItemClick.Contains("if (name == 'mni");
                Settings.ClientSideEvents.ItemClick += (ifExisted ? " else " : @"
    ") + @"if (name == '" + name + "') {" + itemClickScript.Replace("\r\n", "\r\n\t") + @"
    }";
            }
        }

        public void AddCustomItem(Permission permission, MVCxMenuItemCollection parentItem, string name, string text, string imageUrl, string imageUrlDisabled, string toolTip, bool clientEnabled, string itemClickScript)
        {
            if (Security.IsUserInPermission(permission))
            {
                AddCustomItem(parentItem, name, text, imageUrl, imageUrlDisabled, toolTip, clientEnabled, itemClickScript);
            }
        }

        public void AddCustomItem(string permissionName, MVCxMenuItemCollection parentItem, string name, string text, string imageUrl, string imageUrlDisabled, string toolTip, bool clientEnabled, string itemClickScript)
        {
            Permission permission;
            if (Enum.TryParse(permissionName, out permission) && Security.IsUserInPermission(permission))
            {
                AddCustomItem(parentItem, name, text, imageUrl, imageUrlDisabled, toolTip, clientEnabled, itemClickScript);
            }
        }

        public void AddAddItem()
        {
            AddCustomItem(
                "Insert" + ControllerName,
                Settings.Items,
                "mniAdd", 
                Translator.Add, 
                "~/Content/Icon/Add/24.png", 
                "~/Content/Icon/Add/D24.png", 
                string.Format(Translator.AddNewRecord, Caption), 
                true, @"
    ShowAddFormInPopup({
        PopupControl: puc,
        Name: '" + GridModel.Name + @"Grid',
        ControllerName: '" + ControllerName + @"',
        ActionName: '" + ActionName + @"',
        EditModelFullName: '" + EditModelFullName + @"',
        AddHeaderImageUrl: '" + AddHeaderImageUrl + @"',
        AddHeaderText: '" + AddHeaderText.Js() + @"',
        Width: " + AddFormWidth.ToString() + @",
        Height: " + AddFormHeight.ToString() + "," + (AddShownMethod != null ? @"
        AddShownMethod: " + AddShownMethod + "," : "") + @"
        EditHeaderImageUrl: '" + EditHeaderImageUrl + @"',
        EditHeaderText: '" + EditHeaderText.Js() + @"'," + (EditShownMethod != null ? @"
        EditShownMethod: " + EditShownMethod + "," : "") + @"
        SuccessMethod: function(jsonResult) { puc" + GridModel.Name + @"Grid.Hide(); grv" + GridModel.Name + @".Refresh(); },
        CancelMethod: function() { puc" + GridModel.Name + @"Grid.Hide(); }
    });");
        }

        public void AddEditItem()
        {
            AddCustomItem(
                "Update" + ControllerName,
                Settings.Items,
                "mniEdit",
                Translator.Edit,
                "~/Content/Icon/Edit/24.png",
                "~/Content/Icon/Edit/D24.png",
                string.Format(Translator.EditTheSelectedRecord, Caption), 
                false, @"
    grv.GetSelectedFieldValues('Id', function(selectedValues) {
        ShowEditFormInPopup({
            PopupControl: puc,
            Name: '" + GridModel.Name + @"Grid',
            ControllerName: '" + ControllerName + @"',
            ActionName: '" + ActionName + @"',
            EditModelFullName: '" + EditModelFullName + @"',
            EditHeaderImageUrl: '" + EditHeaderImageUrl + @"',
            EditHeaderText: '" + EditHeaderText.Js() + @"',
            Width: " + EditFormWidth.ToString() + @",
            Height: " + EditFormHeight.ToString() + "," + (EditShownMethod != null ? @"
            EditShownMethod:" + EditShownMethod + "," : "") + @"
            Id: selectedValues[0],
            SuccessMethod: function(jsonResult) { puc" + GridModel.Name + @"Grid.Hide(); grv" + GridModel.Name + @".Refresh(); },
            CancelMethod: function() { puc" + GridModel.Name + @"Grid.Hide(); }
        });
    });");
        }

        public void AddDeleteItem()
        {
            AddCustomItem(
                "Delete" + ControllerName,
                Settings.Items,
                "mniDelete",
                Translator.Delete,
                "~/Content/Icon/Delete/24.png",
                "~/Content/Icon/Delete/D24.png",
                Translator.DeleteTheSelectedRecord_s, 
                false, @"
    ShowOkCancel({
        Message: String.format('" + Translator.AreYouSureYouWantToDeleteRecord_s.Js() + @"', selectedCount),
        OkScript: function(s, e) {
            grv.GetSelectedFieldValues('Id', function(selectedValues) {
                var deleteModel = {
                    Ids: selectedValues
                };

                $.ajax({
                    url: ""/" + ControllerName + @"/Delete"",
                    type: ""post"",
                    data: JSON.stringify(deleteModel),
                    datatype: ""json"",
                    contentType: ""application/json; charset=utf-8"",
                    success: function (jsonModel) {
                        pucMessage.Hide();
                        grv" + GridModel.Name + @".cpEndCallback = 'UnselectRows';
                        grv" + GridModel.Name + @".Refresh();
                    },
                    error: function (xhr) {
                        alert('Unexpected Error: ' + xhr.statusText);
                    }
                });
            });
        }
    });");

        }

        public void AddUpDownItem()
        {
            Permission permission;
            if (Enum.TryParse("Update" + ControllerName, out permission) && Security.IsUserInPermission(permission))
            {
                Settings.Items.Add(item =>
                {
                    item.Name = "mniUp";
                    item.Text = Translator.Up;
                    item.Image.Url = "~/Content/Icon/ArrowUp/24.png";
                    item.Image.UrlDisabled = "~/Content/Icon/ArrowUp/D24.png";
                    item.ClientEnabled = false;
                    item.ToolTip = string.Format(Translator.MoveTheSelectedRecordUp, Caption);
                });

                Settings.Items.Add(item =>
                {
                    item.Name = "mniDown";
                    item.Text = Translator.Down;
                    item.Image.Url = "~/Content/Icon/ArrowDown/24.png";
                    item.Image.UrlDisabled = "~/Content/Icon/ArrowDown/D24.png";
                    item.ClientEnabled = false;
                    item.ToolTip = string.Format(Translator.MoveTheSelectedRecordDown, Caption);
                });

                bool ifExisted = Settings.ClientSideEvents.ItemClick.Contains("if (name == 'mni");

                Settings.ClientSideEvents.ItemClick += (ifExisted ? " else " : @"
    ") + @"if (name == 'mniUp' || name == 'mniDown') {
        grv.GetSelectedFieldValues('Id', function(selectedValues) {
            var data = {
                id1: selectedValues[0],
                id2: (selectedValues.length == 2 ? selectedValues[1] : null)
            };

            $.ajax({
                url: '/" + ControllerName + @"/' + name.substr(3),
                type: ""post"",
                data: JSON.stringify(data),
                datatype: ""json"",
                contentType: ""application/json; charset=utf-8"",
                success: function (jsonModel) {
                    if (jsonModel.Result) {
                        grv" + GridModel.Name + @".Refresh();
                    }
                },
                error: function (xhr) {
                    alert('Unexpected Error: ' + xhr.statusText);
                }
            });
        });
    }";
            }
        }

        public void AddRefreshItem()
        {
            AddCustomItem(
                "Read" + ControllerName,
                Settings.Items,
                "mniRefresh",
                Translator.Refresh,
                "~/Content/Icon/Refresh/24.png",
                "~/Content/Icon/Refresh/D24.png",
                Translator.Refresh, 
                true, @"
    grv.Refresh();");
        }

        public void AddColumnItem()
        {
            AddCustomItem(
                "Read" + ControllerName,
                Settings.Items,
                "mniColumn",
                Translator.Columns,
                "~/Content/Icon/Column/24.png",
                "~/Content/Icon/Column/D24.png",
                Translator.ShowDragAndDropColumns, 
                true, @"
    grv.ShowCustomizationWindow();");
        }

        public void AddExportItem()
        {
            Permission permission;
            if (Enum.TryParse("Export" + ControllerName, out permission) && Security.IsUserInPermission(permission))
            {
                Settings.Items.Add(item =>
                {
                    item.Name = "mniExport";
                    item.VisibleIndex = 10;
                    item.Index = 10;
                    item.Text = Translator.Export;
                    item.Image.Url = "~/Content/Icon/Export/24.png";
                    item.Image.UrlDisabled = "~/Content/Icon/Export/D24.png";
                    item.ToolTip = Translator.ExportWholeShownData;

                    item.Items.Add(itemXls =>
                    {
                        itemXls.Name = "mniXls";
                        item.VisibleIndex = 20;
                        item.Index = 20;
                        itemXls.Text = Translator.ExcelXls;
                        itemXls.Image.Url = "~/Content/Icon/ExcelXls/24.png";
                        itemXls.Image.UrlDisabled = "~/Content/Icon/ExcelXls/D24.png";
                    });
                    item.Items.Add(itemXlsx =>
                    {
                        itemXlsx.Name = "mniXlsx";
                        item.VisibleIndex = 70;
                        item.Index = 70;
                        itemXlsx.Text = Translator.ExcelXlsx;
                        itemXlsx.Image.Url = "~/Content/Icon/ExcelXlsx/24.png";
                        itemXlsx.Image.UrlDisabled = "~/Content/Icon/ExcelXlsx/D24.png";
                    });
                    item.Items.Add(itemPdf =>
                    {
                        itemPdf.Name = "mniPdf";
                        item.VisibleIndex = 40;
                        item.Index = 40;
                        itemPdf.Text = Translator.AcrobatReader;
                        itemPdf.Image.Url = "~/Content/Icon/Pdf/24.png";
                        itemPdf.Image.UrlDisabled = "~/Content/Icon/Pdf/D24.png";
                    });
                    item.Items.Add(itemHtml =>
                    {
                        itemHtml.Name = "mniCsv";
                        item.VisibleIndex = 50;
                        item.Index = 50;
                        itemHtml.Text = "CSV (*.csv)";
                        itemHtml.Image.Url = "~/Content/Icon/Csv/24.png";
                        itemHtml.Image.UrlDisabled = "~/Content/Icon/Csv/D24.png";
                    });
                    item.Items.Add(itemHtml =>
                    {
                        itemHtml.Name = "mniRtf";
                        item.VisibleIndex = 60;
                        item.Index = 60;
                        itemHtml.Text = Translator.RichTextFormat;
                        itemHtml.Image.Url = "~/Content/Icon/Rtf/24.png";
                        itemHtml.Image.UrlDisabled = "~/Content/Icon/Rtf/D24.png";
                    });
                });

                bool ifExisted = Settings.ClientSideEvents.ItemClick.Contains("if (name == 'mni");

                Settings.ClientSideEvents.ItemClick += (ifExisted ? " else " : @"
    ") + @"if (name == 'mniXls' || name == 'mniXlsx' || name == 'mniPdf' || name == 'mniCsv' || name == 'mniRtf') {
        var data = {
            Submit: 'Export',
            Export: name.substr(3),
            ModelName: '" + GridModel.Name + @"'
        };

        document.getElementById('hdnSubmit" + GridModel.Name + @"Grid').value = JSON.stringify(data);
        document.forms['frm" + GridModel.Name + @"Grid'].submit();
    }";
            }
        }

        public void BeginAddItem()
        {
            Settings.Items.Clear();

            Settings.ClientSideEvents.ItemClick = @"function(s, e) {
    var name = e.item.name;
    var grv = grv" + GridModel.Name + @";
    var selectedCount = grv.GetSelectedRowCount();
    var puc = puc" + GridModel.Name + "Grid;";
        }

        public void EndAddItem()
        {
            Settings.ClientSideEvents.ItemClick += @"
}";
        }

        public void UseStandardToolbar(bool addUpDown)
        {
            BeginAddItem();
            AddAddItem();
            AddEditItem();
            AddDeleteItem();
            if (addUpDown)
            {
                AddUpDownItem();
            }
            AddRefreshItem();
            AddColumnItem();
            AddExportItem();
            EndAddItem();
        }

        public void UseEditToolbar()
        {
            Settings.Items.Clear();

            BeginAddItem();

            Permission permission;
            if (Enum.TryParse("Edit" + ControllerName, out permission) && Security.IsUserInPermission(permission))
            {

                Settings.Items.Add(item =>
                {
                    item.Name = "mniEdit";
                    item.Text = Translator.Edit;
                    item.Image.Url = "~/Content/Icon/Edit/24.png";
                    item.Image.UrlDisabled = "~/Content/Icon/Edit/D24.png";
                    item.ToolTip = string.Format(Translator.EditTheSelectedRecord, Caption);
                });

                EditHeaderImageUrl = string.Format("/Content/Icon/{0}/16.png", ControllerName);
                EditHeaderText = string.Format(Translator.EditTheRecord, Caption);

                bool ifExisted = Settings.ClientSideEvents.ItemClick.Contains("if (name == 'mni");

                Settings.ClientSideEvents.ItemClick += (ifExisted ? " else " : @"
    ") + @"if (name == 'mniEdit') {
        ShowAddFormInPopup({
            PopupControl: puc,
            Name: '" + GridModel.Name + @"Grid',
            ControllerName: '" + ControllerName + @"',
            ActionName: '" + ActionName + @"',
            EditModelFullName: '" + EditModelFullName + @"',
            AddHeaderImageUrl: '" + AddHeaderImageUrl + @"',
            AddHeaderText: '" + AddHeaderText.Js() + @"',
            Width: " + AddFormWidth.ToString() + @",
            Height: " + AddFormHeight.ToString() + "," + (AddShownMethod != null ? @"
            AddShownMethod: '" + AddShownMethod + "'," : "") + @"
            EditHeaderImageUrl: '" + EditHeaderImageUrl + @"',
            EditHeaderText: '" + EditHeaderText.Js() + @"'," + (EditShownMethod != null ? @"
            EditShownMethod: '" + EditShownMethod + "'," : "") + @"
            SuccessMethod: function(jsonResult) { puc" + GridModel.Name + @"Grid.Hide(); grv" + GridModel.Name + @".Refresh(); },
            CancelMethod: function() { puc" + GridModel.Name + @"Grid.Hide(); }
        });
    }";
            }

            AddRefreshItem();
            AddColumnItem();
            AddExportItem();
            EndAddItem();
        }

        public void UseReadOnlyToolbar()
        {
            BeginAddItem();
            AddRefreshItem();
            AddColumnItem();
            AddExportItem();
            EndAddItem();
        }

        public GridMenuModel(GridModel gridModel, string controllerName)
        {
            this.GridModel = gridModel;
            this.ControllerName = controllerName;
            this.Caption = Translator.ResourceManager.GetString(controllerName);

            AddHeaderImageUrl = string.Format("/Content/Icon/{0}/16.png", ControllerName);
            AddHeaderText = string.Format(Translator.AddNewRecord, Caption);

            EditHeaderImageUrl = string.Format("/Content/Icon/{0}/16.png", ControllerName);
            EditHeaderText = string.Format(Translator.EditTheRecord, Caption);

            Settings = new MenuSettings();
            Settings.Style.Value = "float: right";

            ControllerName = controllerName;
            ActionName = "DetailPartial";
            EditModelFullName = Type.GetType("VillageSociety.Models." + controllerName + "EditModel").FullName;

            AddFormWidth = 500;
            EditFormWidth = 500;
        }

    }
}