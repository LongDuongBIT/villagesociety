﻿using Core;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace VillageSociety.Models
{
    public class GridModel
    {
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name != value)
                {
                    name = value;
                    NameChanged();
                }
            }
        }

        public readonly string ControllerName;

        public GridViewSettings Settings { get; set; }
        public Func<IQueryable<BaseEntity>> Data { get; set; }
        public GridTitlePanelModel TitlePanelModel { get; set; }
        public GridMenuModel MenuModel { get; set; }

        public string MasterController { get; set; }

        public GridModel()
        {
            string typeName = this.GetType().Name;
            ControllerName = typeName.Remove(typeName.Length - "GridModel".Length);

            Settings = new DevExpress.Web.Mvc.GridViewSettings();
            TitlePanelModel = new GridTitlePanelModel(this, ControllerName);
            MenuModel = new GridMenuModel(this, ControllerName);

            Name = ControllerName;

            Settings.KeyFieldName = "Id";
            Settings.Width = Unit.Percentage(100);
            Settings.Settings.HorizontalScrollBarMode = ScrollBarMode.Auto;
            Settings.Settings.VerticalScrollBarMode = ScrollBarMode.Auto;

            Settings.SettingsPager.PageSize = 20;

            //Settings.EnableRowsCache = false;

            Settings.CommandColumn.Visible = true;
            Settings.CommandColumn.ShowSelectCheckbox = true;
            Settings.CommandColumn.Width = Unit.Pixel(40);
            Settings.CommandColumn.ShowClearFilterButton = true;
            Settings.CommandColumn.SelectAllCheckboxMode = DevExpress.Web.GridViewSelectAllCheckBoxMode.AllPages;

            Settings.SettingsContextMenu.Enabled = true;

            Settings.Settings.ShowFilterBar = DevExpress.Web.GridViewStatusBarMode.Visible;
            Settings.Settings.ShowFilterRow = true;
            Settings.Settings.ShowFilterRowMenu = true;
            Settings.Settings.ShowGroupPanel = true;

            Settings.Settings.ShowTitlePanel = true;

            Settings.SettingsBehavior.ColumnResizeMode = DevExpress.Web.ColumnResizeMode.Control;
            Settings.SettingsBehavior.EnableCustomizationWindow = true;
            Settings.SettingsPopup.CustomizationWindow.Width = Unit.Pixel(250);
            Settings.SettingsPopup.CustomizationWindow.Height = Unit.Pixel(500);

        }

        protected virtual void NameChanged()
        {
            Settings.Name = "grv" + Name;
            Settings.CallbackRouteValues = new { Controller = ControllerName, Action = "GridPartial", Name = Name };

            MenuModel.Settings.Name = "mnu" + Name;

        }
    }
}