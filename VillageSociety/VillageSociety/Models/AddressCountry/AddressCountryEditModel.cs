﻿using DevExpress.Web;
using DevExpress.Web.Mvc;
using Internationalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VillageSociety.Models
{
    public class AddressCountryEditModel : EditModel
    {
        [Display(Name = "CountryCode", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(2, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string CountryCode { get; set; }

        [Display(Name = "CountryName", ResourceType = typeof(Translator))]
        [Required(ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "Required")]
        [StringLength(50, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string CountryName { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Translator))]
        [StringLength(512, MinimumLength = 2, ErrorMessageResourceType = typeof(Translator), ErrorMessageResourceName = "StringLength")]
        public string CountryDescription { get; set; }

    }
}