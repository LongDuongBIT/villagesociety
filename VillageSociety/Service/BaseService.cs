﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class BaseService<TEntity> : IBaseService<TEntity>, IDisposable where TEntity : BaseEntity
    {
        internal IUnitOfWork uow;
        internal readonly IRepository<TEntity> repository;

        public BaseService(IUnitOfWork uow)
        {
            this.uow = uow;
            this.repository = uow.Repository<TEntity>();
        }

        public virtual IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null)
        {
            return repository.Get(filter);
        }

        public virtual TEntity First(Expression<Func<TEntity, bool>> filter)
        {
            return repository.First(filter);
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter)
        {
            return repository.FirstOrDefault(filter);
        }
        public virtual TEntity Single(Expression<Func<TEntity, bool>> filter)
        {
            return repository.Single(filter);
        }
        public virtual TEntity SingleOrDefault(Expression<Func<TEntity, bool>> filter)
        {
            return repository.SingleOrDefault(filter);
        }
        public virtual TEntity GetById(int id)
        {
            return repository.GetById(id);
        }
        public virtual void Insert(TEntity item)
        {
            repository.Insert(item);
        }
        public virtual void Update(TEntity item)
        {
            repository.Update(item);
        }
        public virtual void Edit(TEntity item, params string[] ignoreButSomeProperties)
        {
            repository.Edit(item, ignoreButSomeProperties);
        }
        public virtual void Delete(TEntity item)
        {
            repository.Delete(item);
        }
        public virtual void Restore(TEntity item, bool ignoreProperties)
        {
            repository.Restore(item, ignoreProperties);
        }

        public void Save()
        {
            uow.Save();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    uow.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public UniqueKeyException ParseUniqueKeyException(Exception exception, TEntity item)
        {
            return repository.ParseUniqueKeyException(exception, item);
        }

        public JsonResultModel ExceptionToJsonResult(Exception exception, TEntity item, bool isNewRecord)
        {
            return repository.ExceptionToJsonResult(exception, item, isNewRecord);
        }
    }
}
