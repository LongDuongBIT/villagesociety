﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class WindsorConfig
    {
        public static void Configure()
        {
            //WindsorRegistrar.Register(typeof(IRepository<>), typeof(Repository<>));
            //WindsorRegistrar.Register(typeof(IRowNumberedRepository<>), typeof(RowNumberedRepository<>));
            //WindsorRegistrar.Register(typeof(IUnitOfWork), typeof(UnitOfWork));

            WindsorRegistrar.RegisterAllFromAssemblies(typeof(IUnitOfWork).Assembly.FullName);
            WindsorRegistrar.RegisterAllFromAssemblies(typeof(UnitOfWork).Assembly.FullName);
            WindsorRegistrar.RegisterAllFromAssemblies(typeof(WindsorConfig).Assembly.FullName);
        }
    }
}
