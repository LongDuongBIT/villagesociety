﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class TemplateDetail2Service : RowNumberedService<TemplateDetail2>, ITemplateDetail2Service
    {
        public TemplateDetail2Service()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public TemplateDetail2Service(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<TemplateDetail2Ex> GetEx(Expression<Func<TemplateDetail2, bool>> filter = null)
        {
            filter = filter ?? (p => true);

            var items = repository.Get();
            var masters = uow.Repository<TemplateMaster>().Get();

            var query = from item in items.Where(filter)
                        join master in masters on item.MasterId equals master.Id

                        select new TemplateDetail2Ex
                        {
                            Id = item.Id,
                            Detail2Name = item.Detail2Name,
                            Detail2Description = item.Detail2Description,
                            RowNumber = item.RowNumber,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,

                            StringColumn = master.StringColumn,
                            MultilineColumn = master.MultilineColumn,
                            HtmlColumn = master.HtmlColumn,
                            PhoneNumberColumn = master.PhoneNumberColumn,
                            CreditCardNumberColumn = master.CreditCardNumberColumn,
                            LatitudeColumn = master.LatitudeColumn,
                            LongitudeColumn = master.LongitudeColumn
                        };

            return query;
        }

    }
}
