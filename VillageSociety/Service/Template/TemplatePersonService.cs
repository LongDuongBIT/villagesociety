﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class TemplatePersonService : BaseService<TemplatePerson>, ITemplatePersonService
    {
        public TemplatePersonService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public TemplatePersonService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<TemplatePersonEx> GetEx(Expression<Func<TemplatePerson, bool>> filter = null,
            Expression<Func<AccountPerson, bool>> filterPerson = null)
        {
            filter = filter ?? (p => true);
            filterPerson = filterPerson ?? (p => true);

            var items = repository.Get();
            var masters = uow.Repository<TemplateMaster>().Get();
            var persons = uow.Repository<AccountPerson>().Get();

            var query = from item in items.Where(filter)
                        join master in masters on item.MasterId equals master.Id
                        join person in persons.Where(filterPerson) on item.PersonId equals person.Id

                        select new TemplatePersonEx
                        {
                            Id = item.Id,
                            MasterId = item.MasterId,
                            PersonId = item.PersonId,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,

                            StringColumn = master.StringColumn,
                            MultilineColumn = master.MultilineColumn,
                            HtmlColumn = master.HtmlColumn,
                            PhoneNumberColumn = master.PhoneNumberColumn,
                            CreditCardNumberColumn = master.CreditCardNumberColumn,
                            LatitudeColumn = master.LatitudeColumn,
                            LongitudeColumn = master.LongitudeColumn,

                            FirstName = person.FirstName,
                            LastName = person.LastName
                        };

            return query;
        }

        public virtual void Edit(int masterId, int[] ids, DateTime updateDate, int updatedBy)
        {
            if (ids == null) ids = new int[] { };

            var repository = uow.Repository<TemplatePerson>();
            var items = repository.Get(p => p.MasterId == masterId);

            foreach (var item in items.Where(p => !p.IsDeleted && !ids.Contains(p.PersonId)))
            {
                item.DeleteDate = updateDate;
                item.DeletedBy = updatedBy;
                item.IsDeleted = true;

                repository.Delete(item);
            }

            foreach (var item in items.Where(p => p.IsDeleted && ids.Contains(p.PersonId)))
            {
                item.UpdateDate = updateDate;
                item.UpdatedBy = updatedBy;
                item.IsDeleted = false;

                repository.Restore(item, false);
            }

            foreach (var id in ids.Where(p => !items.Any(tp => tp.PersonId == p)))
            {
                var item = new TemplatePerson
                {
                    MasterId = masterId,
                    PersonId = id,
                    InsertDate = updateDate,
                    InsertedBy = updatedBy,
                    IsDeleted = false
                };

                repository.Insert(item);
            }
        }
    }
}
