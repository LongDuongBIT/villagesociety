﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class TemplateMasterService : BaseService<TemplateMaster>, ITemplateMasterService
    {
        public TemplateMasterService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public TemplateMasterService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<TemplateMasterEx> GetEx(Expression<Func<TemplateMaster, bool>> filter = null)
        {
            filter = filter ?? (p => true);

            var items = repository.Get();
            var persons = uow.Repository<AccountPerson>().Get();
            var addresses = uow.Repository<Address>().Get();
            var districts = uow.Repository<AddressDistrict>().Get();
            var cities = uow.Repository<AddressCity>().Get();
            var countries = uow.Repository<AddressCountry>().Get();

            var query = from item in items.Where(filter)
                        join comboBoxPerson in persons on item.ComboBoxPersonId equals comboBoxPerson.Id
                        join gridLookupPerson in persons on item.GridLookupPersonId equals gridLookupPerson.Id

                        join comboBoxAddress in addresses on item.ComboBoxAddressId equals comboBoxAddress.Id
                        join comboBoxDistrict in districts on comboBoxAddress.DistrictId equals comboBoxDistrict.Id
                        join comboBoxCity in cities on comboBoxDistrict.CityId equals comboBoxCity.Id
                        join comboBoxCountry in countries on comboBoxCity.CountryId equals comboBoxCountry.Id

                        join gridLookupAddress in addresses on item.GridLookupAddressId equals gridLookupAddress.Id
                        join gridLookupDistrict in districts on gridLookupAddress.DistrictId equals gridLookupDistrict.Id
                        join gridLookupCity in cities on gridLookupDistrict.CityId equals gridLookupCity.Id
                        join gridLookupCountry in countries on gridLookupCity.CountryId equals gridLookupCountry.Id
                        
                        select new TemplateMasterEx
                        {
                            Id = item.Id,
                            ComboBoxPersonId = item.ComboBoxPersonId,
                            GridLookupPersonId = item.GridLookupPersonId,
                            ComboBoxAddressId = item.ComboBoxAddressId,
                            GridLookupAddressId = item.GridLookupAddressId,
                            BitColumn1 = item.BitColumn1,
                            BitColumn2 = item.BitColumn2,
                            IntColumn = item.IntColumn,
                            FloatColumn = item.FloatColumn,
                            DecimalColumn = item.DecimalColumn,
                            MoneyColumn = item.MoneyColumn,
                            PercentColumn = item.PercentColumn,
                            DateColumn = item.DateColumn,
                            DateTimeColumn = item.DateTimeColumn,
                            StringColumn = item.StringColumn,
                            MultilineColumn = item.MultilineColumn,
                            HtmlColumn = item.HtmlColumn,
                            PhoneNumberColumn = item.PhoneNumberColumn,
                            CreditCardNumberColumn = item.CreditCardNumberColumn,
                            LatitudeColumn = item.LatitudeColumn,
                            LongitudeColumn = item.LongitudeColumn,
                            FilePathColumn = item.FilePathColumn,
                            ImagePathColumn = item.ImagePathColumn,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,

                            ComboBoxFirstName = comboBoxPerson.FirstName,
                            ComboBoxLastName = comboBoxPerson.LastName,

                            GridLookupFirstName = gridLookupPerson.FirstName,
                            GridLookupLastName = gridLookupPerson.LastName,

                            ComboBoxCountryId = comboBoxCity.CountryId,
                            ComboBoxCountryName = comboBoxCountry.CountryName,
                            ComboBoxCityId = comboBoxDistrict.CityId,
                            ComboBoxCityName = comboBoxCity.CityName,
                            ComboBoxDistrictId = comboBoxAddress.DistrictId,
                            ComboBoxDistrictName = comboBoxDistrict.DistrictName,
                            ComboBoxFullAddress = comboBoxAddress.FullAddress,
                            ComboBoxPostalCode = comboBoxAddress.PostalCode,
                            ComboBoxLatitude = comboBoxAddress.Latitude,
                            ComboBoxLongitude = comboBoxAddress.Longitude,

                            GridLookupCountryId = gridLookupCity.CountryId,
                            GridLookupCountryName = gridLookupCountry.CountryName,
                            GridLookupCityId = gridLookupDistrict.CityId,
                            GridLookupCityName = gridLookupCity.CityName,
                            GridLookupDistrictId = gridLookupAddress.DistrictId,
                            GridLookupDistrictName = gridLookupDistrict.DistrictName,
                            GridLookupFullAddress = gridLookupAddress.FullAddress,
                            GridLookupPostalCode = gridLookupAddress.PostalCode,
                            GridLookupLatitude = gridLookupAddress.Latitude,
                            GridLookupLongitude = gridLookupAddress.Longitude,
                        };

            return query;
        }

        public virtual void Insert(TemplateMaster item, int[] templatePersons, int[] templateUsers)
        {
            var templatePersonRepository = uow.Repository<TemplatePerson>();

            foreach (int id in templatePersons)
            {
                var templatePerson = new TemplatePerson
                {
                    MasterId = item.Id,
                    PersonId = id,
                    InsertDate = item.InsertDate,
                    InsertedBy = item.InsertedBy,
                    IsDeleted = false
                };

                templatePersonRepository.Insert(templatePerson);
            }

            var templateUserRepository = uow.Repository<TemplateUser>();

            foreach (int id in templateUsers)
            {
                var templateUser = new TemplateUser
                {
                    MasterId = item.Id,
                    UserId = id,
                    InsertDate = item.InsertDate,
                    InsertedBy = item.InsertedBy,
                    IsDeleted = false
                };

                templateUserRepository.Insert(templateUser);
            }

            repository.Insert(item);
        }

        public virtual void Update(TemplateMaster item, int[] templatePersons, int[] templateUsers)
        {
            var personService = new TemplatePersonService(uow);
            personService.Edit(item.Id, templatePersons, item.UpdateDate.Value, item.UpdatedBy.Value);

            var userService = new TemplateUserService(uow);
            userService.Edit(item.Id, templateUsers, item.UpdateDate.Value, item.UpdatedBy.Value);

            Update(item);
        }
    }
}
