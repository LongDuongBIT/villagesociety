﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class TemplateDetail1Service : BaseService<TemplateDetail1>, ITemplateDetail1Service
    {
        public TemplateDetail1Service()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public TemplateDetail1Service(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<TemplateDetail1Ex> GetEx(Expression<Func<TemplateDetail1, bool>> filter = null)
        {
            filter = filter ?? (p => true);

            var items = repository.Get();
            var masters = uow.Repository<TemplateMaster>().Get();

            var query = from item in items.Where(filter)
                        join master in masters on item.MasterId equals master.Id

                        select new TemplateDetail1Ex
                        {
                            Id = item.Id,
                            Detail1Name = item.Detail1Name,
                            Detail1Description = item.Detail1Description,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,

                            StringColumn = master.StringColumn,
                            MultilineColumn = master.MultilineColumn,
                            HtmlColumn = master.HtmlColumn,
                            PhoneNumberColumn = master.PhoneNumberColumn,
                            CreditCardNumberColumn = master.CreditCardNumberColumn,
                            LatitudeColumn = master.LatitudeColumn,
                            LongitudeColumn = master.LongitudeColumn
                        };

            return query;
        }

    }
}
