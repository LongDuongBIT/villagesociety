﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class TemplateUserService : BaseService<TemplateUser>, ITemplateUserService
    {
        public TemplateUserService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public TemplateUserService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<TemplateUserEx> GetEx(Expression<Func<TemplateUser, bool>> filter = null,
            Expression<Func<AccountUser, bool>> filterUser = null)
        {
            filter = filter ?? (p => true);
            filterUser = filterUser ?? (p => true);

            var items = repository.Get();
            var masters = uow.Repository<TemplateMaster>().Get();
            var users = uow.Repository<AccountUser>().Get();

            var query = from item in items.Where(filter)
                        join master in masters on item.MasterId equals master.Id
                        join user in users.Where(filterUser) on item.UserId equals user.Id

                        select new TemplateUserEx
                        {
                            Id = item.Id,
                            MasterId = item.MasterId,
                            UserId = item.UserId,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,

                            StringColumn = master.StringColumn,
                            MultilineColumn = master.MultilineColumn,
                            HtmlColumn = master.HtmlColumn,
                            PhoneNumberColumn = master.PhoneNumberColumn,
                            CreditCardNumberColumn = master.CreditCardNumberColumn,
                            LatitudeColumn = master.LatitudeColumn,
                            LongitudeColumn = master.LongitudeColumn,

                            Username = user.Username,
                            UserEMail = user.UserEMail
                        };

            return query;
        }

        public virtual void Edit(int masterId, int[] ids, DateTime updateDate, int updatedBy)
        {
            if (ids == null) ids = new int[] { };

            var repository = uow.Repository<TemplateUser>();
            var items = repository.Get(p => p.MasterId == masterId);

            foreach (var item in items.Where(p => !p.IsDeleted && !ids.Contains(p.UserId)))
            {
                item.DeleteDate = updateDate;
                item.DeletedBy = updatedBy;
                item.IsDeleted = true;

                repository.Delete(item);
            }

            foreach (var item in items.Where(p => p.IsDeleted && ids.Contains(p.UserId)))
            {
                item.UpdateDate = updateDate;
                item.UpdatedBy = updatedBy;
                item.IsDeleted = false;

                repository.Restore(item, false);
            }

            foreach (var id in ids.Where(p => !items.Any(tp => tp.UserId == p)))
            {
                var item = new TemplateUser
                {
                    MasterId = masterId,
                    UserId = id,
                    InsertDate = updateDate,
                    InsertedBy = updatedBy,
                    IsDeleted = false
                };

                repository.Insert(item);
            }
        }

    }
}
