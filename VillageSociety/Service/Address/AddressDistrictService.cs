﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AddressDistrictService : RowNumberedService<AddressDistrict>, IAddressDistrictService
    {
        public AddressDistrictService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AddressDistrictService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<AddressDistrictEx> GetEx(Expression<Func<AddressDistrict, bool>> filter = null,
            Expression<Func<AddressCountry, bool>> filterCountry = null,
            Expression<Func<AddressCity, bool>> filterCity = null)
        {
            filter = filter ?? (p => true);
            filterCountry = filterCountry ?? (p => true);
            filterCity = filterCity ?? (p => true);

            var items = repository.Get();
            var countries = uow.Repository<AddressCountry>().Get();
            var cities = uow.Repository<AddressCity>().Get();

            var query = from item in repository.Get(filter)
                        join city in cities.Where(filterCity) on item.CityId equals city.Id
                        join country in countries.Where(filterCountry) on city.CountryId equals country.Id
                        select new AddressDistrictEx
                        {
                            Id = item.Id,
                            CityId = item.CityId,
                            DistrictName = item.DistrictName,
                            DistrictDescription = item.DistrictDescription,
                            RowNumber = item.RowNumber,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,

                            CountryId = country.Id,
                            CountryCode = country.CountryCode,
                            CountryName = country.CountryName,

                            CityName = city.CityName
                        };
            return query;

        }
    }
}
