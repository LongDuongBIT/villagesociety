﻿using AutoMapper;
using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AddressService : RowNumberedService<Address>, IAddressService
    {
        public AddressService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AddressService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<AddressEx> GetEx(Expression<Func<Address, bool>> filter = null,
            Expression<Func<AddressCountry, bool>> filterCountry = null,
            Expression<Func<AddressCity, bool>> filterCity = null,
            Expression<Func<AddressDistrict, bool>> filterDistrict = null)
        {
            filter = filter ?? (p => true);
            filterCountry = filterCountry ?? (p => true);
            filterCity = filterCity ?? (p => true);
            filterDistrict = filterDistrict ?? (p => true);

            var items = repository.Get();
            var countries = uow.Repository<AddressCountry>().Get();
            var cities = uow.Repository<AddressCity>().Get();
            var districts = uow.Repository<AddressDistrict>().Get();

            var query = from item in repository.Get(filter)
                        join district in districts.Where(filterDistrict) on item.DistrictId equals district.Id
                        join city in cities.Where(filterCity) on district.CityId equals city.Id
                        join country in countries.Where(filterCountry) on city.CountryId equals country.Id
                        select new AddressEx
                        {
                            Id = item.Id,
                            DistrictId = item.DistrictId,
                            FullAddress = item.FullAddress,
                            FullAddressDescription = item.FullAddressDescription,
                            PostalCode = item.PostalCode,
                            Latitude = item.Latitude,
                            Longitude = item.Longitude,
                            RowNumber = item.RowNumber,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,

                            CountryId = country.Id,
                            CountryCode = country.CountryCode,
                            CountryName = country.CountryName,

                            CityId = city.Id,
                            CityName = city.CityName,

                            DistrictName = district.DistrictName
                        };
            return query;

        }

        public AddressSetStruct InsertOrUpdate(AddressCountry country, AddressCity city, AddressDistrict district, Address address)
        {
            var countryRepository = uow.Repository<AddressCountry>();
            var countryDb = countryRepository.Get(p => p.CountryCode == country.CountryCode).SingleOrDefault();

            var cityRepository = uow.Repository<AddressCity>();
            var cityDb = cityRepository.Get(p => p.CityName == city.CityName).SingleOrDefault();

            var districtRepository = uow.Repository<AddressDistrict>();
            var districtDb = districtRepository.Get(p => p.DistrictName == district.DistrictName).SingleOrDefault();

            var addressRepository = uow.Repository<Address>();
            var addressDb = addressRepository.Get(p => p.FullAddress == address.FullAddress).SingleOrDefault();

            if (countryDb == null)
            {
                countryRepository.Insert(country);
            }
            else
            {
                AutoMapper.Mapper.CreateMap<AddressCountry, AddressCountry>()
                    .ForMember(t => t.UpdateDate, opt => opt.MapFrom(src => src.InsertDate))
                    .ForMember(t => t.UpdatedBy, opt => opt.MapFrom(src => src.InsertedBy))
                    .ForMember(t => t.Id, opt => opt.Ignore())
                    .ForMember(t => t.InsertDate, opt => opt.Ignore())
                    .ForMember(t => t.InsertedBy, opt => opt.Ignore())
                    .ForMember(t => t.DeleteDate, opt => opt.Ignore())
                    .ForMember(t => t.DeletedBy, opt => opt.Ignore())
                    .ForMember(t => t.IsDeleted, opt => opt.Ignore());

                AutoMapper.Mapper.Map<AddressCountry, AddressCountry>(country, countryDb);
                countryRepository.Update(countryDb);
            }

            if (cityDb == null)
            {
                city.CountryId = countryDb == null ? country.Id : countryDb.Id;
                cityRepository.Insert(city);
            }
            else
            {
                AutoMapper.Mapper.CreateMap<AddressCity, AddressCity>()
                    .ForMember(t => t.UpdateDate, opt => opt.MapFrom(src => src.InsertDate))
                    .ForMember(t => t.UpdatedBy, opt => opt.MapFrom(src => src.InsertedBy))
                    .ForMember(t => t.Id, opt => opt.Ignore())
                    .ForMember(t => t.CountryId, opt => opt.Ignore())
                    .ForMember(t => t.InsertDate, opt => opt.Ignore())
                    .ForMember(t => t.InsertedBy, opt => opt.Ignore())
                    .ForMember(t => t.DeleteDate, opt => opt.Ignore())
                    .ForMember(t => t.DeletedBy, opt => opt.Ignore())
                    .ForMember(t => t.IsDeleted, opt => opt.Ignore());

                AutoMapper.Mapper.Map<AddressCity, AddressCity>(city, cityDb);
                cityRepository.Update(cityDb);
            }

            if (districtDb == null)
            {
                district.CityId = cityDb == null ? city.Id : cityDb.Id;
                districtRepository.Insert(district);
            }
            else
            {
                AutoMapper.Mapper.CreateMap<AddressDistrict, AddressDistrict>()
                    .ForMember(t => t.UpdateDate, opt => opt.MapFrom(src => src.InsertDate))
                    .ForMember(t => t.UpdatedBy, opt => opt.MapFrom(src => src.InsertedBy))
                    .ForMember(t => t.Id, opt => opt.Ignore())
                    .ForMember(t => t.CityId, opt => opt.Ignore())
                    .ForMember(t => t.InsertDate, opt => opt.Ignore())
                    .ForMember(t => t.InsertedBy, opt => opt.Ignore())
                    .ForMember(t => t.DeleteDate, opt => opt.Ignore())
                    .ForMember(t => t.DeletedBy, opt => opt.Ignore())
                    .ForMember(t => t.IsDeleted, opt => opt.Ignore());

                AutoMapper.Mapper.Map<AddressDistrict, AddressDistrict>(district, districtDb);
                districtRepository.Update(districtDb);
            }

            if (addressDb == null)
            {
                address.DistrictId = districtDb == null ? district.Id : districtDb.Id;
                addressRepository.Insert(address);
            }
            else
            {
                AutoMapper.Mapper.CreateMap<Address, Address>()
                    .ForMember(t => t.UpdateDate, opt => opt.MapFrom(src => src.InsertDate))
                    .ForMember(t => t.UpdatedBy, opt => opt.MapFrom(src => src.InsertedBy))
                    .ForMember(t => t.Id, opt => opt.Ignore())
                    .ForMember(t => t.DistrictId, opt => opt.Ignore())
                    .ForMember(t => t.InsertDate, opt => opt.Ignore())
                    .ForMember(t => t.InsertedBy, opt => opt.Ignore())
                    .ForMember(t => t.DeleteDate, opt => opt.Ignore())
                    .ForMember(t => t.DeletedBy, opt => opt.Ignore())
                    .ForMember(t => t.IsDeleted, opt => opt.Ignore());

                AutoMapper.Mapper.Map<Address, Address>(address, addressDb);
                addressRepository.Update(addressDb);
            }

            return new AddressSetStruct
            {
                Country = countryDb ?? country,
                City = cityDb ?? city,
                District = districtDb ?? district,
                Address = addressDb ?? address
            };
        }
    }
}
