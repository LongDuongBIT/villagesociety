﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AddressCityService : RowNumberedService<AddressCity>, IAddressCityService
    {
        public AddressCityService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AddressCityService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<AddressCityEx> GetEx(Expression<Func<AddressCity, bool>> filter = null,
            Expression<Func<AddressCountry, bool>> filterCountry = null)
        {
            filter = filter ?? (p => true);
            filterCountry = filterCountry ?? (p => true);

            var items = repository.Get();
            var countries = uow.Repository<AddressCountry>().Get();

            var query = from item in items.Where(filter)
                        join country in countries.Where(filterCountry) on item.CountryId equals country.Id
                        select new AddressCityEx
                        {
                            Id = item.Id,
                            CountryId = item.CountryId,
                            CityName = item.CityName,
                            CityDescription = item.CityDescription,
                            RowNumber = item.RowNumber,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,

                            CountryCode = country.CountryCode,
                            CountryName = country.CountryName
                        };
            return query;

        }
    }
}
