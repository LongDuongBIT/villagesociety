﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AddressCountryService : RowNumberedService<AddressCountry>, IAddressCountryService
    {
        public AddressCountryService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AddressCountryService(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
