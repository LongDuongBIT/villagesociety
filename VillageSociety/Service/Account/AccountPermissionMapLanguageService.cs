﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountPermissionMapLanguageService : BaseService<AccountPermissionMapLanguage>, IAccountPermissionMapLanguageService
    {
        public AccountPermissionMapLanguageService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountPermissionMapLanguageService(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
