﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountUserService : BaseService<AccountUser>, IAccountUserService
    {
        public AccountUserService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountUserService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<AccountUserEx> GetEx(Expression<Func<AccountUser, bool>> filter = null,
            Expression<Func<AccountPerson, bool>> filterPerson = null,
            Expression<Func<SystemVillage, bool>> filterVillage = null,
            Expression<Func<AccountRole, bool>> filterRole = null)
        {
            filter = filter ?? (p => true);
            filterPerson = filterPerson ?? (p => true);
            filterVillage = filterVillage ?? (p => true);
            filterRole = filterRole ?? (p => true);

            var items = repository.Get();
            var persons = uow.Repository<AccountPerson>().Get();
            var roles = uow.Repository<AccountRole>().Get();
            var themes = uow.Repository<AccountTheme>().Get();
            var languages = uow.Repository<AccountLanguage>().Get();
            var villages = uow.Repository<SystemVillage>().Get();

            var query = from item in items.Where(filter)
                        join person in persons.Where(filterPerson) on item.PersonId equals person.Id
                        join role in roles.Where(filterRole) on item.RoleId equals role.Id
                        join theme in themes on item.ThemeId equals theme.Id
                        join language in languages on item.LanguageId equals language.Id
                        join village in villages.Where(filterVillage) on person.VillageId equals village.Id
                        select new AccountUserEx
                        {
                            Id = item.Id,
                            PersonId = item.PersonId,
                            RoleId = item.RoleId,
                            Username = item.Username,
                            UserEMail = item.UserEMail,
                            Password = item.Password,
                            ThemeId = item.ThemeId,
                            LanguageId = item.LanguageId,
                            IsSystemAdmin = item.IsSystemAdmin,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,

                            VillageId = person.VillageId,
                            VillageName = village.VillageName,

                            FirstName = person.FirstName,
                            LastName = person.LastName,
                            IsMale = person.IsMale,
                            PicturePath = person.PicturePath,

                            RoleName = role.RoleName,

                            ThemeName = theme.ThemeName,

                            LanguageCode = language.LanguageCode,
                            LanguageName = language.LanguageName
                        };
            return query;

        }

        public void SaveProfile(AccountPerson person, AccountUser user)
        {
            var personService = new AccountPersonService(uow);
            personService.Edit(person, "PicturePath", "UpdateDate", "UpdatedBy");
            Edit(user, "Password", "ThemeId", "LanguageId", "UpdateDate", "UpdatedBy");
        }

        public IQueryable<int?> GetPermissions(int userId)
        {
            var userPermissions = uow.Repository<AccountUserPermission>().Get();
            var users = uow.Repository<AccountUser>().Get();
            var rolePermissions = uow.Repository<AccountRolePermission>().Get();

            var query1 = from userPermission in userPermissions.Where(p => !p.IsDeleted)
                        join user in users on userPermission.UserId equals user.Id
                        where
                            userPermission.UserId == userId
                            && !rolePermissions.Any(p => !p.IsDeleted && p.RoleId == user.RoleId && p.PermissionId == userPermission.PermissionId)
                        select (int?)userPermission.PermissionId;

            var query2 = from rolePermission in rolePermissions.Where(p => !p.IsDeleted)
                         join user in users on rolePermission.RoleId equals user.RoleId
                         where
                             user.Id == userId
                             && !userPermissions.Any(p => !p.IsDeleted && p.UserId == userId && p.PermissionId == rolePermission.PermissionId)
                         select (int?)rolePermission.PermissionId;

            return query1.Union(query2);
        }
    }
}
