﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountThemeService : RowNumberedService<AccountTheme>, IAccountThemeService
    {
        public AccountThemeService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountThemeService(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
