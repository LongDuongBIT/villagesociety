﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountRoleService : RowNumberedService<AccountRole>, IAccountRoleService
    {
        public AccountRoleService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountRoleService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public virtual IQueryable<AccountRoleEx> GetWithLanguage(Expression<Func<AccountRole, bool>> filter, int languageId)
        {
            filter = filter ?? (p => true);

            var items = repository.Get();
            var roleLanguages = uow.Repository<AccountRoleLanguage>().Get();

            return from item in items.Where(filter)
                   from roleLanguage in roleLanguages.Where(p => p.RoleId == item.Id && p.LanguageId == languageId).DefaultIfEmpty()
                   select new AccountRoleEx
                   {
                       Id = item.Id,
                       RoleName = roleLanguage.RoleName ?? item.RoleName,
                       RoleDescription = roleLanguage.RoleDescription ?? item.RoleDescription,
                       RowNumber = item.RowNumber,
                       InsertDate = item.InsertDate,
                       InsertedBy = item.InsertedBy,
                       UpdateDate = item.UpdateDate,
                       UpdatedBy = item.UpdatedBy,
                       DeleteDate = item.DeleteDate,
                       DeletedBy = item.DeletedBy,
                       IsDeleted = item.IsDeleted
                   };
                   
        }
    }
}
