﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountUserPermissionService : BaseService<AccountUserPermission>, IAccountUserPermissionService
    {
        public AccountUserPermissionService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountUserPermissionService(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
