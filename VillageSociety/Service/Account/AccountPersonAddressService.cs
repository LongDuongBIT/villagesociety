﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountPersonAddressService : BaseService<AccountPersonAddress>, IAccountPersonAddressService
    {
        public AccountPersonAddressService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountPersonAddressService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<AccountPersonAddressEx> GetEx(Expression<Func<AccountPersonAddress, bool>> filter = null,
            Expression<Func<AccountPerson, bool>> filterPerson = null,
            Expression<Func<Address, bool>> filterAddress = null,
            Expression<Func<AddressDistrict, bool>> filterDistrict = null,
            Expression<Func<AddressCity, bool>> filterCity = null,
            Expression<Func<AddressCountry, bool>> filterCountry = null,
            Expression<Func<AddressType, bool>> filterAddressType = null)
        {
            filter = filter ?? (p => true);
            filterPerson = filterPerson ?? (p => true);
            filterAddress = filterAddress ?? (p => true);
            filterDistrict = filterDistrict ?? (p => true);
            filterCity = filterCity ?? (p => true);
            filterCountry = filterCountry ?? (p => true);
            filterAddressType = filterAddressType ?? (p => true);

            var items = repository.Get();
            var persons = uow.Repository<AccountPerson>().Get();
            var addresses = uow.Repository<Address>().Get();
            var districts = uow.Repository<AddressDistrict>().Get();
            var cities = uow.Repository<AddressCity>().Get();
            var countries = uow.Repository<AddressCountry>().Get();
            var addressTypes = uow.Repository<AddressType>().Get();

            var query = from item in items.Where(filter)
                        join person in persons.Where(filterPerson) on item.PersonId equals person.Id
                        join address in addresses.Where(filterAddress) on item.AddressId equals address.Id
                        join district in districts.Where(filterDistrict) on address.DistrictId equals district.Id
                        join city in cities on district.CityId equals city.Id
                        join country in countries.Where(filterCountry) on city.CountryId equals country.Id
                        join addressType in addressTypes.Where(filterAddressType) on item.AddressTypeId equals addressType.Id
                        select new AccountPersonAddressEx
                        {
                            Id = item.Id,
                            PersonId = item.PersonId,
                            AddressId = item.AddressId,
                            AddressTypeId = item.AddressTypeId,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,
                            FirstName = person.FirstName,
                            LastName = person.LastName,
                            CountryId = country.Id,
                            CountryCode = country.CountryCode,
                            CountryName = country.CountryName,
                            CityId = city.Id,
                            CityName = city.CityName,
                            DistrictId = district.Id,
                            DistrictName = district.DistrictName,
                            FullAddress = address.FullAddress,
                            FullAddressDescription = address.FullAddressDescription,
                            PostalCode = address.PostalCode,
                            Latitude = address.Latitude,
                            Longitude = address.Longitude,
                            AddressTypeName = addressType.AddressTypeName
                        };
            return query;

        }
    }
}
