﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountPermissionMapService : BaseService<AccountPermissionMap>, IAccountPermissionMapService
    {
        public AccountPermissionMapService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountPermissionMapService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<AccountPermissionMapWithRole> GetWithRole(int? roleId, int languageId)
        {
            var items = repository.Get();
            var permissionMapLanguages = uow.Repository<AccountPermissionMapLanguage>().Get();
            var roles = uow.Repository<AccountRole>().Get();
            var permissions = uow.Repository<AccountPermission>().Get();
            var rolePermissions = uow.Repository<AccountRolePermission>().Get();

            var query = from permissionMap in items
                        from permissionMapLanguage in permissionMapLanguages.Where(p => p.PermissionMapId == permissionMap.Id && p.LanguageId == languageId).DefaultIfEmpty()
                        from permission in permissions.Where(p => !p.IsDeleted && p.Id == permissionMap.PermissionId).DefaultIfEmpty()
                        join role in roles.Where(p => !p.IsDeleted) on 1 equals 1
                        from rolePermission in rolePermissions.Where(p => p.RoleId == role.Id && p.PermissionId == permissionMap.PermissionId).DefaultIfEmpty()
                        where !roleId.HasValue || role.Id == roleId
                        orderby role.RowNumber, permissionMap.Id
                        select new AccountPermissionMapWithRole
                        {
                            Id = roleId.HasValue ? permissionMap.Id : permissionMap.Id + role.Id * 100000,
                            ParentId = roleId.HasValue ? permissionMap.ParentId : (permissionMap.ParentId != null ? permissionMap.ParentId + role.Id * 100000 : role.Id),
                            PermissionId = permissionMap.PermissionId,
                            PermissionMapName = permissionMapLanguage.PermissionMapName ?? permissionMap.PermissionMapName,
                            PermissionMapDescription = permissionMapLanguage.PermissionMapDescription ?? permissionMap.PermissionMapDescription,
                            InsertDate = permissionMap.InsertDate,
                            InsertedBy = permissionMap.InsertedBy,
                            UpdateDate = permissionMap.UpdateDate,
                            UpdatedBy = permissionMap.UpdatedBy,
                            DeleteDate = permissionMap.DeleteDate,
                            DeletedBy = permissionMap.DeletedBy,
                            IsDeleted = permissionMap.IsDeleted,
                            PermissionName = permission.PermissionName,
                            RoleId = role.Id,
                            RoleName = role.RoleName,
                            RolePermissionId = rolePermission.Id,
                            InRolePermission = rolePermission != null && !rolePermission.IsDeleted
                        };

            if (roleId.HasValue)
            {
                return query;
            }

            var roleLanguages = uow.Repository<AccountRoleLanguage>().Get();

            var roleQuery = from role in roles
                            from roleLanguage in roleLanguages.Where(p => p.RoleId == role.Id && p.LanguageId == languageId).DefaultIfEmpty()
                            where !role.IsDeleted
                            select new AccountPermissionMapWithRole
                            {
                                Id = role.Id,
                                ParentId = null,
                                PermissionId = null,
                                PermissionMapName = roleLanguage.RoleName ?? role.RoleName,
                                PermissionMapDescription = null,
                                InsertDate = role.InsertDate,
                                InsertedBy = role.InsertedBy,
                                UpdateDate = null,
                                UpdatedBy = null,
                                DeleteDate = null,
                                DeletedBy = null,
                                IsDeleted = false,
                                PermissionName = null,
                                RoleId = role.Id,
                                RoleName = role.RoleName,
                                RolePermissionId = null,
                                InRolePermission = false
                            };

            return roleQuery.Union(query);
        }

        public IQueryable<AccountPermissionMapWithUser> GetWithUser(int? userId, int languageId)
        {
            var items = repository.Get();
            var users = uow.Repository<AccountUser>().Get();
            var persons = uow.Repository<AccountPerson>().Get();
            var permissions = uow.Repository<AccountPermission>().Get();
            var userPermissions = uow.Repository<AccountUserPermission>().Get();
            var rolePermissions = uow.Repository<AccountRolePermission>().Get();

            var query = from permissionMap in items
                        from permission in permissions.Where(p => !p.IsDeleted && p.Id == permissionMap.PermissionId).DefaultIfEmpty()
                        join user in users.Where(p => !p.IsDeleted) on 1 equals 1
                        join person in persons on user.PersonId equals person.Id
                        from userPermission in userPermissions.Where(p => p.UserId == user.Id && p.PermissionId == permissionMap.PermissionId).DefaultIfEmpty()
                        from rolePermission in rolePermissions.Where(p => p.RoleId == user.RoleId && p.PermissionId == permissionMap.PermissionId).DefaultIfEmpty()
                        where !userId.HasValue || user.Id == userId
                        orderby permissionMap.Id
                        select new AccountPermissionMapWithUser
                        {
                            Id = userId.HasValue ? permissionMap.Id : permissionMap.Id + user.Id * 100000,
                            ParentId = userId.HasValue ? permissionMap.ParentId : (permissionMap.ParentId != null ? permissionMap.ParentId + user.Id * 100000 : user.Id),
                            PermissionId = permissionMap.PermissionId,
                            PermissionMapName = permissionMap.PermissionMapName,
                            PermissionMapDescription = permissionMap.PermissionMapDescription,
                            InsertDate = permissionMap.InsertDate,
                            InsertedBy = permissionMap.InsertedBy,
                            UpdateDate = permissionMap.UpdateDate,
                            UpdatedBy = permissionMap.UpdatedBy,
                            DeleteDate = permissionMap.DeleteDate,
                            DeletedBy = permissionMap.DeletedBy,
                            IsDeleted = permissionMap.IsDeleted,
                            PermissionName = permission.PermissionName,
                            UserId = user.Id,
                            Username = user.Username,
                            UserEMail = user.UserEMail,
                            FirstName = person.FirstName,
                            LastName = person.LastName,
                            UserPermissionId = userPermission.Id,
                            IsDeletedUserPermission = userPermission.IsDeleted,
                            RolePermissionId = rolePermission.Id,
                            IsDeletedRolePermission = rolePermission.IsDeleted
                        };

            if (userId.HasValue)
            {
                return query;
            }

            var userQuery = from user in users
                            join person in persons on user.PersonId equals person.Id
                            where !user.IsDeleted
                            select new AccountPermissionMapWithUser
                            {
                                Id = user.Id,
                                ParentId = null,
                                PermissionId = null,
                                PermissionMapName = person.FirstName + " " + person.LastName + " (" + user.Username + ")",
                                PermissionMapDescription = null,
                                InsertDate = user.InsertDate,
                                InsertedBy = user.InsertedBy,
                                UpdateDate = null,
                                UpdatedBy = null,
                                DeleteDate = null,
                                DeletedBy = null,
                                IsDeleted = false,
                                PermissionName = null,
                                UserId = user.Id,
                                Username = user.Username,
                                UserEMail = user.UserEMail,
                                FirstName = person.FirstName,
                                LastName = person.LastName,
                                UserPermissionId = null,
                                IsDeletedUserPermission = null,
                                RolePermissionId = null,
                                IsDeletedRolePermission = null
                            };

            return userQuery.Union(query);
        }

    }
}
