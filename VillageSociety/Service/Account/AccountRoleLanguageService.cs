﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountRoleLanguageService : BaseService<AccountRoleLanguage>, IAccountRoleLanguageService
    {
        public AccountRoleLanguageService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountRoleLanguageService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<AccountRoleLanguageEx> GetEx(Expression<Func<AccountRoleLanguage, bool>> filter = null)
        {
            filter = filter ?? (p => true);

            var items = repository.Get();
            var roles = uow.Repository<AccountRole>().Get();
            var languages = uow.Repository<AccountLanguage>().Get();

            var query = from item in items.Where(filter)
                        join role in roles on item.RoleId equals role.Id
                        join language in languages on item.LanguageId equals language.Id
                        select new AccountRoleLanguageEx
                        {
                            Id = item.Id,
                            RoleId = item.RoleId,
                            LanguageId = item.LanguageId,
                            RoleName = item.RoleName,
                            RoleDescription = item.RoleDescription,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,
                            
                            DefaultRoleName = role.RoleName,
                            DefaultRoleDescription = role.RoleDescription,

                            LanguageCode = language.LanguageCode,
                            LanguageName = language.LanguageName,
                            IconPath = language.IconPath
                        };

            return query;
        }
    }
}
