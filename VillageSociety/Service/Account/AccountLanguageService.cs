﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountLanguageService : RowNumberedService<AccountLanguage>, IAccountLanguageService
    {
        public AccountLanguageService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountLanguageService(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
