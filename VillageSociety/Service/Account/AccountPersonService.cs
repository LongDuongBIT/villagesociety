﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountPersonService : BaseService<AccountPerson>, IAccountPersonService
    {
        public AccountPersonService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountPersonService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public IQueryable<AccountPersonEx> GetEx(Expression<Func<AccountPerson, bool>> filter = null,
            Expression<Func<SystemVillage, bool>> filterVillage = null,
            Expression<Func<AccountPerson, bool>> filterFather = null,
            Expression<Func<AccountPerson, bool>> filterMother = null)
        {
            filter = filter ?? (p => true);
            filterVillage = filterVillage ?? (p => true);
            filterFather = filterFather ?? (p => true);
            filterMother = filterMother ?? (p => true);

            var items = repository.Get();
            var villages = uow.Repository<SystemVillage>().Get();
            var persons = uow.Repository<AccountPerson>().Get();

            var query = from item in items.Where(filter)
                        join village in villages.Where(filterVillage) on item.VillageId equals village.Id
                        from father in persons.Where(p => p.Id == item.Father).DefaultIfEmpty().Where(filterFather)
                        from mother in persons.Where(p => p.Id == item.Mother).DefaultIfEmpty().Where(filterMother)
                        select new AccountPersonEx
                        {
                            Id = item.Id,
                            VillageId = item.VillageId,
                            FirstName = item.FirstName,
                            LastName = item.LastName,
                            IsMale = item.IsMale,
                            PicturePath = item.PicturePath,
                            Father = item.Father,
                            Mother = item.Mother,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,
                            
                            VillageName = village.VillageName,
                            
                            FatherFirstName = father.FirstName,
                            FatherLastName = father.LastName,
                            
                            MotherFirstName = mother.FirstName,
                            MotherLastName = mother.LastName
                        };
            return query;

        }
    }
}
