﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountPermissionService : BaseService<AccountPermission>, IAccountPermissionService
    {
        public AccountPermissionService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountPermissionService(IUnitOfWork uow)
            : base(uow)
        {
        }
    }
}
