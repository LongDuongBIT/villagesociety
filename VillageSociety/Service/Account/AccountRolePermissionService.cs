﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AccountRolePermissionService : BaseService<AccountRolePermission>, IAccountRolePermissionService
    {
        public AccountRolePermissionService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public AccountRolePermissionService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public override void Insert(AccountRolePermission item)
        {
            base.Insert(item);

            var users = uow.Repository<AccountUser>().Get(p => p.RoleId == item.RoleId);
            var userPermissionRepository = uow.Repository<AccountUserPermission>();
            var userPermissions = userPermissionRepository.Get(p => !p.IsDeleted && p.PermissionId == item.PermissionId && users.Any(u => u.Id == p.UserId));

            foreach (var userPermission in userPermissions)
            {
                userPermission.DeleteDate = item.InsertDate;
                userPermission.DeletedBy = item.InsertedBy;
                userPermission.IsDeleted = true;

                userPermissionRepository.Delete(userPermission);
            }
        }
        public override void Delete(AccountRolePermission item)
        {
            base.Delete(item);

            var users = uow.Repository<AccountUser>().Get(p => p.RoleId == item.RoleId);
            var userPermissionRepository = uow.Repository<AccountUserPermission>();
            var userPermissions = userPermissionRepository.Get(p => !p.IsDeleted && p.PermissionId == item.PermissionId && users.Any(u => u.Id == p.UserId));

            foreach (var userPermission in userPermissions)
            {
                userPermission.DeleteDate = item.DeleteDate;
                userPermission.DeletedBy = item.DeletedBy;
                userPermission.IsDeleted = true;

                userPermissionRepository.Delete(userPermission);
            }
        }
        public override void Restore(AccountRolePermission item, bool ignoreProperties)
        {
            base.Restore(item, ignoreProperties);

            var users = uow.Repository<AccountUser>().Get(p => p.RoleId == item.RoleId);
            var userPermissionRepository = uow.Repository<AccountUserPermission>();
            var userPermissions = userPermissionRepository.Get(p => !p.IsDeleted && p.PermissionId == item.PermissionId && users.Any(u => u.Id == p.UserId));

            foreach (var userPermission in userPermissions)
            {
                userPermission.DeleteDate = item.UpdateDate;
                userPermission.DeletedBy = item.UpdatedBy;
                userPermission.IsDeleted = true;

                userPermissionRepository.Delete(userPermission);
            }
        }
    }
}
