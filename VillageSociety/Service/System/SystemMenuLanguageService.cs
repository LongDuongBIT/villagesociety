﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class SystemMenuLanguageService : BaseService<SystemMenuLanguage>, ISystemMenuLanguageService
    {
        public SystemMenuLanguageService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public SystemMenuLanguageService(IUnitOfWork uow)
            : base(uow)
        {
        }

    }
}
