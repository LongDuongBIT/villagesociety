﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class SystemVillageService : BaseService<SystemVillage>, ISystemVillageService
    {
        public SystemVillageService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public SystemVillageService(IUnitOfWork uow)
            : base(uow)
        {
        }

    }
}
