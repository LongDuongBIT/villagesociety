﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class SystemLayoutService : BaseService<SystemLayout>, ISystemLayoutService
    {
        public SystemLayoutService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public SystemLayoutService(IUnitOfWork uow)
            : base(uow)
        {
        }

    }
}
