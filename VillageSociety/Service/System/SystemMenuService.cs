﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class SystemMenuService : RowNumberedService<SystemMenu>, ISystemMenuService
    {
        public SystemMenuService()
            : base(IoC.Resolve<IUnitOfWork>())
        {
        }
        public SystemMenuService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public List<SystemMenuEx> ListOfLeftTree(Expression<Func<SystemMenu, bool>> filter, int userId)
        {
            var items = repository.Get();
            var users = uow.Repository<AccountUser>().Get();
            var menuLanguages = uow.Repository<SystemMenuLanguage>().Get();
            var permissions = new AccountUserService(uow).GetPermissions(userId);

            var list = (from item in items.Where(filter)
                        join user in users on userId equals user.Id
                        from menuLanguage in menuLanguages.Where(p => !p.IsDeleted && p.MenuId == item.Id && p.LanguageId == user.LanguageId).DefaultIfEmpty()
                        from permission in permissions.Where(p => p == item.PermissionId).DefaultIfEmpty()
                        select new SystemMenuEx
                        {
                            Id = item.Id,
                            ParentId = item.ParentId,
                            PermissionId = item.PermissionId,
                            MenuName = menuLanguage == null ? item.MenuName : menuLanguage.MenuName,
                            MenuDescription = menuLanguage == null || menuLanguage.MenuDescription == null ? item.MenuDescription : menuLanguage.MenuDescription,
                            NavigateUrl = permission != null ? item.NavigateUrl : null,
                            ControllerName = permission != null ? item.ControllerName : null,
                            ActionName = permission != null ? item.ActionName : null,
                            IconPath = item.IconPath,
                            RowNumber = item.RowNumber,
                            InsertDate = item.InsertDate,
                            InsertedBy = item.InsertedBy,
                            UpdateDate = item.UpdateDate,
                            UpdatedBy = item.UpdatedBy,
                            DeleteDate = item.DeleteDate,
                            DeletedBy = item.DeletedBy,
                            IsDeleted = item.IsDeleted,
                            IsLeaf = true,
                            Signed = permission != null && (item.NavigateUrl != null || (item.ControllerName != null && item.ActionName != null))
                        }).ToList();

            Func<SystemMenuEx, bool> funcSignParents = null;
            funcSignParents = (item) =>
            {
                if (item.FootprintOnTheLadderToParent)
                {
                    return false;
                }

                item.Signed = true;
                item.FootprintOnTheLadderToParent = true;

                if (!item.ParentId.HasValue)
                {
                    return true;
                }

                var parent = list.Single(p => p.Id == item.ParentId.Value);
                parent.IsLeaf = false;

                return funcSignParents(parent);
            };

            foreach (var item in list.Where(p => p.Signed))
            {
                funcSignParents(item);
            }

            var a = list.Where(p => p.Signed).ToList();

            return list.Where(p => p.Signed).ToList();
        }

    }
}
