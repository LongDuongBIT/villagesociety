﻿using Core;
using DalSqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class RowNumberedService<TEntity> : BaseService<TEntity>, IRowNumberedService<TEntity>, IDisposable where TEntity : RowNumberedEntity
    {
        public RowNumberedService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public bool Up(int id1, int? id2)
        {
            var rowNumberedRepository = (IRowNumberedRepository<TEntity>)repository;
            var entities = rowNumberedRepository.Get(p => p.Id == id1 || p.Id == id2).ToList();
            var rn = entities.First().RowNumber;
            var ups = rowNumberedRepository.Get(p => !p.IsDeleted && p.RowNumber < rn).OrderByDescending(p => p.RowNumber).Take(id2.HasValue ? 1 : 2).ToList();

            double rn1 = 0;
            double rn2 = 0;

            if (id2.HasValue)
            {
                rn1 = rn;
                rn2 = ups.Count() == 1 ? ups[0].RowNumber : rn1 - 2048;
            }
            else if (ups.Count() >= 2)
            {
                rn1 = ups[0].RowNumber;
                rn2 = ups[1].RowNumber;
            }
            else if (ups.Count() == 1)
            {
                rn1 = ups[0].RowNumber;
                rn2 = rn1 - 2048;
            }
            else
            {
                return false;
            }

            var entity = entities.Last();
            entity.RowNumber = (rn1 + rn2) / 2;

            rowNumberedRepository.Up(entity);

            return true;
        }
        public bool Down(int id1, int? id2)
        {
            var rowNumberedRepository = (IRowNumberedRepository<TEntity>)repository;
            var entities = rowNumberedRepository.Get(p => p.Id == id1 || p.Id == id2).ToList();
            var rn = entities.Last().RowNumber;
            var downs = rowNumberedRepository.Get(p => !p.IsDeleted && p.RowNumber > rn).Take(id2.HasValue ? 1 : 2).ToList();

            double rn1 = 0;
            double rn2 = 0;

            if (id2.HasValue)
            {
                rn1 = rn;
                rn2 = downs.Count() == 1 ? downs[0].RowNumber : rn1 + 2048;
            }
            else if (downs.Count() >= 2)
            {
                rn1 = downs[0].RowNumber;
                rn2 = downs[1].RowNumber;
            }
            else if (downs.Count() == 1)
            {
                rn1 = downs[0].RowNumber;
                rn2 = rn1 + 2048;
            }
            else
            {
                return false;
            }

            var entity = entities.First();
            entity.RowNumber = (rn1 + rn2) / 2;

            rowNumberedRepository.Down(entity);

            return true;
        }
    }
}
