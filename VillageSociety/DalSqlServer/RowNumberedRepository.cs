﻿using Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DalSqlServer
{
    public class RowNumberedRepository<TEntity> : Repository<TEntity>, IRowNumberedRepository<TEntity> where TEntity: RowNumberedEntity
    {
        public RowNumberedRepository()
            : base(new VillageSocietyEntities())
        {
        }
        public RowNumberedRepository(VillageSocietyEntities context)
            : base(context)
        {
        }

        public override void Insert(TEntity item)
        {
            item.RowNumber = this.Get().Select(p => p.RowNumber).DefaultIfEmpty(0).Max() + 1024;
            base.Insert(item);
        }

        public virtual void Up(TEntity item)
        {
            var entry = context.Entry(item);
            entry.State = EntityState.Modified;
            entry.IgnoreButSomeProperties("RowNumber");
        }
        public virtual void Down(TEntity item)
        {
            var entry = context.Entry(item);
            entry.State = EntityState.Modified;
            entry.IgnoreButSomeProperties("RowNumber");
        }
    }
}
