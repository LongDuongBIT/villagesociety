﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DalSqlServer
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class VillageSocietyEntities : DbContext
    {
        public VillageSocietyEntities()
            : base("name=VillageSocietyEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Core.AccountLanguage> AccountLanguage { get; set; }
        public virtual DbSet<Core.AccountPermission> AccountPermission { get; set; }
        public virtual DbSet<Core.AccountPermissionMap> AccountPermissionMap { get; set; }
        public virtual DbSet<Core.AccountPermissionMapLanguage> AccountPermissionMapLanguage { get; set; }
        public virtual DbSet<Core.AccountPerson> AccountPerson { get; set; }
        public virtual DbSet<Core.AccountPersonAddress> AccountPersonAddress { get; set; }
        public virtual DbSet<Core.AccountRole> AccountRole { get; set; }
        public virtual DbSet<Core.AccountRoleLanguage> AccountRoleLanguage { get; set; }
        public virtual DbSet<Core.AccountRolePermission> AccountRolePermission { get; set; }
        public virtual DbSet<Core.AccountTheme> AccountTheme { get; set; }
        public virtual DbSet<Core.AccountUser> AccountUser { get; set; }
        public virtual DbSet<Core.AccountUserPermission> AccountUserPermission { get; set; }
        public virtual DbSet<Core.Address> Address { get; set; }
        public virtual DbSet<Core.AddressCity> AddressCity { get; set; }
        public virtual DbSet<Core.AddressCountry> AddressCountry { get; set; }
        public virtual DbSet<Core.AddressDistrict> AddressDistrict { get; set; }
        public virtual DbSet<Core.AddressType> AddressType { get; set; }
        public virtual DbSet<Core.SystemLayout> SystemLayout { get; set; }
        public virtual DbSet<Core.SystemMenu> SystemMenu { get; set; }
        public virtual DbSet<Core.SystemMenuLanguage> SystemMenuLanguage { get; set; }
        public virtual DbSet<Core.SystemVillage> SystemVillage { get; set; }
        public virtual DbSet<Core.TemplateDetail1> TemplateDetail1 { get; set; }
        public virtual DbSet<Core.TemplateDetail2> TemplateDetail2 { get; set; }
        public virtual DbSet<Core.TemplateMaster> TemplateMaster { get; set; }
        public virtual DbSet<Core.TemplatePerson> TemplatePerson { get; set; }
        public virtual DbSet<Core.TemplateUser> TemplateUser { get; set; }
    }
}
