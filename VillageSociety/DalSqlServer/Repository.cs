﻿using Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DalSqlServer
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        internal VillageSocietyEntities context;
        internal DbSet<TEntity> dbSet;

        public Repository(VillageSocietyEntities context)
        {
            //context.Configuration.ProxyCreationEnabled = false;
            //context.Configuration.AutoDetectChangesEnabled = false;
            this.context = context;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = dbSet;
            
            if (filter == null)
            {
                return query;
            }
            else
            {
                return query.Where(filter);
            }
        }

        public virtual TEntity First(Expression<Func<TEntity, bool>> filter)
        {
            return dbSet.First(filter);
        }
        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter)
        {
            return dbSet.FirstOrDefault(filter);
        }
        public virtual TEntity Single(Expression<Func<TEntity, bool>> filter)
        {
            return dbSet.Single(filter);
        }
        public virtual TEntity SingleOrDefault(Expression<Func<TEntity, bool>> filter)
        {
            return dbSet.SingleOrDefault(filter);
        }
        public virtual TEntity GetById(int id)
        {
            return dbSet.Find(id);
        }
        public virtual void Insert(TEntity item)
        {
            dbSet.Add(item);
        }
        public virtual void Update(TEntity item)
        {
            var entry = context.Entry(item);
            entry.State = EntityState.Modified;
            entry.IgnorePropertiesForUpdate();
        }
        public virtual void Edit(TEntity item, params string[] ignoreButSomeProperties)
        {
            var entry = context.Entry(item);
            entry.State = EntityState.Modified;
            entry.IgnoreButSomeProperties(ignoreButSomeProperties);
        }
        public virtual void Delete(TEntity item)
        {
            var entry = context.Entry(item);
            entry.State = EntityState.Modified;
            entry.IgnorePropertiesForDelete();
        }
        public virtual void Restore(TEntity item, bool ignoreProperties)
        {
            var entry = context.Entry(item);
            entry.State = EntityState.Modified;
            entry.IgnorePropertiesForRestore(ignoreProperties);
        }
        protected virtual void Save()
        {
            context.SaveChanges();
        }

        public virtual UniqueKeyException ParseUniqueKeyException(Exception exception, TEntity item)
        {
            UniqueKeyException uniqueKeyException = UniqueKeyException.Parse(exception, typeof(TEntity).Name);

            if (uniqueKeyException != null)
            {
                var entry = context.Entry(item);

                ParameterExpression pe = Expression.Parameter(typeof(TEntity), "p");
                BinaryExpression expression = null;

                foreach (string column in uniqueKeyException.Columns)
                {
                    Expression left = Expression.Property(pe, column);
                    ConstantExpression right = Expression.Constant(entry.Property(column).CurrentValue);
                    BinaryExpression be = Expression.Equal(left, right);
                    //Expression.Lambda<Func<TEntity, bool>>()
                    expression = expression == null ? be : Expression.And(expression, be);
                }

                var filter = Expression.Lambda<Func<TEntity, bool>>(expression, new ParameterExpression[] { pe });
                TEntity entity = this.Single(filter);
                uniqueKeyException.Id = entity.Id;
                uniqueKeyException.IsDeleted = entity.IsDeleted;
            }

            return uniqueKeyException;
        }
        public virtual JsonResultModel ExceptionToJsonResult(Exception exception, TEntity item, bool isNewRecord)
        {
            JsonResultModel jsonModel = null;

            UniqueKeyException uniqueKeyException = ParseUniqueKeyException(exception, item);

            if (uniqueKeyException != null)
            {
                jsonModel = new JsonResultModel();

                if (uniqueKeyException.IsDeleted)
                {
                    if (isNewRecord)
                    {
                        jsonModel.Result = new { IsNewRecord = isNewRecord, ExistingId = uniqueKeyException.Id };
                        jsonModel.Exception = "UniqueKeyExceptionWhileInserting";
                    }
                    else
                    {
                        jsonModel.Result = new { IsNewRecord = isNewRecord, ExistingId = uniqueKeyException.Id };
                        jsonModel.Exception = "UniqueKeyExceptionWhileUpdating";
                    }
                }
                else
                {
                    jsonModel.Exception = "UniqueKeyExceptionExistingRecord";
                }
            }

            return jsonModel;
        }
    }
}
