﻿using Core;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DalSqlServer
{
    public static class ExtensionMethod
    {
        public static void IgnorePropertiesForUpdate<TEntity>(this DbEntityEntry<TEntity> entry) where TEntity : BaseEntity
        {
            if (typeof(TEntity).IsSubclassOf(typeof(RowNumberedEntity)))
            {
                entry.Property("RowNumber").IsModified = false;
            }
            entry.Property(p => p.InsertDate).IsModified = false;
            entry.Property(p => p.InsertedBy).IsModified = false;
            entry.Property(p => p.DeleteDate).IsModified = false;
            entry.Property(p => p.DeletedBy).IsModified = false;
            entry.Property(p => p.IsDeleted).IsModified = false;
        }
        public static void IgnorePropertiesForDelete<TEntity>(this DbEntityEntry<TEntity> entry) where TEntity : BaseEntity
        {
            foreach(var propertyName in entry.CurrentValues.PropertyNames)
            {
                if (propertyName != "Id" && propertyName != "DeleteDate" && propertyName != "DeletedBy" && propertyName != "IsDeleted")
                {
                    var property = entry.Property(propertyName);
                    property.IsModified = false;
                }
            }
        }
        public static void IgnorePropertiesForRestore<TEntity>(this DbEntityEntry<TEntity> entry, bool ignoreProperties) where TEntity : BaseEntity
        {
            if (ignoreProperties)
            {
                foreach (var propertyName in entry.CurrentValues.PropertyNames)
                {
                    if (propertyName != "Id" && propertyName != "UpdateDate" && propertyName != "UpdatedBy" && propertyName != "IsDeleted")
                    {
                        var property = entry.Property(propertyName);
                        property.IsModified = false;
                    }
                }
            }
        }

        public static void IgnoreButSomeProperties<TEntity>(this DbEntityEntry<TEntity> entry, params string[] someProperties) where TEntity : BaseEntity
        {
            foreach (var propertyName in entry.CurrentValues.PropertyNames)
            {
                if (propertyName != "Id" && !someProperties.Contains(propertyName))
                {
                    var property = entry.Property(propertyName);
                    property.IsModified = false;
                }
            }
        }
    }
}
