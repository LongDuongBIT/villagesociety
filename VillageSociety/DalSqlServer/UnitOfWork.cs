﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DalSqlServer
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly VillageSocietyEntities context;
        private Dictionary<string, object> repositories;

        public UnitOfWork()
        {
            context = new VillageSocietyEntities();
        }

        public UnitOfWork(VillageSocietyEntities context)
        {
            //context.Configuration.ValidateOnSaveEnabled = false;
            //context.Configuration.ProxyCreationEnabled = false;
            context.Configuration.AutoDetectChangesEnabled = false;
            this.context = context;
        }

        public IRepository<T> Repository<T>() where T : BaseEntity
        {
            if (repositories == null)
            {
                repositories = new Dictionary<string, object>();
            }

            var type = typeof(T);

            if (!repositories.ContainsKey(type.Name))
            {
                var repositoryType = type.IsSubclassOf(typeof(RowNumberedEntity)) ? typeof(RowNumberedRepository<>) : typeof(Repository<>);
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), context);
                repositories.Add(type.Name, repositoryInstance);
            }
            return (Repository<T>)repositories[type.Name];
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
