USE [master]
GO
/****** Object:  Database [VillageSociety]    Script Date: 15.12.2015 10:36:47 ******/
CREATE DATABASE [VillageSociety]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'VillageSociety', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VillageSociety.mdf' , SIZE = 9216KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'VillageSociety_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VillageSociety_log.ldf' , SIZE = 4672KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [VillageSociety] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [VillageSociety].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [VillageSociety] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [VillageSociety] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [VillageSociety] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [VillageSociety] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [VillageSociety] SET ARITHABORT OFF 
GO
ALTER DATABASE [VillageSociety] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [VillageSociety] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [VillageSociety] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [VillageSociety] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [VillageSociety] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [VillageSociety] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [VillageSociety] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [VillageSociety] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [VillageSociety] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [VillageSociety] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [VillageSociety] SET  DISABLE_BROKER 
GO
ALTER DATABASE [VillageSociety] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [VillageSociety] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [VillageSociety] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [VillageSociety] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [VillageSociety] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [VillageSociety] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [VillageSociety] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [VillageSociety] SET RECOVERY FULL 
GO
ALTER DATABASE [VillageSociety] SET  MULTI_USER 
GO
ALTER DATABASE [VillageSociety] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [VillageSociety] SET DB_CHAINING OFF 
GO
ALTER DATABASE [VillageSociety] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [VillageSociety] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'VillageSociety', N'ON'
GO
USE [VillageSociety]
GO
/****** Object:  Table [dbo].[AccountLanguage]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountLanguage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageCode] [char](2) NOT NULL,
	[LanguageName] [nvarchar](50) NOT NULL,
	[LanguageDescription] [nvarchar](512) NULL,
	[IconPath] [nvarchar](1024) NOT NULL,
	[RowNumber] [float] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountLanguage] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountPermission]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountPermission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PermissionName] [nvarchar](50) NOT NULL,
	[PermissionDescription] [nvarchar](512) NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountPermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountPermissionMap]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountPermissionMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[PermissionId] [int] NULL,
	[PermissionMapName] [nvarchar](50) NOT NULL,
	[PermissionMapDescription] [nvarchar](512) NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountPermissionMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountPermissionMapLanguage]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountPermissionMapLanguage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PermissionMapId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[PermissionMapName] [nvarchar](50) NOT NULL,
	[PermissionMapDescription] [nvarchar](512) NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountPermissionMapLanguage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountPerson]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountPerson](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VillageId] [int] NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[IsMale] [bit] NOT NULL,
	[PicturePath] [nvarchar](1024) NULL,
	[Father] [int] NULL,
	[Mother] [int] NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountPerson] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountPersonAddress]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountPersonAddress](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[AddressId] [int] NOT NULL,
	[AddressTypeId] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountPersonAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountRole]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](50) NOT NULL,
	[RoleDescription] [nvarchar](512) NULL,
	[RowNumber] [float] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountRole] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountRoleLanguage]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountRoleLanguage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[RoleName] [nvarchar](50) NOT NULL,
	[RoleDescription] [nvarchar](512) NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountRoleLanguage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountRolePermission]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountRolePermission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountRolePermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountTheme]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountTheme](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ThemeName] [nvarchar](50) NOT NULL,
	[ThemeDescription] [nvarchar](512) NULL,
	[IconPath] [nvarchar](1024) NOT NULL,
	[RowNumber] [float] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountTheme] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountUser]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[UserEMail] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[ThemeId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[IsSystemAdmin] [bit] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountUserPermission]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountUserPermission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AccountUserPermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Address]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DistrictId] [int] NOT NULL,
	[FullAddress] [nvarchar](255) NOT NULL,
	[FullAddressDescription] [nvarchar](512) NULL,
	[PostalCode] [nvarchar](15) NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[RowNumber] [float] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AddressCity]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddressCity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryId] [int] NOT NULL,
	[CityName] [nvarchar](255) NOT NULL,
	[CityDescription] [nvarchar](512) NULL,
	[RowNumber] [float] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_City] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AddressCountry]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddressCountry](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [nvarchar](3) NOT NULL,
	[CountryName] [nvarchar](255) NOT NULL,
	[CountryDescription] [nvarchar](512) NULL,
	[RowNumber] [float] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AddressDistrict]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddressDistrict](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CityId] [int] NOT NULL,
	[DistrictName] [nvarchar](255) NOT NULL,
	[DistrictDescription] [nvarchar](512) NULL,
	[RowNumber] [float] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_District] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AddressType]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddressType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AddressTypeName] [nvarchar](50) NOT NULL,
	[AddressTypeDescription] [nvarchar](512) NULL,
	[RowNumber] [float] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_AddressType] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemLayout]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemLayout](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[LayoutName] [nvarchar](255) NOT NULL,
	[LayoutData] [nvarchar](4000) NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_SystemLayout] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemMenu]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemMenu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[PermissionId] [int] NULL,
	[MenuName] [nvarchar](50) NOT NULL,
	[MenuDescription] [nvarchar](512) NULL,
	[NavigateUrl] [nvarchar](255) NULL,
	[ControllerName] [nvarchar](255) NULL,
	[ActionName] [nvarchar](255) NULL,
	[IconPath] [nvarchar](1024) NOT NULL,
	[RowNumber] [float] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_SystemMenu] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemMenuLanguage]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemMenuLanguage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MenuId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[MenuName] [nvarchar](50) NOT NULL,
	[MenuDescription] [nvarchar](512) NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_SystemMenuLanguage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemVillage]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemVillage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VillageName] [nvarchar](50) NOT NULL,
	[VillageDescription] [nvarchar](512) NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_SystemVillage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TemplateDetail1]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemplateDetail1](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MasterId] [int] NOT NULL,
	[Detail1Name] [nvarchar](50) NOT NULL,
	[Detail1Description] [nvarchar](512) NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TemplateDetail1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TemplateDetail2]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemplateDetail2](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MasterId] [int] NOT NULL,
	[Detail2Name] [nvarchar](50) NOT NULL,
	[Detail2Description] [nvarchar](512) NOT NULL,
	[RowNumber] [float] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TemplateDetail2] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TemplateMaster]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemplateMaster](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ComboBoxPersonId] [int] NOT NULL,
	[GridLookupPersonId] [int] NOT NULL,
	[ComboBoxAddressId] [int] NOT NULL,
	[GridLookupAddressId] [int] NOT NULL,
	[BitColumn1] [bit] NOT NULL,
	[BitColumn2] [bit] NOT NULL,
	[IntColumn] [int] NOT NULL,
	[FloatColumn] [float] NOT NULL,
	[DecimalColumn] [decimal](18, 0) NOT NULL,
	[MoneyColumn] [decimal](18, 0) NOT NULL,
	[PercentColumn] [decimal](18, 0) NOT NULL,
	[DateColumn] [date] NOT NULL,
	[DateTimeColumn] [datetime] NOT NULL,
	[StringColumn] [nvarchar](50) NOT NULL,
	[MultilineColumn] [nvarchar](max) NOT NULL,
	[HtmlColumn] [nvarchar](max) NOT NULL,
	[PhoneNumberColumn] [nvarchar](50) NOT NULL,
	[CreditCardNumberColumn] [nvarchar](50) NOT NULL,
	[LatitudeColumn] [float] NOT NULL,
	[LongitudeColumn] [float] NOT NULL,
	[FilePathColumn] [nvarchar](1024) NOT NULL,
	[ImagePathColumn] [nvarchar](1024) NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TemplateMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TemplatePerson]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemplatePerson](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MasterId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TemplatePerson] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TemplateUser]    Script Date: 15.12.2015 10:36:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemplateUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MasterId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[InsertedBy] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedBy] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TemplateUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_AccountLanguage_RowNumber]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_AccountLanguage_RowNumber] ON [dbo].[AccountLanguage]
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AccountRole_RowNumber]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_AccountRole_RowNumber] ON [dbo].[AccountRole]
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AccountTheme_RowNumber]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_AccountTheme_RowNumber] ON [dbo].[AccountTheme]
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Address_RowNumber]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_Address_RowNumber] ON [dbo].[Address]
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AddressCity_RowNumber]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_AddressCity_RowNumber] ON [dbo].[AddressCity]
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AddressCountry_RowNumber]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_AddressCountry_RowNumber] ON [dbo].[AddressCountry]
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AddressDistrict_RowNumber]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_AddressDistrict_RowNumber] ON [dbo].[AddressDistrict]
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AddressType_RowNumber]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_AddressType_RowNumber] ON [dbo].[AddressType]
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SystemMenu_RowNumber]    Script Date: 15.12.2015 10:36:47 ******/
CREATE CLUSTERED INDEX [IX_SystemMenu_RowNumber] ON [dbo].[SystemMenu]
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TemplateDetail2_RowNumber]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_TemplateDetail2_RowNumber] ON [dbo].[TemplateDetail2]
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AccountLanguage_LanguageCode_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountLanguage_LanguageCode_Unique] ON [dbo].[AccountLanguage]
(
	[LanguageCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AccountLanguage_LanguageName_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountLanguage_LanguageName_Unique] ON [dbo].[AccountLanguage]
(
	[LanguageName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AccountPermission_PermissionName_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountPermission_PermissionName_Unique] ON [dbo].[AccountPermission]
(
	[PermissionName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AccountPermissionMapLanguage_PermissionMapId_LanguageId_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountPermissionMapLanguage_PermissionMapId_LanguageId_Unique] ON [dbo].[AccountPermissionMapLanguage]
(
	[PermissionMapId] ASC,
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AccountPersonAddress_PersonId_AddressId_AddressTypeId_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountPersonAddress_PersonId_AddressId_AddressTypeId_Unique] ON [dbo].[AccountPersonAddress]
(
	[PersonId] ASC,
	[AddressId] ASC,
	[AddressTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AccountRole_RoleName_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountRole_RoleName_Unique] ON [dbo].[AccountRole]
(
	[RoleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AccountRoleLanguage_RoleId_LanguageId_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountRoleLanguage_RoleId_LanguageId_Unique] ON [dbo].[AccountRoleLanguage]
(
	[RoleId] ASC,
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AccountRolePermission_RoleId_PermissionId_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountRolePermission_RoleId_PermissionId_Unique] ON [dbo].[AccountRolePermission]
(
	[RoleId] ASC,
	[PermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AccountTheme_ThemeName_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountTheme_ThemeName_Unique] ON [dbo].[AccountTheme]
(
	[ThemeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AccountUser_UserEMail_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountUser_UserEMail_Unique] ON [dbo].[AccountUser]
(
	[UserEMail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AccountUser_Username_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountUser_Username_Unique] ON [dbo].[AccountUser]
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Address_FullAddress_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Address_FullAddress_Unique] ON [dbo].[Address]
(
	[FullAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AddressCity_CityName_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AddressCity_CityName_Unique] ON [dbo].[AddressCity]
(
	[CityName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AddressCountry_CountryCode_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AddressCountry_CountryCode_Unique] ON [dbo].[AddressCountry]
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AddressCountry_CountryName_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AddressCountry_CountryName_Unique] ON [dbo].[AddressCountry]
(
	[CountryName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AddressDistrict_DistrictName_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AddressDistrict_DistrictName_Unique] ON [dbo].[AddressDistrict]
(
	[DistrictName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AddressType_AddressTypeName_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AddressType_AddressTypeName_Unique] ON [dbo].[AddressType]
(
	[AddressTypeName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_SystemLayout_UserId_LayoutName_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_SystemLayout_UserId_LayoutName_Unique] ON [dbo].[SystemLayout]
(
	[UserId] ASC,
	[LayoutName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SystemMenuLanguage_MenuId_LanguageId_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_SystemMenuLanguage_MenuId_LanguageId_Unique] ON [dbo].[SystemMenuLanguage]
(
	[MenuId] ASC,
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TemplateDetail2_Detail2Name_Unique]    Script Date: 15.12.2015 10:36:47 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TemplateDetail2_Detail2Name_Unique] ON [dbo].[TemplateDetail2]
(
	[Detail2Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AccountPermissionMap]  WITH CHECK ADD  CONSTRAINT [FK_AccountPermissionMap_AccountPermission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[AccountPermission] ([Id])
GO
ALTER TABLE [dbo].[AccountPermissionMap] CHECK CONSTRAINT [FK_AccountPermissionMap_AccountPermission]
GO
ALTER TABLE [dbo].[AccountPermissionMap]  WITH CHECK ADD  CONSTRAINT [FK_AccountPermissionMap_AccountPermissionMap] FOREIGN KEY([ParentId])
REFERENCES [dbo].[AccountPermissionMap] ([Id])
GO
ALTER TABLE [dbo].[AccountPermissionMap] CHECK CONSTRAINT [FK_AccountPermissionMap_AccountPermissionMap]
GO
ALTER TABLE [dbo].[AccountPermissionMapLanguage]  WITH CHECK ADD  CONSTRAINT [FK_AccountPermissionMapLanguage_AccountLanguage] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[AccountLanguage] ([Id])
GO
ALTER TABLE [dbo].[AccountPermissionMapLanguage] CHECK CONSTRAINT [FK_AccountPermissionMapLanguage_AccountLanguage]
GO
ALTER TABLE [dbo].[AccountPermissionMapLanguage]  WITH CHECK ADD  CONSTRAINT [FK_AccountPermissionMapLanguage_AccountPermissionMap] FOREIGN KEY([PermissionMapId])
REFERENCES [dbo].[AccountPermissionMap] ([Id])
GO
ALTER TABLE [dbo].[AccountPermissionMapLanguage] CHECK CONSTRAINT [FK_AccountPermissionMapLanguage_AccountPermissionMap]
GO
ALTER TABLE [dbo].[AccountPerson]  WITH CHECK ADD  CONSTRAINT [FK_AccountPerson_AccountPerson] FOREIGN KEY([Father])
REFERENCES [dbo].[AccountPerson] ([Id])
GO
ALTER TABLE [dbo].[AccountPerson] CHECK CONSTRAINT [FK_AccountPerson_AccountPerson]
GO
ALTER TABLE [dbo].[AccountPerson]  WITH CHECK ADD  CONSTRAINT [FK_AccountPerson_AccountPerson1] FOREIGN KEY([Mother])
REFERENCES [dbo].[AccountPerson] ([Id])
GO
ALTER TABLE [dbo].[AccountPerson] CHECK CONSTRAINT [FK_AccountPerson_AccountPerson1]
GO
ALTER TABLE [dbo].[AccountPerson]  WITH CHECK ADD  CONSTRAINT [FK_AccountPerson_SystemVillage] FOREIGN KEY([VillageId])
REFERENCES [dbo].[SystemVillage] ([Id])
GO
ALTER TABLE [dbo].[AccountPerson] CHECK CONSTRAINT [FK_AccountPerson_SystemVillage]
GO
ALTER TABLE [dbo].[AccountPersonAddress]  WITH CHECK ADD  CONSTRAINT [FK_AccountPersonAddress_AccountPerson] FOREIGN KEY([PersonId])
REFERENCES [dbo].[AccountPerson] ([Id])
GO
ALTER TABLE [dbo].[AccountPersonAddress] CHECK CONSTRAINT [FK_AccountPersonAddress_AccountPerson]
GO
ALTER TABLE [dbo].[AccountPersonAddress]  WITH CHECK ADD  CONSTRAINT [FK_AccountPersonAddress_Address] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([Id])
GO
ALTER TABLE [dbo].[AccountPersonAddress] CHECK CONSTRAINT [FK_AccountPersonAddress_Address]
GO
ALTER TABLE [dbo].[AccountPersonAddress]  WITH CHECK ADD  CONSTRAINT [FK_AccountPersonAddress_AddressType] FOREIGN KEY([AddressTypeId])
REFERENCES [dbo].[AddressType] ([Id])
GO
ALTER TABLE [dbo].[AccountPersonAddress] CHECK CONSTRAINT [FK_AccountPersonAddress_AddressType]
GO
ALTER TABLE [dbo].[AccountRoleLanguage]  WITH CHECK ADD  CONSTRAINT [FK_AccountRoleLanguage_AccountLanguage] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[AccountLanguage] ([Id])
GO
ALTER TABLE [dbo].[AccountRoleLanguage] CHECK CONSTRAINT [FK_AccountRoleLanguage_AccountLanguage]
GO
ALTER TABLE [dbo].[AccountRoleLanguage]  WITH CHECK ADD  CONSTRAINT [FK_AccountRoleLanguage_AccountRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AccountRole] ([Id])
GO
ALTER TABLE [dbo].[AccountRoleLanguage] CHECK CONSTRAINT [FK_AccountRoleLanguage_AccountRole]
GO
ALTER TABLE [dbo].[AccountRolePermission]  WITH CHECK ADD  CONSTRAINT [FK_AccountRolePermission_AccountPermission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[AccountPermission] ([Id])
GO
ALTER TABLE [dbo].[AccountRolePermission] CHECK CONSTRAINT [FK_AccountRolePermission_AccountPermission]
GO
ALTER TABLE [dbo].[AccountRolePermission]  WITH CHECK ADD  CONSTRAINT [FK_AccountRolePermission_AccountRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AccountRole] ([Id])
GO
ALTER TABLE [dbo].[AccountRolePermission] CHECK CONSTRAINT [FK_AccountRolePermission_AccountRole]
GO
ALTER TABLE [dbo].[AccountUser]  WITH CHECK ADD  CONSTRAINT [FK_AccountUser_AccountLanguage] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[AccountLanguage] ([Id])
GO
ALTER TABLE [dbo].[AccountUser] CHECK CONSTRAINT [FK_AccountUser_AccountLanguage]
GO
ALTER TABLE [dbo].[AccountUser]  WITH CHECK ADD  CONSTRAINT [FK_AccountUser_AccountPerson] FOREIGN KEY([PersonId])
REFERENCES [dbo].[AccountPerson] ([Id])
GO
ALTER TABLE [dbo].[AccountUser] CHECK CONSTRAINT [FK_AccountUser_AccountPerson]
GO
ALTER TABLE [dbo].[AccountUser]  WITH CHECK ADD  CONSTRAINT [FK_AccountUser_AccountRole] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AccountRole] ([Id])
GO
ALTER TABLE [dbo].[AccountUser] CHECK CONSTRAINT [FK_AccountUser_AccountRole]
GO
ALTER TABLE [dbo].[AccountUser]  WITH CHECK ADD  CONSTRAINT [FK_AccountUser_AccountTheme] FOREIGN KEY([ThemeId])
REFERENCES [dbo].[AccountTheme] ([Id])
GO
ALTER TABLE [dbo].[AccountUser] CHECK CONSTRAINT [FK_AccountUser_AccountTheme]
GO
ALTER TABLE [dbo].[AccountUserPermission]  WITH CHECK ADD  CONSTRAINT [FK_AccountUserPermission_AccountPermission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[AccountPermission] ([Id])
GO
ALTER TABLE [dbo].[AccountUserPermission] CHECK CONSTRAINT [FK_AccountUserPermission_AccountPermission]
GO
ALTER TABLE [dbo].[AccountUserPermission]  WITH CHECK ADD  CONSTRAINT [FK_AccountUserPermission_AccountUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[AccountUser] ([Id])
GO
ALTER TABLE [dbo].[AccountUserPermission] CHECK CONSTRAINT [FK_AccountUserPermission_AccountUser]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_District] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[AddressDistrict] ([Id])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_District]
GO
ALTER TABLE [dbo].[AddressCity]  WITH CHECK ADD  CONSTRAINT [FK_City_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[AddressCountry] ([Id])
GO
ALTER TABLE [dbo].[AddressCity] CHECK CONSTRAINT [FK_City_Country]
GO
ALTER TABLE [dbo].[AddressDistrict]  WITH CHECK ADD  CONSTRAINT [FK_District_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[AddressCity] ([Id])
GO
ALTER TABLE [dbo].[AddressDistrict] CHECK CONSTRAINT [FK_District_City]
GO
ALTER TABLE [dbo].[SystemLayout]  WITH CHECK ADD  CONSTRAINT [FK_SystemLayout_AccountUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[AccountUser] ([Id])
GO
ALTER TABLE [dbo].[SystemLayout] CHECK CONSTRAINT [FK_SystemLayout_AccountUser]
GO
ALTER TABLE [dbo].[SystemMenu]  WITH CHECK ADD  CONSTRAINT [FK_SystemMenu_AccountPermission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[AccountPermission] ([Id])
GO
ALTER TABLE [dbo].[SystemMenu] CHECK CONSTRAINT [FK_SystemMenu_AccountPermission]
GO
ALTER TABLE [dbo].[SystemMenu]  WITH CHECK ADD  CONSTRAINT [FK_SystemMenu_SystemMenu] FOREIGN KEY([ParentId])
REFERENCES [dbo].[SystemMenu] ([Id])
GO
ALTER TABLE [dbo].[SystemMenu] CHECK CONSTRAINT [FK_SystemMenu_SystemMenu]
GO
ALTER TABLE [dbo].[SystemMenuLanguage]  WITH CHECK ADD  CONSTRAINT [FK_SystemMenuLanguage_AccountLanguage] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[AccountLanguage] ([Id])
GO
ALTER TABLE [dbo].[SystemMenuLanguage] CHECK CONSTRAINT [FK_SystemMenuLanguage_AccountLanguage]
GO
ALTER TABLE [dbo].[SystemMenuLanguage]  WITH CHECK ADD  CONSTRAINT [FK_SystemMenuLanguage_SystemMenu] FOREIGN KEY([MenuId])
REFERENCES [dbo].[SystemMenu] ([Id])
GO
ALTER TABLE [dbo].[SystemMenuLanguage] CHECK CONSTRAINT [FK_SystemMenuLanguage_SystemMenu]
GO
ALTER TABLE [dbo].[TemplateDetail1]  WITH CHECK ADD  CONSTRAINT [FK_TemplateDetail1_TemplateMaster] FOREIGN KEY([MasterId])
REFERENCES [dbo].[TemplateMaster] ([Id])
GO
ALTER TABLE [dbo].[TemplateDetail1] CHECK CONSTRAINT [FK_TemplateDetail1_TemplateMaster]
GO
ALTER TABLE [dbo].[TemplateDetail2]  WITH CHECK ADD  CONSTRAINT [FK_TemplateDetail2_TemplateMaster] FOREIGN KEY([MasterId])
REFERENCES [dbo].[TemplateMaster] ([Id])
GO
ALTER TABLE [dbo].[TemplateDetail2] CHECK CONSTRAINT [FK_TemplateDetail2_TemplateMaster]
GO
ALTER TABLE [dbo].[TemplateMaster]  WITH CHECK ADD  CONSTRAINT [FK_TemplateMaster_AccountPerson] FOREIGN KEY([ComboBoxPersonId])
REFERENCES [dbo].[AccountPerson] ([Id])
GO
ALTER TABLE [dbo].[TemplateMaster] CHECK CONSTRAINT [FK_TemplateMaster_AccountPerson]
GO
ALTER TABLE [dbo].[TemplateMaster]  WITH CHECK ADD  CONSTRAINT [FK_TemplateMaster_AccountPerson1] FOREIGN KEY([GridLookupPersonId])
REFERENCES [dbo].[AccountPerson] ([Id])
GO
ALTER TABLE [dbo].[TemplateMaster] CHECK CONSTRAINT [FK_TemplateMaster_AccountPerson1]
GO
ALTER TABLE [dbo].[TemplateMaster]  WITH CHECK ADD  CONSTRAINT [FK_TemplateMaster_Address] FOREIGN KEY([ComboBoxAddressId])
REFERENCES [dbo].[Address] ([Id])
GO
ALTER TABLE [dbo].[TemplateMaster] CHECK CONSTRAINT [FK_TemplateMaster_Address]
GO
ALTER TABLE [dbo].[TemplateMaster]  WITH CHECK ADD  CONSTRAINT [FK_TemplateMaster_Address1] FOREIGN KEY([GridLookupAddressId])
REFERENCES [dbo].[Address] ([Id])
GO
ALTER TABLE [dbo].[TemplateMaster] CHECK CONSTRAINT [FK_TemplateMaster_Address1]
GO
ALTER TABLE [dbo].[TemplatePerson]  WITH CHECK ADD  CONSTRAINT [FK_TemplatePerson_AccountPerson] FOREIGN KEY([PersonId])
REFERENCES [dbo].[AccountPerson] ([Id])
GO
ALTER TABLE [dbo].[TemplatePerson] CHECK CONSTRAINT [FK_TemplatePerson_AccountPerson]
GO
ALTER TABLE [dbo].[TemplatePerson]  WITH CHECK ADD  CONSTRAINT [FK_TemplatePerson_TemplateMaster] FOREIGN KEY([MasterId])
REFERENCES [dbo].[TemplateMaster] ([Id])
GO
ALTER TABLE [dbo].[TemplatePerson] CHECK CONSTRAINT [FK_TemplatePerson_TemplateMaster]
GO
ALTER TABLE [dbo].[TemplateUser]  WITH CHECK ADD  CONSTRAINT [FK_TemplateUser_AccountUser] FOREIGN KEY([UserId])
REFERENCES [dbo].[AccountUser] ([Id])
GO
ALTER TABLE [dbo].[TemplateUser] CHECK CONSTRAINT [FK_TemplateUser_AccountUser]
GO
ALTER TABLE [dbo].[TemplateUser]  WITH CHECK ADD  CONSTRAINT [FK_TemplateUser_TemplateMaster] FOREIGN KEY([MasterId])
REFERENCES [dbo].[TemplateMaster] ([Id])
GO
ALTER TABLE [dbo].[TemplateUser] CHECK CONSTRAINT [FK_TemplateUser_TemplateMaster]
GO
USE [master]
GO
ALTER DATABASE [VillageSociety] SET  READ_WRITE 
GO
