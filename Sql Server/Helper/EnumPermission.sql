use VillageSociety;

declare @s nvarchar(max) = '', @Id int, @PermissionName nvarchar(255);

declare cur cursor for
	select Id, PermissionName from AccountPermission where IsDeleted = 0;

open cur;

fetch next from cur into @Id, @PermissionName;

while @@FETCH_STATUS = 0
begin
	if @s <> ''
		set @s = @s + ',
	';

	set @PermissionName = REPLACE(@PermissionName, ' of ', ' Of ');

	set @s = @s + REPLACE(@PermissionName, ' ', '') + ' = ' + cast(@Id as nvarchar(10));

	fetch next from cur into @Id, @PermissionName;
end;

close cur;
deallocate cur;

set @s = 'public enum Permission : int
{
	None = 0,
	' + @s + '
}';

select @s;