use VillageSociety;

declare @rowNumber int;

---Village
insert into SystemVillage (VillageName, InsertDate, InsertedBy, IsDeleted) values('Akkise K�y�', '20120101', 1, 0); declare @vAkkiseKoyu int = scope_identity();

---AddressCountry
set @rowNumber = 0;
set identity_insert AddressCountry on 
insert AddressCountry (Id, CountryCode, CountryName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (1, N'TR', N'T�rkiye', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressCountry;
insert AddressCountry (Id, CountryCode, CountryName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (2, N'DE', N'Almanya', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressCountry;
insert AddressCountry (Id, CountryCode, CountryName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (3, N'US', N'Birle�ik Devletler', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressCountry;
set identity_insert AddressCountry off

---AddressCity
set @rowNumber = 0;
set identity_insert AddressCity on 
insert AddressCity (Id, CountryId, CityName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (1, 1, N'Konya', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressCity;
insert AddressCity (Id, CountryId, CityName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (2, 1, N'�stanbul', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressCity;
insert AddressCity (Id, CountryId, CityName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (3, 1, N'Ankara', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressCity;
insert AddressCity (Id, CountryId, CityName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (4, 2, N'Baden-V�rtemberg', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressCity;
insert AddressCity (Id, CountryId, CityName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (5, 2, N'Berlin', '20120101', @rowNumber, 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressCity;
insert AddressCity (Id, CountryId, CityName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (6, 3, N'District of Columbia', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressCity;
insert AddressCity (Id, CountryId, CityName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (7, 3, N'New York', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressCity;
insert AddressCity (Id, CountryId, CityName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (8, 3, N'Antalya', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressCity;
set identity_insert AddressCity off

---AddressDistrict
set @rowNumber = 0;
set identity_insert AddressDistrict on 
insert AddressDistrict (Id, CityId, DistrictName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (1, 1, N'Ah�rl�', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressDistrict;
insert AddressDistrict (Id, CityId, DistrictName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (2, 1, N'Karatay', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressDistrict;
insert AddressDistrict (Id, CityId, DistrictName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (3, 1, N'Sel�uklu', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressDistrict;
insert AddressDistrict (Id, CityId, DistrictName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (4, 2, N'Fatih', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressDistrict;
insert AddressDistrict (Id, CityId, DistrictName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (5, 3, N'Polatl�', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressDistrict;
insert AddressDistrict (Id, CityId, DistrictName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (6, 4, N'Regierungsbezirk Stuttgart', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressDistrict;
insert AddressDistrict (Id, CityId, DistrictName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (7, 5, N'Treptow-K�penick', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressDistrict;
insert AddressDistrict (Id, CityId, DistrictName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (8, 6, N'Washington', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressDistrict;
insert AddressDistrict (Id, CityId, DistrictName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (9, 7, N'Manhattan', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressDistrict;
insert AddressDistrict (Id, CityId, DistrictName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (10, 7, N'Kepez', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressDistrict;
set identity_insert AddressDistrict off

---Address
set @rowNumber = 0;
set identity_insert Address on 
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (1, 1, N'Akkise K�y� Yolu, 42180 Akkise/Konya, T�rkiye', N'42180', 37.365697040292183, 32.1348069445969, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (2, 1, N'Bozk�r Seydi�ehir Yolu, Ali�er�i/Konya, T�rkiye', NULL, 37.253465575766405, 32.147853209245341, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (3, 2, N'�elebi Mh., Dolap Pare Caddesi 6-10, 42000 Konya/Konya, T�rkiye', N'42000', 37.87530161301413, 32.512805296647684, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (4, 2, N'�elebi Mh., Ali Ulvi Kurucu Caddesi (Susurlu Sk.) No:2, 42000 Konya/Konya, T�rkiye', N'42000', 37.870959384817127, 32.515116238745122, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (5, 3, N'Feritpa�a Mh., Kerk�k Caddesi 27-39, 42000 Konya/Konya, T�rkiye', N'42000', 37.887280956756129, 32.489267328527831, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (6, 4, N'Topkap� Mh., �apa �stanbul, �apa T�p Fk., 34200 Fatih/�stanbul, T�rkiye', N'34200', 41.016895, 28.934180999999967, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (7, 5, N'Zafer Mh., Dedeefendi Caddesi 29-39, 06900 Polatl�/Ankara, T�rkiye', N'06900', 39.577990290738619, 32.138639450293, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (8, 6, N'Stuttgart Airport (STR), Flughafen Frachtzentrum 615, 70629 Stuttgart, Almanya', N'70629', 48.68491613563063, 9.2071209523926427, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (9, 7, N'Am Treptower Park 2, 12435 Berlin, Almanya', N'12435', 52.4932827191504, 13.455635222753926, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (10, 8, N'1155 15th Street Northwest, Washington, DC 20005, Birle�ik Devletler', N'20005', 38.905188560356031, -77.033780795214852, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (11, 9, N'158 Hester Street, New York, New York 10013, Birle�ik Devletler', N'10013', 40.7177279777992, -73.997014908398441, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (12, 1, N'Ak�ren Seydi�ehir Yolu, 42395 Akkise/Ah�rl�/Konya, T�rkiye', N'10013', 37.365697040292183, 32.1348069445969, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
insert Address (Id, DistrictId, FullAddress, PostalCode, Latitude, Longitude, RowNumber, InsertDate, InsertedBy, IsDeleted) values (13, 10, N'Baraj, K�r�i�e�i Cd., 07320 Kepez/Antalya, T�rkiye', N'10013', 36.941016468536, 30.741603209245341, @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from Address;
set identity_insert Address off

---AddressType
set @rowNumber = 0;
set identity_insert AddressType on 
insert AddressType (Id, AddressTypeName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (1, 'Billing', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressType;
insert AddressType (Id, AddressTypeName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (2, 'Home', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressType;
insert AddressType (Id, AddressTypeName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (3, 'Main Office', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressType;
insert AddressType (Id, AddressTypeName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (4, 'Primary', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressType;
insert AddressType (Id, AddressTypeName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (5, 'Shipping', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressType;
insert AddressType (Id, AddressTypeName, RowNumber, InsertDate, InsertedBy, IsDeleted) values (6, 'Archive', @rowNumber, '20120101', 1, 0); select @rowNumber = max(RowNumber) + 1024 from AddressType;
set identity_insert AddressType off

---AccountLanguage
set @rowNumber = 0;
insert into AccountLanguage (LanguageCode, LanguageName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('tr', 'T�rk�e', '/Content/Image/AccountLanguage/Turkish/24.png', @rowNumber, '20120101', 1, 0); declare @lTurkish int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountLanguage;
insert into AccountLanguage (LanguageCode, LanguageName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('en', 'English', '/Content/Image/AccountLanguage/English/24.png', @rowNumber, '20120101', 1, 0); declare @lEnglish int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountLanguage;

---AccountLanguage
set @rowNumber = 0;
insert into AccountTheme(ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Default', '/Content/Image/AccountTheme/Default.jpg', @rowNumber, '20120101', 1, 0); declare @tDefault int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Aqua', '/Content/Image/AccountTheme/Aqua.jpg', @rowNumber, '20120101', 1, 0); declare @tAqua int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('BlackGlass', '/Content/Image/AccountTheme/BlackGlass.jpg', @rowNumber, '20120101', 1, 0); declare @tBlackGlass int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('DevEx', '/Content/Image/AccountTheme/DevEx.jpg', @rowNumber, '20120101', 1, 0); declare @tDevEx int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Glass', '/Content/Image/AccountTheme/Glass.jpg', @rowNumber, '20120101', 1, 0); declare @tGlass int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Metropolis', '/Content/Image/AccountTheme/Metropolis.jpg', @rowNumber, '20120101', 1, 0); declare @tMetropolis int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('MetropolisBlue', '/Content/Image/AccountTheme/MetropolisBlue.jpg', @rowNumber, '20120101', 1, 0); declare @tMetropolisBlue int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Office2003Blue', '/Content/Image/AccountTheme/Office2003Blue.jpg', @rowNumber, '20120101', 1, 0); declare @tOffice2003Blue int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Office2003Olive', '/Content/Image/AccountTheme/Office2003Olive.jpg', @rowNumber, '20120101', 1, 0); declare @tOffice2003Olive int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Office2003Silver', '/Content/Image/AccountTheme/Office2003Silver.jpg', @rowNumber, '20120101', 1, 0); declare @tOffice2003Silver int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Office2010Black', '/Content/Image/AccountTheme/Office2010Black.jpg', @rowNumber, '20120101', 1, 0); declare @tOffice2010Black int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Office2010Blue', '/Content/Image/AccountTheme/Office2010Blue.jpg', @rowNumber, '20120101', 1, 0); declare @tOffice2010Blue int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Office2010Silver', '/Content/Image/AccountTheme/Office2010Silver.jpg', @rowNumber, '20120101', 1, 0); declare @tOffice2010Silver int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('PlasticBlue', '/Content/Image/AccountTheme/PlasticBlue.jpg', @rowNumber, '20120101', 1, 0); declare @tPlasticBlue int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('RedWine', '/Content/Image/AccountTheme/RedWine.jpg', @rowNumber, '20120101', 1, 0); declare @tRedWine int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('SoftOrange', '/Content/Image/AccountTheme/SoftOrange.jpg', @rowNumber, '20120101', 1, 0); declare @tSoftOrange int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Youthful', '/Content/Image/AccountTheme/Youthful.jpg', @rowNumber, '20120101', 1, 0); declare @tYouthful int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('iOS', '/Content/Image/AccountTheme/iOS.jpg', @rowNumber, '20120101', 1, 0); declare @tiOS int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Moderno', '/Content/Image/AccountTheme/Moderno.jpg', @rowNumber, '20120101', 1, 0); declare @tModerno int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;
insert into AccountTheme (ThemeName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Mulberry', '/Content/Image/AccountTheme/Mulberry.jpg', @rowNumber, '20120101', 1, 0); declare @tMulberry int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountTheme;

---AccountPerson
set identity_insert AccountPerson on;
insert into AccountPerson (Id, VillageId , FirstName, LastName, IsMale, PicturePath, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(1, 1, 'Fatih', 'Do�anay', 1, '/Content/Upload/AccountPerson/PicturePath/1 Fatih Do�anay/Fatih_Doganay.png', null, null, '20120101', 1, 0);
insert into AccountPerson (Id, VillageId , FirstName, LastName, IsMale, PicturePath, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(2, 1, 'Muhammed', 'Babur', 1, null, null, null, '20120101', 1, 0);
insert into AccountPerson (Id, VillageId , FirstName, LastName, IsMale, PicturePath, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(3, 1, 'Vehbi', '�zalp', 1, null, null, null, '20120101', 1, 0);
insert into AccountPerson (Id, VillageId , FirstName, LastName, IsMale, PicturePath, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(4, 1, 'Musa', 'Uyar', 1, null, null, null, '20120101', 1, 0);
insert into AccountPerson (Id, VillageId, FirstName, LastName, IsMale, PicturePath, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(5, 1, 'Ahmet', 'Turan', 1, null, null, null, '20120101', 1, 0);
insert into AccountPerson (Id, VillageId, FirstName, LastName, IsMale, PicturePath, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(6, 1, 'Leyla', 'Turan', 0, null, null, null, '20120101', 1, 0);
insert into AccountPerson (Id, VillageId, FirstName, LastName, IsMale, PicturePath, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(7, 1, '�akir', 'Turan', 1, null, 5, 6, '20120101', 1, 0);
insert into AccountPerson (Id, VillageId, FirstName, LastName, IsMale, PicturePath, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(8, 1, 'Recep', 'Turan', 1, null, 5, 6, '20120101', 1, 0);
insert into AccountPerson (Id, VillageId, FirstName, LastName, IsMale, PicturePath, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(9, 1, '�akir', 'Kara', 1, null, null, null, '20120101', 1, 0);
insert into AccountPerson (Id, VillageId, FirstName, LastName, IsMale, PicturePath, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(10, 1, 'Fatma', 'Turan', 0, null, 9, null, '20120101', 1, 0);
insert into AccountPerson (Id, VillageId, FirstName, LastName, IsMale, PicturePath, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(11, 1, 'G�l', 'Turan', 0, null, 8, 10, '20120101', 1, 0);
set identity_insert AccountPerson off;

---AccountRole
set @rowNumber = 0;
insert into AccountRole (RoleName, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Full authorize', @rowNumber, '20120101', 1, 0); declare @rFullAuthorize int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountRole;
insert into AccountRole (RoleName, RowNumber, InsertDate, InsertedBy, IsDeleted) values('General manager', @rowNumber, '20120101', 1, 0); declare @rGeneralManager int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountRole;
insert into AccountRole (RoleName, RowNumber, InsertDate, InsertedBy, IsDeleted) values('Editor', '20120101', @rowNumber, 1, 0); declare @rEditor int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from AccountRole;

---AccountRoleLanguage
insert into AccountRoleLanguage (RoleId, LanguageId, RoleName, InsertDate, InsertedBy, IsDeleted) values(@rFullAuthorize, @lTurkish, 'Tam yetki', '20120101', 1, 0);
insert into AccountRoleLanguage (RoleId, LanguageId, RoleName, InsertDate, InsertedBy, IsDeleted) values(@rGeneralManager, @lTurkish, 'Genel m�d�r', '20120101', 1, 0);
insert into AccountRoleLanguage (RoleId, LanguageId, RoleName, InsertDate, InsertedBy, IsDeleted) values(@rEditor, @lTurkish, 'Edit�r', '20120101', 1, 0);

---AccountUser
insert into AccountUser (PersonId, RoleId, Username, UserEMail, Password, ThemeId, LanguageId, IsSystemAdmin, InsertDate, InsertedBy, IsDeleted) values(1, @rFullAuthorize, 'fatih', 'bestprogramming@hotmail.com', 'a', @tOffice2010Blue, @lEnglish, 1, '20120101', 1, 0); declare @uFatih int = scope_identity();
insert into AccountUser (PersonId, RoleId, Username, UserEMail, Password, ThemeId, LanguageId, IsSystemAdmin, InsertDate, InsertedBy, IsDeleted) values(2, @rFullAuthorize, 'muhammed', 'muhammed@hotmail.com', 'a', @tOffice2010Blue, @lTurkish, 0, '20120101', 1, 0); declare @uMuhammed int = scope_identity();
insert into AccountUser (PersonId, RoleId, Username, UserEMail, Password, ThemeId, LanguageId, IsSystemAdmin, InsertDate, InsertedBy, IsDeleted) values(3, @rGeneralManager, 'vehbi', 'vehbi@hotmail.com', 'a', @tOffice2010Blue, @lTurkish, 0, '20120101', 1, 0); declare @uVehbi int = scope_identity();
insert into AccountUser (PersonId, RoleId, Username, UserEMail, Password, ThemeId, LanguageId, IsSystemAdmin, InsertDate, InsertedBy, IsDeleted) values(4, @rEditor, 'musa', 'musa@hotmail.com', 'a', @tOffice2010Blue, @lTurkish, 0, '20120101', 1, 0); declare @uMusa int = scope_identity();

---AccountPermission
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read TemplateMaster', '20120101', 1, 0); declare @pReadTemplateMaster int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert TemplateMaster', '20120101', 1, 0); declare @pInsertTemplateMaster int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update TemplateMaster', '20120101', 1, 0); declare @pUpdateTemplateMaster int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete TemplateMaster', '20120101', 1, 0); declare @pDeleteTemplateMaster int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export TemplateMaster', '20120101', 1, 0); declare @pExportTemplateMaster int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read TemplateDetail1', '20120101', 1, 0); declare @pReadTemplateDetail1 int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert TemplateDetail1', '20120101', 1, 0); declare @pInsertTemplateDetail1 int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update TemplateDetail1', '20120101', 1, 0); declare @pUpdateTemplateDetail1 int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete TemplateDetail1', '20120101', 1, 0); declare @pDeleteTemplateDetail1 int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export TemplateDetail1', '20120101', 1, 0); declare @pExportTemplateDetail1 int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read TemplateDetail2', '20120101', 1, 0); declare @pReadTemplateDetail2 int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert TemplateDetail2', '20120101', 1, 0); declare @pInsertTemplateDetail2 int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update TemplateDetail2', '20120101', 1, 0); declare @pUpdateTemplateDetail2 int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete TemplateDetail2', '20120101', 1, 0); declare @pDeleteTemplateDetail2 int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export TemplateDetail2', '20120101', 1, 0); declare @pExportTemplateDetail2 int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read TemplatePerson', '20120101', 1, 0); declare @pReadTemplatePerson int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Edit TemplatePerson', '20120101', 1, 0); declare @pEditTemplatePerson int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export TemplatePerson', '20120101', 1, 0); declare @pExportTemplatePerson int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read TemplateUser', '20120101', 1, 0); declare @pReadTemplateUser int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Edit TemplateUser', '20120101', 1, 0); declare @pEditTemplateUser int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export TemplateUser', '20120101', 1, 0); declare @pExportTemplateUser int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AccountLanguage', '20120101', 1, 0); declare @pReadAccountLanguage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert AccountLanguage', '20120101', 1, 0); declare @pInsertAccountLanguage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update AccountLanguage', '20120101', 1, 0); declare @pUpdateAccountLanguage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete AccountLanguage', '20120101', 1, 0); declare @pDeleteAccountLanguage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AccountLanguage', '20120101', 1, 0); declare @pExportAccountLanguage int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AccountPerson', '20120101', 1, 0); declare @pReadAccountPerson int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert AccountPerson', '20120101', 1, 0); declare @pInsertAccountPerson int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update AccountPerson', '20120101', 1, 0); declare @pUpdateAccountPerson int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete AccountPerson', '20120101', 1, 0); declare @pDeleteAccountPerson int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AccountPerson', '20120101', 1, 0); declare @pExportAccountPerson int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AccountPersonAddress', '20120101', 1, 0); declare @pReadAccountPersonAddress int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert AccountPersonAddress', '20120101', 1, 0); declare @pInsertAccountPersonAddress int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update AccountPersonAddress', '20120101', 1, 0); declare @pUpdateAccountPersonAddress int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete AccountPersonAddress', '20120101', 1, 0); declare @pDeleteAccountPersonAddress int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AccountPersonAddress', '20120101', 1, 0); declare @pExportAccountPersonAddress int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AccountRole', '20120101', 1, 0); declare @pReadAccountRole int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert AccountRole', '20120101', 1, 0); declare @pInsertAccountRole int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update AccountRole', '20120101', 1, 0); declare @pUpdateAccountRole int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete AccountRole', '20120101', 1, 0); declare @pDeleteAccountRole int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AccountRole', '20120101', 1, 0); declare @pExportAccountRole int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AccountRoleLanguage', '20120101', 1, 0); declare @pReadAccountRoleLanguage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert AccountRoleLanguage', '20120101', 1, 0); declare @pInsertAccountRoleLanguage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update AccountRoleLanguage', '20120101', 1, 0); declare @pUpdateAccountRoleLanguage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete AccountRoleLanguage', '20120101', 1, 0); declare @pDeleteAccountRoleLanguage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AccountRoleLanguage', '20120101', 1, 0); declare @pExportAccountRoleLanguage int = scope_identity();


insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AccountRolePermission', '20120101', 1, 0); declare @pReadAccountRolePermission int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Edit AccountRolePermission', '20120101', 1, 0); declare @pEditAccountRolePermission int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AccountRolePermission', '20120101', 1, 0); declare @pExportAccountRolePermission int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AccountTheme', '20120101', 1, 0); declare @pReadAccountTheme int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert AccountTheme', '20120101', 1, 0); declare @pInsertAccountTheme int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update AccountTheme', '20120101', 1, 0); declare @pUpdateAccountTheme int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete AccountTheme', '20120101', 1, 0); declare @pDeleteAccountTheme int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AccountTheme', '20120101', 1, 0); declare @pExportAccountTheme int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AccountUser', '20120101', 1, 0); declare @pReadAccountUser int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert AccountUser', '20120101', 1, 0); declare @pInsertAccountUser int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update AccountUser', '20120101', 1, 0); declare @pUpdateAccountUser int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete AccountUser', '20120101', 1, 0); declare @pDeleteAccountUser int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AccountUser', '20120101', 1, 0); declare @pExportAccountUser int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read Password Column of AccountUser', '20120101', 1, 0); declare @pReadPasswordColumnOfAccountUser int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AccountUserPermission', '20120101', 1, 0); declare @pReadAccountUserPermission int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Edit AccountUserPermission', '20120101', 1, 0); declare @pEditAccountUserPermission int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AccountUserPermission', '20120101', 1, 0); declare @pExportAccountUserPermission int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read Address', '20120101', 1, 0); declare @pReadAddress int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert Address', '20120101', 1, 0); declare @pInsertAddress int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update Address', '20120101', 1, 0); declare @pUpdateAddress int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete Address', '20120101', 1, 0); declare @pDeleteAddress int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export Address', '20120101', 1, 0); declare @pExportAddress int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AddressCity', '20120101', 1, 0); declare @pReadAddressCity int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert AddressCity', '20120101', 1, 0); declare @pInsertAddressCity int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update AddressCity', '20120101', 1, 0); declare @pUpdateAddressCity int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete AddressCity', '20120101', 1, 0); declare @pDeleteAddressCity int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AddressCity', '20120101', 1, 0); declare @pExportAddressCity int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AddressCountry', '20120101', 1, 0); declare @pReadAddressCountry int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert AddressCountry', '20120101', 1, 0); declare @pInsertAddressCountry int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update AddressCountry', '20120101', 1, 0); declare @pUpdateAddressCountry int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete AddressCountry', '20120101', 1, 0); declare @pDeleteAddressCountry int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AddressCountry', '20120101', 1, 0); declare @pExportAddressCountry int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AddressDistrict', '20120101', 1, 0); declare @pReadAddressDistrict int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert AddressDistrict', '20120101', 1, 0); declare @pInsertAddressDistrict int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update AddressDistrict', '20120101', 1, 0); declare @pUpdateAddressDistrict int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete AddressDistrict', '20120101', 1, 0); declare @pDeleteAddressDistrict int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AddressDistrict', '20120101', 1, 0); declare @pExportAddressDistrict int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read AddressType', '20120101', 1, 0); declare @pReadAddressType int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert AddressType', '20120101', 1, 0); declare @pInsertAddressType int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update AddressType', '20120101', 1, 0); declare @pUpdateAddressType int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete AddressType', '20120101', 1, 0); declare @pDeleteAddressType int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export AddressType', '20120101', 1, 0); declare @pExportAddressType int = scope_identity();

insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Read SystemVillage', '20120101', 1, 0); declare @pReadSystemVillage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Insert SystemVillage', '20120101', 1, 0); declare @pInsertSystemVillage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Update SystemVillage', '20120101', 1, 0); declare @pUpdateSystemVillage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Delete SystemVillage', '20120101', 1, 0); declare @pDeleteSystemVillage int = scope_identity();
insert into AccountPermission (PermissionName, InsertDate, InsertedBy, IsDeleted) values('Export SystemVillage', '20120101', 1, 0); declare @pExportSystemVillage int = scope_identity();


---AccountRolePermission
insert into AccountRolePermission (RoleId, PermissionId, InsertDate, InsertedBy, IsDeleted)
select @rFullAuthorize, Id, '20150101', 1, 0 from AccountPermission;

insert into AccountRolePermission (RoleId, PermissionId, InsertDate, InsertedBy, IsDeleted)
values(@rGeneralManager, @pReadAccountPerson, '20150101', 1, 0),
(@rGeneralManager, @pInsertAccountPerson, '20150101', 1, 0),
(@rGeneralManager, @pUpdateAccountPerson, '20150101', 1, 0),
(@rGeneralManager, @pDeleteAccountPerson, '20150101', 1, 0),
(@rGeneralManager, @pExportAccountPerson, '20150101', 1, 0);

insert into AccountRolePermission (RoleId, PermissionId, InsertDate, InsertedBy, IsDeleted)
values(@rEditor, @pReadAddress, '20150101', 1, 0),
(@rEditor, @pInsertAddress, '20150101', 1, 0),
(@rEditor, @pUpdateAddress, '20150101', 1, 0),
(@rEditor, @pDeleteAddress, '20150101', 1, 0),
(@rEditor, @pExportAddress, '20150101', 1, 0);

---AccountPermissionMap
insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(null, null, 'Template process', '20120101', 1, 0); declare @pmTemplateProcess int = scope_identity();
	insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateProcess, null, 'Template master page', '20120101', 1, 0); declare @pmTemplateMasterPage int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateMasterPage, @pReadTemplateMaster,  'Read', '20120101', 1, 0); declare @pmReadTemplateMaster int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateMasterPage, @pInsertTemplateMaster,  'Insert', '20120101', 1, 0); declare @pmInsertTemplateMaster int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateMasterPage, @pUpdateTemplateMaster,  'Update', '20120101', 1, 0); declare @pmUpdateTemplateMaster int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateMasterPage, @pDeleteTemplateMaster,  'Delete', '20120101', 1, 0); declare @pmDeleteTemplateMaster int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateMasterPage, @pExportTemplateMaster,  'Export', '20120101', 1, 0); declare @pmExportTemplateMaster int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateMasterPage, null, 'Template detail 1 page', '20120101', 1, 0); declare @pmTemplateDetail1Page int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail1Page, @pReadTemplateDetail1,  'Read', '20120101', 1, 0); declare @pmReadTemplateDetail1 int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail1Page, @pInsertTemplateDetail1,  'Insert', '20120101', 1, 0); declare @pmInsertTemplateDetail1 int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail1Page, @pUpdateTemplateDetail1,  'Update', '20120101', 1, 0); declare @pmUpdateTemplateDetail1 int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail1Page, @pDeleteTemplateDetail1,  'Delete', '20120101', 1, 0); declare @pmDeleteTemplateDetail1 int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail1Page, @pExportTemplateDetail1,  'Export', '20120101', 1, 0); declare @pmExportTemplateDetail1 int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateMasterPage, null, 'Template detail 2 page', '20120101', 1, 0); declare @pmTemplateDetail2Page int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail2Page, @pReadTemplateDetail2,  'Read', '20120101', 1, 0); declare @pmReadTemplateDetail2 int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail2Page, @pInsertTemplateDetail2,  'Insert', '20120101', 1, 0); declare @pmInsertTemplateDetail2 int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail2Page, @pUpdateTemplateDetail2,  'Update', '20120101', 1, 0); declare @pmUpdateTemplateDetail2 int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail2Page, @pDeleteTemplateDetail2,  'Delete', '20120101', 1, 0); declare @pmDeleteTemplateDetail2 int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail2Page, @pExportTemplateDetail2,  'Export', '20120101', 1, 0); declare @pmExportTemplateDetail2 int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateMasterPage, null, 'Template person page', '20120101', 1, 0); declare @pmTemplatePersonPage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplatePersonPage, @pReadTemplatePerson,  'Read', '20120101', 1, 0); declare @pmReadTemplatePerson int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplatePersonPage, @pEditTemplatePerson,  'Edit', '20120101', 1, 0); declare @pmEditTemplatePerson int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplatePersonPage, @pExportTemplatePerson,  'Export', '20120101', 1, 0); declare @pmExportTemplatePerson int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateMasterPage, null, 'Template user page', '20120101', 1, 0); declare @pmTemplateUserPage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateUserPage, @pReadTemplateUser,  'Read', '20120101', 1, 0); declare @pmReadTemplateUser int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateUserPage, @pEditTemplateUser,  'Edit', '20120101', 1, 0); declare @pmEditTemplateUser int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateUserPage, @pExportTemplateUser,  'Export', '20120101', 1, 0); declare @pmExportTemplateUser int = scope_identity();

insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(null, null, 'Account process', '20120101', 1, 0); declare @pmAccountProcess int = scope_identity();
	insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountProcess, null, 'Person page', '20120101', 1, 0); declare @pmAccountPersonPage int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonPage, @pReadAccountPerson,  'Read', '20120101', 1, 0); declare @pmReadAccountPerson int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonPage, @pInsertAccountPerson,  'Insert', '20120101', 1, 0); declare @pmInsertAccountPerson int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonPage, @pUpdateAccountPerson,  'Update', '20120101', 1, 0); declare @pmUpdateAccountPerson int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonPage, @pDeleteAccountPerson,  'Delete', '20120101', 1, 0); declare @pmDeleteAccountPerson int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonPage, @pExportAccountPerson,  'Export', '20120101', 1, 0); declare @pmExportAccountPerson int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonPage, null, 'User page', '20120101', 1, 0); declare @pmAccountUserPage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountUserPage, @pReadAccountUser,  'Read', '20120101', 1, 0); declare @pmReadAccountUser int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountUserPage, @pInsertAccountUser,  'Insert', '20120101', 1, 0); declare @pmInsertAccountUser int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountUserPage, @pUpdateAccountUser,  'Update', '20120101', 1, 0); declare @pmUpdateAccountUser int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountUserPage, @pDeleteAccountUser,  'Delete', '20120101', 1, 0); declare @pmDeleteAccountUser int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountUserPage, @pExportAccountUser,  'Export', '20120101', 1, 0); declare @pmExportAccountUser int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountUserPage, null, 'User grid', '20120101', 1, 0); declare @pmAccountUserGrid int = scope_identity();
				insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountUserGrid, @pReadPasswordColumnOfAccountUser,  'Read password column', '20120101', 1, 0); declare @pmReadPasswordColumnOfAccountUser int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonPage, null, 'Person address page', '20120101', 1, 0); declare @pmAccountPersonAddressPage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonAddressPage, @pReadAccountPersonAddress,  'Read', '20120101', 1, 0); declare @pmReadAccountPersonAddress int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonAddressPage, @pInsertAccountPersonAddress,  'Insert', '20120101', 1, 0); declare @pmInsertAccountPersonAddress int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonAddressPage, @pUpdateAccountPersonAddress,  'Update', '20120101', 1, 0); declare @pmUpdateAccountPersonAddress int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonAddressPage, @pDeleteAccountPersonAddress,  'Delete', '20120101', 1, 0); declare @pmDeleteAccountPersonAddress int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonAddressPage, @pExportAccountPersonAddress,  'Export', '20120101', 1, 0); declare @pmExportAccountPersonAddress int = scope_identity();
	insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountProcess, null, 'Role page', '20120101', 1, 0); declare @pmAccountRolePage int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePage, @pReadAccountRole,  'Read', '20120101', 1, 0); declare @pmReadAccountRole int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePage, @pInsertAccountRole,  'Insert', '20120101', 1, 0); declare @pmInsertAccountRole int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePage, @pUpdateAccountRole,  'Update', '20120101', 1, 0); declare @pmUpdateAccountRole int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePage, @pDeleteAccountRole,  'Delete', '20120101', 1, 0); declare @pmDeleteAccountRole int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePage, @pExportAccountRole,  'Export', '20120101', 1, 0); declare @pmExportAccountRole int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePage, null, 'Role language page', '20120101', 1, 0); declare @pmAccountRoleLanguagePage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRoleLanguagePage, @pReadAccountRoleLanguage,  'Read', '20120101', 1, 0); declare @pmReadAccountRoleLanguage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRoleLanguagePage, @pInsertAccountRoleLanguage,  'Insert', '20120101', 1, 0); declare @pmInsertAccountRoleLanguage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRoleLanguagePage, @pUpdateAccountRoleLanguage,  'Update', '20120101', 1, 0); declare @pmUpdateAccountRoleLanguage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRoleLanguagePage, @pDeleteAccountRoleLanguage,  'Delete', '20120101', 1, 0); declare @pmDeleteAccountRoleLanguage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRoleLanguagePage, @pExportAccountRoleLanguage,  'Export', '20120101', 1, 0); declare @pmExportAccountRoleLanguage int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePage, null, 'Role permissions page', '20120101', 1, 0); declare @pmAccountRolePermissionPage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePermissionPage, @pReadAccountRolePermission,  'Read', '20120101', 1, 0); declare @pmReadAccountRolePermission int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePermissionPage, @pEditAccountRolePermission,  'Edit', '20120101', 1, 0); declare @pmEditAccountRolePermission int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePermissionPage, @pExportAccountRolePermission,  'Export', '20120101', 1, 0); declare @pmExportAccountRolePermission int = scope_identity();


insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(null, null, 'Adress process', '20120101', 1, 0); declare @pmAdressProcess int = scope_identity();
	insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(null, null, 'References', '20120101', 1, 0); declare @pmAdressProcessReferences int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAdressProcessReferences, null, 'Address type page', '20120101', 1, 0); declare @pmAddressTypePage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressTypePage, @pReadAddressType, 'Read', '20120101', 1, 0); declare @pmReadAddressType int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressTypePage, @pInsertAddressType, 'Insert', '20120101', 1, 0); declare @pmInsertAddressType int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressTypePage, @pUpdateAddressType, 'Update', '20120101', 1, 0); declare @pmUpdateAddressType int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressTypePage, @pDeleteAddressType, 'Delete', '20120101', 1, 0); declare @pmDeleteAddressType int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressTypePage, @pExportAddressType, 'Export', '20120101', 1, 0); declare @pmExportAddressType int = scope_identity();
	insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAdressProcess, null, 'Country page', '20120101', 1, 0); declare @pmAddressCountryPage int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCountryPage, @pReadAddressCountry, 'Read', '20120101', 1, 0); declare @pmReadAddressCountry int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCountryPage, @pInsertAddressCountry, 'Insert', '20120101', 1, 0); declare @pmInsertAddressCountry int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCountryPage, @pUpdateAddressCountry, 'Update', '20120101', 1, 0); declare @pmUpdateAddressCountry int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCountryPage, @pDeleteAddressCountry, 'Delete', '20120101', 1, 0); declare @pmDeleteAddressCountry int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCountryPage, @pExportAddressCountry, 'Export', '20120101', 1, 0); declare @pmExportAddressCountry int = scope_identity();
		insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCountryPage, null, 'City page', '20120101', 1, 0); declare @pmAddressCityPage int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCityPage, @pReadAddressCity, 'Read', '20120101', 1, 0); declare @pmReadAddressCity int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCityPage, @pInsertAddressCity, 'Insert', '20120101', 1, 0); declare @pmInsertAddressCity int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCityPage, @pUpdateAddressCity, 'Update', '20120101', 1, 0); declare @pmUpdateAddressCity int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCityPage, @pDeleteAddressCity, 'Delete', '20120101', 1, 0); declare @pmDeleteAddressCity int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCityPage, @pExportAddressCity, 'Export', '20120101', 1, 0); declare @pmExportAddressCity int = scope_identity();
			insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCityPage, null, 'District page', '20120101', 1, 0); declare @pmAddressDistrictPage int = scope_identity();
				insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressDistrictPage, @pReadAddressDistrict, 'Read', '20120101', 1, 0); declare @pmReadAddressDistrict int = scope_identity();
				insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressDistrictPage, @pInsertAddressDistrict, 'Insert', '20120101', 1, 0); declare @pmInsertAddressDistrict int = scope_identity();
				insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressDistrictPage, @pUpdateAddressDistrict, 'Update', '20120101', 1, 0); declare @pmUpdateAddressDistrict int = scope_identity();
				insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressDistrictPage, @pDeleteAddressDistrict, 'Delete', '20120101', 1, 0); declare @pmDeleteAddressDistrict int = scope_identity();
				insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressDistrictPage, @pExportAddressDistrict, 'Export', '20120101', 1, 0); declare @pmExportAddressDistrict int = scope_identity();
				insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressDistrictPage, null, 'Address page', '20120101', 1, 0); declare @pmAddressPage int = scope_identity();
					insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressPage, @pReadAddress, 'Read', '20120101', 1, 0); declare @pmReadAddress int = scope_identity();
					insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressPage, @pInsertAddress, 'Insert', '20120101', 1, 0); declare @pmInsertAddress int = scope_identity();
					insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressPage, @pUpdateAddress, 'Update', '20120101', 1, 0); declare @pmUpdateAddress int = scope_identity();
					insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressPage, @pDeleteAddress, 'Delete', '20120101', 1, 0); declare @pmDeleteAddress int = scope_identity();
					insert into AccountPermissionMap (ParentId, PermissionId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressPage, @pExportAddress, 'Export', '20120101', 1, 0); declare @pmExportAddress int = scope_identity();

---AccountPermissionMapLanguage
insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateProcess, @lTurkish, '�ablon i�lemleri', '20120101', 1, 0);
	insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateMasterPage, @lTurkish, 'Temel �ablon sayfas�', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadTemplateMaster, @lTurkish, '�zleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertTemplateMaster, @lTurkish, 'Ekleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateTemplateMaster, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteTemplateMaster, @lTurkish, 'Silme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportTemplateMaster, @lTurkish, 'D��a aktarma', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail1Page, @lTurkish, 'Detay �ablon 1 sayfas�', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadTemplateDetail1, @lTurkish, '�zleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertTemplateDetail1, @lTurkish, 'Ekleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateTemplateDetail1, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteTemplateDetail1, @lTurkish, 'Silme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportTemplateDetail1, @lTurkish, 'D��a aktarma', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmTemplateDetail2Page, @lTurkish, 'Detay �ablon 2 sayfas�', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadTemplateDetail2, @lTurkish, '�zleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertTemplateDetail2, @lTurkish, 'Ekleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateTemplateDetail2, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteTemplateDetail2, @lTurkish, 'Silme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportTemplateDetail2, @lTurkish, 'D��a aktarma', '20120101', 1, 0);

insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountProcess, @lTurkish, 'Hesap i�lemleri', '20120101', 1, 0);
	insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonPage, @lTurkish, 'Ki�i sayfas�', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadAccountPerson, @lTurkish, '�zleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertAccountPerson, @lTurkish, 'Ekleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateAccountPerson, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteAccountPerson, @lTurkish, 'Silme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportAccountPerson, @lTurkish, 'D��a aktarma', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountUserPage, @lTurkish, 'Kullan�c� sayfas�', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadAccountUser, @lTurkish, '�zleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertAccountUser, @lTurkish, 'Ekleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateAccountUser, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteAccountUser, @lTurkish, 'Silme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportAccountUser, @lTurkish, 'D��a aktarma', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountUserGrid, @lTurkish, 'Kullan�c� gridi', '20120101', 1, 0);
				insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadPasswordColumnOfAccountUser, @lTurkish, '�ifre alan�n� g�rme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountPersonAddressPage, @lTurkish, 'Ki�i adresleri sayfas�', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadAccountPersonAddress, @lTurkish, '�zleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertAccountPersonAddress, @lTurkish, 'Ekleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateAccountPersonAddress, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteAccountPersonAddress, @lTurkish, 'Silme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportAccountPersonAddress, @lTurkish, 'D��a aktarma', '20120101', 1, 0);
	
	insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePage, @lTurkish, 'Rol sayfas�', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadAccountRole, @lTurkish, '�zleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertAccountRole, @lTurkish, 'Ekleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateAccountRole, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteAccountRole, @lTurkish, 'Silme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportAccountRole, @lTurkish, 'D��a aktarma', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRoleLanguagePage, @lTurkish, 'Rol dili sayfas�', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadAccountRoleLanguage, @lTurkish, '�zleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertAccountRoleLanguage, @lTurkish, 'Ekleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateAccountRoleLanguage, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteAccountRoleLanguage, @lTurkish, 'Silme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportAccountRoleLanguage, @lTurkish, 'D��a aktarma', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAccountRolePermissionPage, @lTurkish, 'Rol izinleri sayfas�', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadAccountRolePermission, @lTurkish, '�zleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmEditAccountRolePermission, @lTurkish, 'D�zenleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportAccountRolePermission, @lTurkish, 'D��a aktarma', '20120101', 1, 0);

insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAdressProcess, @lTurkish, 'Adres i�lemleri', '20120101', 1, 0);
	insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAdressProcessReferences, @lTurkish, 'Referanslar', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressTypePage, @lTurkish, 'Adres tipi sayfas�', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadAddressType, @lTurkish, '�zleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertAddressType, @lTurkish, 'Ekleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateAddressType, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteAddressType, @lTurkish, 'Silme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportAddressType, @lTurkish, 'D��a aktarma', '20120101', 1, 0);
	insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCountryPage, @lTurkish, '�lke sayfas�', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadAddressCountry, @lTurkish, '�zleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertAddressCountry, @lTurkish, 'Ekleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateAddressCountry, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteAddressCountry, @lTurkish, 'Silme', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportAddressCountry, @lTurkish, 'D��a aktarma', '20120101', 1, 0);
		insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressCityPage, @lTurkish, '�l sayfas�', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadAddressCity, @lTurkish, '�zleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertAddressCity, @lTurkish, 'Ekleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateAddressCity, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteAddressCity, @lTurkish, 'Silme', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportAddressCity, @lTurkish, 'D��a aktarma', '20120101', 1, 0);
			insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressDistrictPage, @lTurkish, '�l�e sayfas�', '20120101', 1, 0);
				insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadAddressDistrict, @lTurkish, '�zleme', '20120101', 1, 0);
				insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertAddressDistrict, @lTurkish, 'Ekleme', '20120101', 1, 0);
				insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateAddressDistrict, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
				insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteAddressDistrict, @lTurkish, 'Silme', '20120101', 1, 0);
				insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportAddressDistrict, @lTurkish, 'D��a aktarma', '20120101', 1, 0);
				insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmAddressPage, @lTurkish, 'Adres sayfas�', '20120101', 1, 0);
					insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmReadAddress, @lTurkish, '�zleme', '20120101', 1, 0);
					insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmInsertAddress, @lTurkish, 'Ekleme', '20120101', 1, 0);
					insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmUpdateAddress, @lTurkish, 'G�ncelleme', '20120101', 1, 0);
					insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmDeleteAddress, @lTurkish, 'Silme', '20120101', 1, 0);
					insert into AccountPermissionMapLanguage (PermissionMapId, LanguageId, PermissionMapName, InsertDate, InsertedBy, IsDeleted) values(@pmExportAddress, @lTurkish, 'D��a aktarma', '20120101', 1, 0);



---SystemMenu
set @rowNumber = 0;
insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(null, null, 'Template Process', null, null, '/Content/Icon/YellowFolder/16.png', @rowNumber, '20120101', 1, 0); declare @mTemplateProcess int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
	insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mTemplateProcess, @pReadTemplateMaster, 'Master Template', 'TemplateMaster', 'Index', '/Content/Icon/TemplateMaster/16.png', @rowNumber, '20120101', 1, 0); declare @mTemplateMaster int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mTemplateMaster, @pReadTemplateDetail1, 'Detail Template 1', 'TemplateDetail1', 'Index', '/Content/Icon/TemplateDetail1/16.png', @rowNumber, '20120101', 1, 0); declare @mTemplateDetail1 int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mTemplateMaster, @pReadTemplateDetail2, 'Detail Template 2', 'TemplateDetail2', 'Index', '/Content/Icon/TemplateDetail2/16.png', @rowNumber, '20120101', 1, 0); declare @mTemplateDetail2 int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mTemplateMaster, @pReadTemplateDetail1, 'Template Person', 'TemplatePerson', 'Index', '/Content/Icon/TemplatePerson/16.png', @rowNumber, '20120101', 1, 0); declare @mTemplatePerson int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mTemplateMaster, @pReadTemplateDetail2, 'Template User', 'TemplateUser', 'Index', '/Content/Icon/TemplateUser/16.png', @rowNumber, '20120101', 1, 0); declare @mTemplateUser int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;

insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(null, null, 'System Process', null, null, '/Content/Icon/YellowFolder/16.png', @rowNumber, '20120101', 1, 0); declare @mSystemProcess int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
	insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mSystemProcess, @pReadSystemVillage, 'Village', 'SystemVillage', 'Index', '/Content/Icon/SystemVillage/16.png', @rowNumber, '20120101', 1, 0); declare @mSystemVillage int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;

insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(null, null, 'Account Process', null, null, '/Content/Icon/YellowFolder/16.png', @rowNumber, '20120101', 1, 0); declare @mAccountProcess int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
	insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAccountProcess, null, 'References', null, null, '/Content/Icon/BlackFolder/16.png', @rowNumber, '20120101', 1, 0); declare @mAccountProcessReferences int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAccountProcessReferences, @pReadAccountLanguage, 'Language', 'AccountLanguage', 'Index', '/Content/Icon/AccountLanguage/16.png', @rowNumber, '20120101', 1, 0); declare @mAccountLanguage int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAccountProcessReferences, @pReadAccountTheme, 'Theme', 'AccountTheme', 'Index', '/Content/Icon/AccountTheme/16.png', @rowNumber, '20120101', 1, 0); declare @mAccountTheme int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
	insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAccountProcess, @pReadAccountPerson, 'Person', 'AccountPerson', 'Index', '/Content/Icon/AccountPerson/16.png', @rowNumber, '20120101', 1, 0); declare @mAccountPerson int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAccountPerson, @pReadAccountPersonAddress, 'Person Address', 'AccountPersonAddress', 'Index', '/Content/Icon/AccountPersonAddress/16.png', @rowNumber, '20120101', 1, 0); declare @mAccountPersonAddress int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAccountPerson, @pReadAccountUser, 'User', 'AccountUser', 'Index', '/Content/Icon/AccountUser/16.png', @rowNumber, '20120101', 1, 0); declare @mAccountUser int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
			insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAccountUser, @pReadAccountUserPermission, 'User Permissions', 'AccountUserPermission', 'Index', '/Content/Icon/AccountUserPermission/16.png', @rowNumber, '20120101', 1, 0); declare @mAccountUserPermission int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
	insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAccountProcess, @pReadAccountRole, 'Role', 'AccountRole', 'Index', '/Content/Icon/AccountRole/16.png', @rowNumber, '20120101', 1, 0); declare @mAccountRole int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAccountRole, @pReadAccountRoleLanguage, 'Role Language', 'AccountRoleLanguage', 'Index', '/Content/Icon/AccountRoleLanguage/16.png', @rowNumber, '20120101', 1, 0); declare @mAccountRoleLanguage int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAccountRole, @pReadAccountRolePermission, 'Role Permissions', 'AccountRolePermission', 'Index', '/Content/Icon/AccountRolePermission/16.png', @rowNumber, '20120101', 1, 0); declare @mAccountRolePermission int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;


insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(null, null, 'Address Process', null, null, '/Content/Icon/YellowFolder/16.png', @rowNumber, '20120101', 1, 0); declare @mAddressProcess int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
	insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAddressProcess, null, 'References', null, null, '/Content/Icon/BlackFolder/16.png', @rowNumber, '20120101', 1, 0); declare @mAddressProcessReferences int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAddressProcessReferences, @pReadAddressType, 'Address Type', 'AddressType', 'Index', '/Content/Icon/AddressType/16.png', @rowNumber, '20120101', 1, 0); declare @mAddressType int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
	insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAddressProcess, @pReadAddressCountry, 'Country', 'AddressCountry', 'Index', '/Content/Icon/AddressCountry/16.png', @rowNumber, '20120101', 1, 0); declare @mAddressCountry int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
		insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAddressCountry, @pReadAddressCity, 'City', 'AddressCity', 'Index', '/Content/Icon/AddressCity/16.png', @rowNumber, '20120101', 1, 0); declare @mAddressCity int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
			insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAddressCity, @pReadAddressDistrict, 'District', 'AddressDistrict', 'Index', '/Content/Icon/AddressDistrict/16.png', @rowNumber, '20120101', 1, 0); declare @mAddressDistrict int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;
				insert into SystemMenu (ParentId, PermissionId, MenuName, ControllerName, ActionName, IconPath, RowNumber, InsertDate, InsertedBy, IsDeleted) values(@mAddressDistrict, @pReadAddress, 'Address', 'Address', 'Index', '/Content/Icon/Address/16.png', @rowNumber, '20120101', 1, 0); declare @mAddress int = scope_identity(); select @rowNumber = max(RowNumber) + 1024 from SystemMenu;


---SystemMenuLanguage
insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mTemplateProcess, @lTurkish, '�ablon i�lemleri', '20120101', 1, 0);
	insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mTemplateMaster, @lTurkish, 'Temel �ablon', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mTemplateDetail1, @lTurkish, 'Detay �ablon 1', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mTemplateDetail2, @lTurkish, 'Detay �ablon 2', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mTemplatePerson, @lTurkish, 'Ki�i �ablon', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mTemplateUser, @lTurkish, 'Kullan�c� �ablon', '20120101', 1, 0);

insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mSystemProcess, @lTurkish, 'Sistem i�lemleri', '20120101', 1, 0);
	insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mSystemVillage, @lTurkish, 'K�y', '20120101', 1, 0);

insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAccountProcess, @lTurkish, 'Hesap i�lemleri', '20120101', 1, 0);
	insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAccountProcessReferences, @lTurkish, 'Referanslar', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAccountLanguage, @lTurkish, 'Dil', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAccountTheme, @lTurkish, 'Tema', '20120101', 1, 0);
	insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAccountUser, @lTurkish, 'Kullan�c�', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAccountPerson, @lTurkish, 'Ki�i', '20120101', 1, 0);
			insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAccountPersonAddress, @lTurkish, 'Ki�i adresleri', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAccountUserPermission, @lTurkish, 'Kullan�c� yetkileri', '20120101', 1, 0);
	insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAccountRole, @lTurkish, 'Rol', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAccountRoleLanguage, @lTurkish, 'Rol dili', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAccountRolePermission, @lTurkish, 'Rol yetkileri', '20120101', 1, 0);

insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAddressProcess, @lTurkish, 'Adres i�lemleri', '20120101', 1, 0);
	insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAddressProcessReferences, @lTurkish, 'Referanslar', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAddressType, @lTurkish, 'Adres tipi', '20120101', 1, 0);
	insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAddressCountry, @lTurkish, '�lke', '20120101', 1, 0);
		insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAddressCity, @lTurkish, '�l', '20120101', 1, 0);
			insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAddressDistrict, @lTurkish, '�l�e', '20120101', 1, 0);
				insert into SystemMenuLanguage (MenuId, LanguageId, MenuName, InsertDate, InsertedBy, IsDeleted) values(@mAddress, @lTurkish, 'Adres', '20120101', 1, 0);



