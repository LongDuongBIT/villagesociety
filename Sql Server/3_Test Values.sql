use VillageSociety;

declare @i int = 1, @j int, @villageId int;

while (@i <= 50)
begin
	insert into SystemVillage (VillageName, VillageDescription, InsertDate, InsertedBy, IsDeleted) values('Village' + cast(@i as nvarchar(10)) , 'Village' + cast(@i as nvarchar(10)), '20120101', 1, 0);
	set @villageId = SCOPE_IDENTITY();

	set @j = 1
	while (@j <= 100)
	begin
		insert into AccountPerson (VillageId, FirstName, LastName, IsMale, Father, Mother, InsertDate, InsertedBy, IsDeleted) values(@villageId, 'PersonName' + cast(@j as nvarchar(10)), 'PersonSurname' + cast(@j as nvarchar(10)), 1, null, null, '20120101', 1, 0);
		set @j = @j + 1;
	end;

	set @i = @i + 1;
end;





SET IDENTITY_INSERT [dbo].[TemplateMaster] ON 

GO
INSERT [dbo].[TemplateMaster] ([Id], [ComboBoxPersonId], [GridLookupPersonId], [ComboBoxAddressId], [GridLookupAddressId], [BitColumn1], [BitColumn2], [IntColumn], [FloatColumn], [DecimalColumn], [MoneyColumn], [PercentColumn], [DateColumn], [DateTimeColumn], [StringColumn], [MultilineColumn], [HtmlColumn], [PhoneNumberColumn], [CreditCardNumberColumn], [LatitudeColumn], [LongitudeColumn], [FilePathColumn], [ImagePathColumn], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (1, 1, 2, 1, 5, 1, 0, 18, 3.15878, CAST(46 AS Decimal(18, 0)), CAST(985 AS Decimal(18, 0)), CAST(75 AS Decimal(18, 0)), CAST(0xC73A0B00 AS Date), CAST(0x0000A56C0087F4B0 AS DateTime), N'String Column 1', N'Line 1
Line 2
Line 3', N'<p><span style="color: #0000ff;">Html Data 1</span></p><p><span style="color: #0000ff;"><em><strong>Html Data 2</strong></em><br/></span><br/></p>', N'12 (345) 678 9012', N'1234 5678 9012 3456', 37.365697040292183, 32.1348069445969, N'/Content/Upload/TemplateMaster/FilePathColumn/grvTemplateMaster.xls', N'/Content/Upload/TemplateMaster/ImagePathColumn/72.png', CAST(0x0000A56C00038D14 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[TemplateMaster] ([Id], [ComboBoxPersonId], [GridLookupPersonId], [ComboBoxAddressId], [GridLookupAddressId], [BitColumn1], [BitColumn2], [IntColumn], [FloatColumn], [DecimalColumn], [MoneyColumn], [PercentColumn], [DateColumn], [DateTimeColumn], [StringColumn], [MultilineColumn], [HtmlColumn], [PhoneNumberColumn], [CreditCardNumberColumn], [LatitudeColumn], [LongitudeColumn], [FilePathColumn], [ImagePathColumn], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (2, 3, 4, 6, 8, 1, 1, 21, 4.6546565, CAST(37 AS Decimal(18, 0)), CAST(996 AS Decimal(18, 0)), CAST(12 AS Decimal(18, 0)), CAST(0xD93A0B00 AS Date), CAST(0x0000A57F00C88020 AS DateTime), N'String Column 2', N'Row 1
Row 2', N'<p><strong>Html Row 1</strong></p><p style="text-align: center;"><strong>Html Row 2</strong><br/></p>', N'98 (765) 432 1098', N'5555 4444 3333 2222', 36.941016468536, 30.741603209245341, N'/Content/Upload/TemplateMaster/FilePathColumn/ImportantNotes.txt', N'/Content/Upload/TemplateMaster/ImagePathColumn/128.png', CAST(0x0000A56C0010E7B8 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[TemplateMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[TemplatePerson] ON 

GO
INSERT [dbo].[TemplatePerson] ([Id], [MasterId], [PersonId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (1, 1, 1, CAST(0x0000A56C00038D14 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[TemplatePerson] ([Id], [MasterId], [PersonId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (2, 1, 3, CAST(0x0000A56C00038D14 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[TemplatePerson] ([Id], [MasterId], [PersonId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (3, 1, 5, CAST(0x0000A56C00038D14 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[TemplatePerson] ([Id], [MasterId], [PersonId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (4, 1, 7, CAST(0x0000A56C00038D14 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[TemplatePerson] ([Id], [MasterId], [PersonId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (5, 2, 2, CAST(0x0000A56C0010E7B8 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[TemplatePerson] ([Id], [MasterId], [PersonId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (6, 2, 4, CAST(0x0000A56C0010E7B8 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[TemplatePerson] ([Id], [MasterId], [PersonId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (7, 2, 6, CAST(0x0000A56C0010E7B8 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[TemplatePerson] ([Id], [MasterId], [PersonId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (8, 2, 8, CAST(0x0000A56C0010E7B8 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[TemplatePerson] OFF
GO
SET IDENTITY_INSERT [dbo].[TemplateUser] ON 

GO
INSERT [dbo].[TemplateUser] ([Id], [MasterId], [UserId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (1, 1, 2, CAST(0x0000A56C00038D14 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[TemplateUser] ([Id], [MasterId], [UserId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (2, 1, 4, CAST(0x0000A56C00038D14 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[TemplateUser] ([Id], [MasterId], [UserId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (3, 2, 1, CAST(0x0000A56C0010E7B8 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[TemplateUser] ([Id], [MasterId], [UserId], [InsertDate], [InsertedBy], [UpdateDate], [UpdatedBy], [DeleteDate], [DeletedBy], [IsDeleted]) VALUES (4, 2, 3, CAST(0x0000A56C0010E7B8 AS DateTime), 1, NULL, NULL, NULL, NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[TemplateUser] OFF
GO